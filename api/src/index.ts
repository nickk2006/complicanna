require("dotenv").config(); // tslint:disable-line
import { GraphQLServer, Options } from "graphql-yoga";
import { resolvers } from "./resolvers";
import permissions from "./permissions";
import { createStorage, createFirebase, getUser } from "./google";
import db from "./prisma";
import * as sgMail from "@sendgrid/mail";
import * as bodyParser from "body-parser";
import webhooks from "./webhooks";

const storage = createStorage();
const firebase = createFirebase();
sgMail.setApiKey(process.env.SENDGRID_API_KEY);

const server = new GraphQLServer({
  typeDefs: "./src/schema.graphql",
  resolvers: resolvers as any,
  middlewares: [getUser, permissions],
  context: req => ({ db, storage, firebase, ...req })
});

server.express.post(
  "/webhooks",
  bodyParser.raw({ type: "application/json" }),
  webhooks
);

const serverOptions: Options =
  process.env.NODE_ENV === "production"
    ? {}
    : {
        playground: "/graphiql"
      };

server.start(serverOptions, ({ port }) =>
  console.log(`Server is running on http://localhost:${port}`)
);
