// This resolver file was scaffolded by github.com/prisma/graphqlgen, DO NOT EDIT.
// Please do not import this file directly but copy & paste to your application code.

import { AddressResolvers } from "../generated/resolvers";

export const Address: AddressResolvers.Type = {
  ...AddressResolvers.defaultResolvers,

  company: (parent, args, ctx) => ctx.db.address({ id: parent.id }).company()
};
