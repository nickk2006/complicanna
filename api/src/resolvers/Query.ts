// This resolver file was scaffolded by github.com/prisma/graphqlgen, DO NOT EDIT.
// Please do not import this file directly but copy & paste to your application code.

import { QueryResolvers } from "../generated/resolvers";

export const Query: QueryResolvers.Type = {
  ...QueryResolvers.defaultResolvers,

  company: (parent, args, ctx) => ctx.db.company(args.where),

  license: (parent, args, ctx) => ctx.db.license(args.where),

  batch: (parent, args, ctx) => ctx.db.batch(args.where),

  batches: (parent, args, ctx) =>
    ctx.db.batches({
      where: { ...args.where, company: { id: ctx.user.companyId } },
      skip: args.skip,
      after: args.after,
      before: args.before,
      first: args.first,
      last: args.last,
      orderBy: "dateOfEntry_DESC"
    }),

  licenseHolder: (parent, args, ctx) => ctx.db.licenseHolder(args.where),

  licenseHolders: (parent, args, ctx) =>
    ctx.db.licenseHolders({
      where: { company: { id: ctx.user.companyId } },
      orderBy: "name_ASC"
    }),

  test: (parent, args, ctx) => ctx.db.test(args.where),

  tests: (parent, args, ctx) =>
    ctx.db.tests({
      where: { company: { id: ctx.user.companyId } },
      orderBy: "createdAt_DESC"
    }),

  testingLab: (parent, args, ctx) => ctx.db.testingLab(args.where),

  testingLabs: (parent, args, ctx) =>
    ctx.db.testingLabs({
      where: { company: { id: ctx.user.companyId } },
      orderBy: "updatedAt_DESC"
    }),

  user: (parent, args, ctx) => ctx.db.user({ id: ctx.user.uid })
};
