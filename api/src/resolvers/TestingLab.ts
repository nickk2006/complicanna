// This resolver file was scaffolded by github.com/prisma/graphqlgen, DO NOT EDIT.
// Please do not import this file directly but copy & paste to your application code.

import { TestingLabResolvers } from "../generated/resolvers";

export const TestingLab: TestingLabResolvers.Type = {
  ...TestingLabResolvers.defaultResolvers,

  company: (parent, args, ctx) =>
    ctx.db.testingLab({ id: parent.id }).company(),

  tests: (parent, args, ctx) => ctx.db.testingLab({ id: parent.id }).tests(),

  license: (parent, args, ctx) => ctx.db.testingLab({ id: parent.id }).license()
};
