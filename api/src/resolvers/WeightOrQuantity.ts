// This resolver file was scaffolded by github.com/prisma/graphqlgen, DO NOT EDIT.
// Please do not import this file directly but copy & paste to your application code.

import { WeightOrQuantityResolvers } from "../generated/resolvers";

export const WeightOrQuantity: WeightOrQuantityResolvers.Type = {
  ...WeightOrQuantityResolvers.defaultResolvers
};
