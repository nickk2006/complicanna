// This resolver file was scaffolded by github.com/prisma/graphqlgen, DO NOT EDIT.
// Please do not import this file directly but copy & paste to your application code.

import { TestResolvers } from "../generated/resolvers";

export const Test: TestResolvers.Type = {
  ...TestResolvers.defaultResolvers,

  lab: (parent, args, ctx) => ctx.db.test({ id: parent.id }).lab(),

  batch: (parent, args, ctx) => ctx.db.test({ id: parent.id }).batch(),

  certificateOfAnalysis: (parent, args, ctx) =>
    ctx.db.test({ id: parent.id }).certificateOfAnalysis(),

  chainOfCustody: (parent, args, ctx) =>
    ctx.db.test({ id: parent.id }).chainOfCustody(),

  company: (parent, args, ctx) => ctx.db.test({ id: parent.id }).company(),

  video: (parent, args, ctx) => ctx.db.test({ id: parent.id }).video()
};
