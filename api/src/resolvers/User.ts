// This resolver file was scaffolded by github.com/prisma/graphqlgen, DO NOT EDIT.
// Please do not import this file directly but copy & paste to your application code.

import { UserResolvers } from "../generated/resolvers";

export const User: UserResolvers.Type = {
  ...UserResolvers.defaultResolvers,

  company: (parent, args, ctx) => ctx.db.user({ id: parent.id }).company(),

  assignedBatches: (parent, args, ctx) =>
    ctx.db
      .user({ id: parent.id })
      .assignedBatches({ orderBy: "dateOfEntry_DESC" })
};
