import { Prisma } from "../generated/prisma-client";

const isBatchIdValid = (db: Prisma, batchId: string, companyId: string) =>
  db.$exists.batch({ id: batchId, company: { id: companyId } });

export async function validateBatches(
  batchIds: string[],
  db: Prisma,
  companyId: string
) {
  const isValidArray = await Promise.all(
    batchIds.map(id => isBatchIdValid(db, id, companyId))
  );

  const includesInvalidBatchId = isValidArray.includes(false);

  return !includesInvalidBatchId;
}
