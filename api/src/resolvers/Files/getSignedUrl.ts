import { Storage } from "@google-cloud/storage";
import { FileLocation } from "../../generated/prisma-client";

const read = async (storage: Storage, bucketName: string, filename: string) => {
  const bucket = storage.bucket(bucketName);

  const file = bucket.file(filename);

  const ONE_HOUR = 1000 * 60 * 60;
  const expires = Date.now() + ONE_HOUR;

  const [signedUrl] = await file.getSignedUrl({
    expires,
    action: "read"
  });

  return signedUrl;
};

export default { read };
