import { ReadStream } from "fs";
import { Storage } from "@google-cloud/storage";
import * as uuid from "uuid/v4";
import getSignedUrl from "./getSignedUrl";
import { FileLocationCreateInput } from "../../generated/prisma-client";

export interface IFileUploadOptions {
  filename: string;
  bucketName: string;
}

interface IUpload {
  createReadStream: () => ReadStream;
  filename: string;
  mimetype: string;
  encoding: string;
}

export type IUploadPromise = Promise<IUpload>;

export type UploadScalar = Array<Promise<IUpload>>;

export const fileUpload = async (
  uploadPromise: Promise<IUpload>,
  companyId: string,
  storage: Storage,
  bucketName = process.env.DEFAULT_STORAGE_BUCKET_NAME
) => {
  const { createReadStream, filename } = await uploadPromise;

  const uuidFilename = `${uuid()}-${filename}`;

  const stream = createReadStream();

  const options: IFileUploadOptions = {
    filename: uuidFilename,
    bucketName
  };

  const signedUrl = await uploadToGoogleCloud(stream, storage, options);

  const fileLocationCreateInput: FileLocationCreateInput = {
    bucketName,
    filename: uuidFilename,
    title: filename,
    company: { connect: { id: companyId } }
  };

  return {
    signedUrl,
    fileLocationCreateInput
  };
};

const uploadToGoogleCloud = async (
  readStream: ReadStream,
  storage: Storage,
  options: IFileUploadOptions
) => {
  const { bucketName, filename } = options;

  const bucket = storage.bucket(bucketName);

  const file = bucket.file(filename);

  const signedUrl = await getSignedUrl.read(storage, bucketName, filename);

  return new Promise<string>((resolve, reject) =>
    readStream
      .pipe(
        file.createWriteStream({
          gzip: true
        })
      )
      .on("error", e => {
        console.log("ERROR Uploading file in fileUpload function", e);
        reject(e);
      })
      .on("finish", () => resolve(signedUrl))
  );
};

// testUpload: async (parent, args, ctx) => {
//   const [testFilePromise] = (args.file.file as unknown) as FilePromise;

//   const testFile = await testFilePromise;

//   console.log("ARGS.FILE.FILE IS:\n");
//   console.dir(testFile);

//   const { createReadStream, filename, mimetype, encoding } = testFile;

//   console.log("TESTFILE IS");
//   console.dir(testFile);
//   const stream = createReadStream();

//   const uploadOptions: IFileUploadOptions = {
//     filename,
//     companyId: ctx.user.companyId,
//   };

//   const storageName = await fileUpload(stream, ctx.storage, uploadOptions)
//     .then(storageName => {
//       console.log("STOREAGE NAME IS", storageName);
//       return storageName;
//     })
//     .catch(e => {
//       console.error(e);
//       throw new Error(e);
//     });

//   return storageName;
// }
