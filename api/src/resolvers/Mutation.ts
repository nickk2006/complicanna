// This resolver file was scaffolded by github.com/prisma/graphqlgen, DO NOT EDIT.
// Please do not import this file directly but copy & paste to your application code.

import { MutationResolvers } from "../generated/resolvers";
import { resolvers as auth } from "./Auth";
import { resolvers as batch } from "./Mutations/Batch";
import { resolvers as demo } from "./Mutations/Demo";
import * as slug from "slug";
import * as testMutations from "./Mutations/Tests";
import { validateBatches } from "./helpers";

export const Mutation: MutationResolvers.Type = {
  ...MutationResolvers.defaultResolvers,
  signUpAdmin: auth.signUpAdmin,

  assignUserToCompany: auth.assignUserToCompany,

  createBatch: batch.createBatch,

  createLicenseHolder: (parent, { data }, ctx) => {
    const nameSlug = slug(data.name, { lower: true });
    const { companyId } = ctx.user;
    return ctx.db.createLicenseHolder({
      name: data.name,
      nameSlug,
      license: {
        create: {
          number: data.license.number,
          type: data.license.type,
          company: {
            connect: { id: companyId }
          }
        }
      },
      company: { connect: { id: companyId } }
    });
  },

  createTest: (parent, { data }, ctx) =>
    ctx.db.createTest({
      company: { connect: { id: ctx.user.companyId } },
      lab: { connect: { id: data.labId } },
      batch: { connect: { id: data.batchId } },
      sampleDate: data.sampleDate
    }),

  createTestingLab: (parent, { data }, ctx) => {
    const nameSlug = slug(data.name, { lower: true });
    const { companyId } = ctx.user;

    return ctx.db.createTestingLab({
      name: data.name,
      nameSlug,
      email: data.email,
      phone: data.phone,
      company: { connect: { id: companyId } }
    });
  },

  createWeightOrQuantity: (parent, args, ctx) =>
    ctx.db.createWeightOrQuantity(args.data),

  updateTest: testMutations.resolvers.updateTest,

  updateTodos: async (parent, { addIds, removeIds }, ctx) => {
    const { db, user } = ctx;

    const isValid = await validateBatches(
      [...addIds, ...removeIds],
      db,
      user.companyId
    );

    if (!isValid) throw new Error("Invalid batch included");

    const connect = addIds.map(id => ({ id }));

    const disconnect = removeIds.map(id => ({ id }));

    await db.updateUser({
      where: { id: ctx.user.uid },
      data: {
        assignedBatches: {
          connect,
          disconnect
        }
      }
    });

    return true;
  },

  requestDemo: demo.requestDemo
};
