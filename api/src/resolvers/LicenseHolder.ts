// This resolver file was scaffolded by github.com/prisma/graphqlgen, DO NOT EDIT.
// Please do not import this file directly but copy & paste to your application code.

import { LicenseHolderResolvers } from "../generated/resolvers";

export const LicenseHolder: LicenseHolderResolvers.Type = {
  ...LicenseHolderResolvers.defaultResolvers,

  license: (parent, args, ctx) =>
    ctx.db.licenseHolder({ id: parent.id }).license(),
  company: (parent, args, ctx) =>
    ctx.db.licenseHolder({ id: parent.id }).company(),
  batchesSent: (parent, args, ctx) =>
    ctx.db.licenseHolder({ id: parent.id }).batchesSent(),
  batchesReceived: (parent, args, ctx) =>
    ctx.db.licenseHolder({ id: parent.id }).batchesReceived()
};
