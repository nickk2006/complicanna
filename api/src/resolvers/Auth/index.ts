import { MutationResolvers } from "../../generated/resolvers";
import * as slug from "slug";
import * as admin from "firebase-admin";
import { Company, User } from "../../generated/prisma-client";
import { ICustomClaims } from "./types";

export const resolvers: Partial<MutationResolvers.Type> = {
  signUpAdmin: async (parent, { input }, ctx) => {
    const { companyName, firstName, lastName, email, password } = input;
    const nameSlug = slug(input.companyName, { lower: true });

    let company: Company | undefined = undefined;
    let firebaseUser: admin.auth.UserRecord | undefined = undefined;
    let user: User | undefined = undefined;

    try {
      company = await ctx.db.createCompany({
        name: companyName,
        nameSlug
      });

      console.log("COMPONANY", company);

      user = await ctx.db.createUser({
        firstName,
        lastName,
        email,
        company: { connect: { id: company.id } }
      });

      firebaseUser = await ctx.firebase.auth().createUser({
        email,
        emailVerified: false,
        password,
        uid: user.id
      });

      const claims: ICustomClaims = {
        companyId: company.id,
        isAdmin: true
      };

      await ctx.firebase.auth().setCustomUserClaims(firebaseUser.uid, claims);

      return company;
    } catch (e) {
      console.error(e);
      if (typeof firebaseUser !== "undefined") {
        await ctx.firebase.auth().deleteUser(firebaseUser.uid);
        console.log("DELETED FIREBSE USER\n");
      }

      if (typeof company !== "undefined") {
        await ctx.db.deleteCompany({ id: company.id });
        console.log("DELETED COMPANY\n");
      }

      if (typeof user !== "undefined") {
        await ctx.db.deleteUser({ id: user.id });
        console.log("DELETED USER\n");
      }
      throw e;
    }
  },
  assignUserToCompany: async (parent, { userEmail }, { firebase, user }) => {
    try {
      const { uid } = await firebase.auth().getUserByEmail(userEmail);

      const claims: ICustomClaims = {
        companyId: user.companyId,
        isAdmin: false
      };

      await firebase.auth().setCustomUserClaims(uid, claims);
      return true;
    } catch (e) {
      console.log("ERROR with assignUserToCompany resolver", e);
      return false;
    }
  }
};
