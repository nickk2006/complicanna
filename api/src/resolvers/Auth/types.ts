export interface ICustomClaims {
  companyId: string;
  isAdmin: boolean;
}
