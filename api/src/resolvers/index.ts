// This resolver file was scaffolded by github.com/prisma/graphqlgen, DO NOT EDIT.
// Please do not import this file directly but copy & paste to your application code.

import { Resolvers } from "../generated/resolvers";

import { Query } from "./Query";
import { Company } from "./Company";
import { Address } from "./Address";
import { User } from "./User";
import { LicenseHolder } from "./LicenseHolder";
import { License } from "./License";
import { TestingLab } from "./TestingLab";
import { Test } from "./Test";
import { Batch } from "./Batch";
import { WeightOrQuantity } from "./WeightOrQuantity";
import { FileLocation } from "./FileLocation";
import { Mutation } from "./Mutation";

export const resolvers: Resolvers = {
  Query,
  Company,
  Address,
  User,
  LicenseHolder,
  License,
  TestingLab,
  Test,
  Batch,
  WeightOrQuantity,
  FileLocation,
  Mutation
};
