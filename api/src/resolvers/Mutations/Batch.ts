import { MutationResolvers } from "../../generated/resolvers";

export const resolvers: Partial<MutationResolvers.Type> = {
  createBatch: (parent, { data }, ctx) =>
    ctx.db.createBatch({
      batchNumber: data.batchNumber,
      description: data.description,
      weightOrQuantity: {
        create: {
          magnitude: data.weightOrQuantity.magnitude,
          unit: data.weightOrQuantity.unit
        }
      },
      dateOfEntry: data.dateOfEntry,
      from: { connect: { id: data.from } },
      for: { connect: { id: data.for } },
      company: { connect: { id: ctx.user.companyId } }
    })
};
