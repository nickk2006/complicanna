import { MutationResolvers } from "../../generated/resolvers";
import * as shortid from "shortid";
import { createWriteStream } from "fs";
import {
  TestUpdateInput,
  FileLocationUpdateOneInput
} from "../../generated/prisma-client";
import { IContext } from "../types/Context";
import { fileUpload, IUploadPromise } from "../Files";
import { Storage } from "@google-cloud/storage";

const storeUpload = async ({ stream, filename }: any): Promise<any> => {
  const id = shortid.generate();
  const path = `forms/${id}-${filename}`;

  return new Promise((res, rej) =>
    stream
      .pipe(createWriteStream(path))
      .on("finish", () => res({ id, path }))
      .on("error", rej)
  );
};

const webFileToApiFile = (file: any) => {
  console.log("FILE type", typeof file);
  console.dir(file);

  const filePromise = (file as unknown) as IUploadPromise;

  return filePromise;
};

const handleFileUpdate = async (
  companyId: string,
  storage: Storage,
  file?: any
): Promise<FileLocationUpdateOneInput | undefined> => {
  if (file) {
    const filePromise = webFileToApiFile(file);
    const res = await fileUpload(filePromise, companyId, storage);
    const updateData: FileLocationUpdateOneInput = {
      create: res.fileLocationCreateInput
    };
    return updateData;
  } else {
    return undefined;
  }
};

export const resolvers: Partial<MutationResolvers.Type> = {
  updateTest: async (parent, { data, testId }, ctx) => {
    const {
      storage,
      user: { companyId }
    } = ctx;

    const {
      certificateOfAnalysis, //Files
      video, //Files
      chainOfCustody //Files
    } = data;

    const updateData: TestUpdateInput = {
      lab: { connect: { id: data.labId } },
      batch: { connect: { id: data.batchId } },
      sampleDate: data.sampleDate,
      resultDate: data.resultDate,
      result: data.result
    };

    const coaFile = await handleFileUpdate(
      companyId,
      storage,
      certificateOfAnalysis
    );

    const videoFile = await handleFileUpdate(companyId, storage, video);

    const cocFile = await handleFileUpdate(companyId, storage, chainOfCustody);

    updateData.certificateOfAnalysis = coaFile;
    updateData.video = videoFile;
    updateData.chainOfCustody = cocFile;

    console.log("updateData", updateData);

    return ctx.db.updateTest({ data: updateData, where: { id: testId } });
  }
};
