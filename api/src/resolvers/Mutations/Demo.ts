import { MutationResolvers } from "../../generated/resolvers";
import * as sgMail from "@sendgrid/mail";
import { MailData } from "@sendgrid/helpers/classes/mail";

export const resolvers: Partial<MutationResolvers.Type> = {
  requestDemo: async (parent, args, ctx) => {
    const { data } = args;

    const msg: MailData = {
      to: process.env.DEMO_REQUEST_EMAIL,
      from: data.email,
      subject: `Demo Request - ${data.companyName}`,
      text: `First Name: ${data.firstName}\n Last Name: ${
        data.lastName
      }\n Company Name: ${data.companyName}\n Email: ${data.email}`
    };

    try {
      await sgMail.send(msg);
      return true;
    } catch (e) {
      console.log(`ERROR sending email for ${JSON.stringify(args.data)}\n${e}`);
      return false;
    }
  }
};
