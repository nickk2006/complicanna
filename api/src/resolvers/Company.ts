// This resolver file was scaffolded by github.com/prisma/graphqlgen, DO NOT EDIT.
// Please do not import this file directly but copy & paste to your application code.

import { CompanyResolvers } from "../generated/resolvers";

export const Company: CompanyResolvers.Type = {
  ...CompanyResolvers.defaultResolvers,

  address: (parent, args, ctx) => ctx.db.company({ id: parent.id }).address(),

  users: (parent, args, ctx) => ctx.db.company({ id: parent.id }).users(),

  licenses: (parent, args, ctx) => ctx.db.company({ id: parent.id }).licenses(),

  licenseHolders: (parent, args, ctx) =>
    ctx.db.company({ id: parent.id }).licenseHolders(),

  testingLabs: (parent, args, ctx) =>
    ctx.db.company({ id: parent.id }).testingLabs(),

  batches: (parent, args, ctx) => ctx.db.company({ id: parent.id }).batches(),

  tests: (parent, args, ctx) => ctx.db.company({ id: parent.id }).tests(),

  forms: (parent, args, ctx) => ctx.db.company({ id: parent.id }).forms()
};
