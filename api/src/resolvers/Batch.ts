// This resolver file was scaffolded by github.com/prisma/graphqlgen, DO NOT EDIT.
// Please do not import this file directly but copy & paste to your application code.

import { BatchResolvers } from "../generated/resolvers";

export const Batch: BatchResolvers.Type = {
  ...BatchResolvers.defaultResolvers,

  weightOrQuantity: (parent, args, ctx) =>
    ctx.db.batch({ id: parent.id }).weightOrQuantity(),

  tests: (parent, args, ctx) => ctx.db.batch({ id: parent.id }).tests(),

  from: (parent, args, ctx) => ctx.db.batch({ id: parent.id }).from(),

  for: (parent, args, ctx) => ctx.db.batch({ id: parent.id }).for(),

  company: (parent, args, ctx) => ctx.db.batch({ id: parent.id }).company(),

  assignedUser: (parent, args, ctx) =>
    ctx.db.batch({ id: parent.id }).assignedUser()
};
