// This resolver file was scaffolded by github.com/prisma/graphqlgen, DO NOT EDIT.
// Please do not import this file directly but copy & paste to your application code.

import { LicenseResolvers } from "../generated/resolvers";

export const License: LicenseResolvers.Type = {
  ...LicenseResolvers.defaultResolvers,

  company: (parent, args, ctx) => ctx.db.license({ id: parent.id }).company()
};
