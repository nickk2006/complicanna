// This resolver file was scaffolded by github.com/prisma/graphqlgen, DO NOT EDIT.
// Please do not import this file directly but copy & paste to your application code.

import { FileLocationResolvers } from "../generated/resolvers";
import { getSignedUrl } from "./Files";

export const FileLocation: FileLocationResolvers.Type = {
  ...FileLocationResolvers.defaultResolvers,

  signedUrl: async (parent, args, ctx) => {
    let fileLocation = parent;

    const isFilenameUndefined = typeof fileLocation.filename === "undefined";
    const isBucketNameUndefined =
      typeof fileLocation.bucketName === "undefined";

    if (isFilenameUndefined || isBucketNameUndefined) {
      fileLocation = await ctx.db.fileLocation({ id: parent.id });
    }

    const signedUrl = await getSignedUrl.read(
      ctx.storage,
      fileLocation.bucketName,
      fileLocation.filename
    );

    return signedUrl;
  }
};
