import { Prisma } from "../../generated/prisma-client";
import * as admin from "firebase-admin";
import { Storage } from "@google-cloud/storage";
import { IFirebaseToken } from "../../google";

// export interface IFirebaseToken {
//   uid: string;
//   companyId: string;
//   isAdmin: boolean;
// }

export interface IContext {
  db: Prisma;
  request: any;
  firebase: typeof admin;
  user: IFirebaseToken;
  storage: Storage;
}
