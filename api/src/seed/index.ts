import * as path from "path";
const dotenvPath =
  process.env.NODE_ENV === "production"
    ? path.resolve(process.cwd(), ".seed-production.env")
    : path.resolve(process.cwd(), ".env");
require("dotenv").config({ path: dotenvPath }); // tslint:disable-line
import { Prisma } from "../generated/prisma-client";
import * as admin from "firebase-admin";

import * as _ from "lodash";
import * as faker from "faker";
import * as fs from "fs";
import * as shortid from "shortid";
import { keys } from "../constants";

import {
  createCompany,
  createLicense,
  createLicenseHolder,
  createTestingLab,
  createBatch,
  createTest,
  createUser,
  createFirebaseUser,
  IFirebaseUserOptions
} from "./create";

const db = new Prisma({
  endpoint: keys.PRISMA_ENDPOINT,
  secret: keys.PRISMA_MANAGEMENT_API_SECRET!
});

admin.initializeApp({
  credential: admin.credential.cert({
    projectId: keys.FIREBASE_PROJECT_ID,
    clientEmail: keys.FIREBASE_CLIENT_EMAIL,
    privateKey: keys.FIREBASE_PRIVATE_KEY
  }),
  databaseURL: keys.FIREBASE_DATABASE_URL
});

const createResourcesForCompany = async (companyId: string, userId: string) => {
  const firebaseUser2: IFirebaseUserOptions = {
    email: `${shortid.generate()}@complicanna.com`,
    password: faker.internet.password()
  };

  const firebaseUser3: IFirebaseUserOptions = {
    email: `${shortid.generate()}@complicanna.com`,
    password: faker.internet.password()
  };

  const license1 = await createLicense(db, companyId);
  const licenseId1 = license1.id;
  const licenseHolder1 = await createLicenseHolder(db, companyId, licenseId1);

  const license2 = await createLicense(db, companyId);
  const licenseId2 = license2.id;
  const licenseHolder2 = await createLicenseHolder(db, companyId, licenseId2);

  const testingLab = await createTestingLab(db, companyId);

  const user2 = await createUser(db, companyId, firebaseUser2);
  const user3 = await createUser(db, companyId, firebaseUser3);

  const batchPromise = () => {
    const randomUserId = faker.random.arrayElement([
      user2.id,
      user3.id,
      userId
    ]);
    return new Promise((resolve, reject) => {
      createBatch(
        db,
        companyId,
        licenseHolder1.id,
        licenseHolder2.id,
        randomUserId
      ).then(async batch => {
        await createTest({
          db,
          companyId,
          labId: testingLab.id,
          batchId: batch.id
        });

        if (faker.random.number({ min: 0, max: 10, precision: 1 }) >= 5) {
          db.updateUser({
            where: { id: userId },
            data: { assignedBatches: { connect: { id: batch.id } } }
          }).then(() => resolve());
        } else {
          resolve();
        }
      });
    });
  };

  const batchPromises = _.times(
    // faker.random.number({ min: 50, max: 60, precision: 1 }),
    15,
    batchPromise
  );

  await Promise.all(batchPromises);
};

const saveLogin = (login: IFirebaseUserOptions) =>
  new Promise((resolve, reject) => {
    const loginString = JSON.stringify(login);
    const encoded = Buffer.from(loginString).toString("base64");

    const stream = fs.createWriteStream("demo-data.txt", { flags: "a" });
    stream.write(`${loginString}\n${encoded}\n\n`);
    stream.on("close", resolve);
  });

const createAccount = async () => {
  const email = `${shortid.generate()}@complicanna.com`;
  const password = faker.random.alphaNumeric(10);
  const login: IFirebaseUserOptions = { email, password };
  // const oldFirebaseUser = await admin
  //   .auth()
  //   .getUserByEmail("demo@complicanna.com");
  // await admin.auth().deleteUser(oldFirebaseUser.uid);
  try {
    const company = await createCompany(db);
    const companyId = company.id;

    const user = await createUser(db, companyId, login);
    await createResourcesForCompany(companyId, user.id);

    await saveLogin(login);
    console.log("LOGIN", login);
    console.log("DONE");
  } catch (e) {
    console.error(e);
    process.exit(1);
  }
  // const user = await admin.auth().getUser("jwaIk9WQcofe2dZZ63fBIGH703w2");
  // console.log(user);
};

// _.times(10, createAccount);
const main = async () => {
  try {
    await createAccount();
    await createAccount();
    await createAccount();
    await createAccount();
    await createAccount();
    setInterval(process.exit(0), 10000);
  } catch (e) {
    console.error(e);
  }
};

main();
