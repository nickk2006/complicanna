import * as faker from "faker";
import { IUser } from "../seed.types";
import * as admin from "firebase-admin";
import { ICustomClaims } from "../../resolvers/Auth/types";

export const createFirebaseUser = async (
  companyId: string,
  email: string,
  password: string
) => {
  const claims: ICustomClaims = {
    isAdmin: faker.random.boolean(),
    companyId
  };

  const firebaseUser = await admin.auth().createUser({ email, password });

  await admin.auth().setCustomUserClaims(firebaseUser.uid, claims);

  return firebaseUser;
};

export default createFirebaseUser;
