import {
  Prisma,
  FileLocationCreateOneInput,
  TestResult
} from "../../generated/prisma-client";
import * as faker from "faker";
import * as moment from "moment";
import * as uuid from "uuid/v4";
import { createStorage } from "../../google";
import { fileUpload } from "../../resolvers/Files/upload";
import * as fs from "fs";
import * as path from "path";
import { keys } from "../../constants";

interface ICreateTestInputs {
  db: Prisma;
  companyId: string;
  labId: string;
  batchId: string;
}

const createTest = async ({
  db,
  companyId,
  labId,
  batchId
}: ICreateTestInputs) => {
  const sampleDate = faker.date.recent(5);
  const resultDate = faker.random.boolean()
    ? faker.date.between(sampleDate, moment(Date.now()).format())
    : null;

  const videoFilename = `${uuid()}.webm`;
  const coaFilename = `${uuid()}.pdf`;
  const custodyFilename = `${uuid()}.pdf`;

  const videoPath = path.join(__dirname, "..", "files", "video.pdf");
  const coaPath = path.join(__dirname, "..", "files", "COA.pdf");
  const custodyPath = path.join(__dirname, "..", "files", "Custody.pdf");

  const videoPromise = uploadFile(videoPath, videoFilename);
  const coaPromise = uploadFile(coaPath, coaFilename);
  const custodyPromise = uploadFile(custodyPath, custodyFilename);

  await Promise.all([videoPromise, coaPromise, custodyPromise]);

  const video: FileLocationCreateOneInput = {
    create: {
      company: { connect: { id: companyId } },
      filename: videoFilename, // tslint:disable-line
      bucketName: "vangoh-files",
      title:
        "stock-footage-coworkers-discussing-in-the-future-of-their-company-over-several-charts-and-graphs-business.webm" // tslint:disable-line
    }
  };

  const certificateOfAnalysis: FileLocationCreateOneInput = {
    create: {
      company: { connect: { id: companyId } },
      filename: coaFilename, // tslint:disable-line
      bucketName: "vangoh-files",
      title: "Certificate_Of_Analysis_Example.pdf" // tslint:disable-line
    }
  };

  const chainOfCustody: FileLocationCreateOneInput = {
    create: {
      company: { connect: { id: companyId } },
      filename: custodyFilename, // tslint:disable-line
      bucketName: "vangoh-files",
      title: "Sample_Chain_of_Custody_Form.pdf" // tslint:disable-line
    }
  };

  const result = faker.random.arrayElement<TestResult>([
    "PASSED",
    "FAILED",
    "PENDING"
  ]);

  const test = await db.createTest({
    lab: { connect: { id: labId } },
    batch: { connect: { id: batchId } },
    certificateOfAnalysis,
    chainOfCustody,
    company: { connect: { id: companyId } },
    sampleDate,
    resultDate,
    video,
    result
  });

  return test;
};

const uploadFile = (pathUrl: string, filename: string) => {
  const storage = createStorage();

  const bucket = storage.bucket(keys.DEFAULT_STORAGE_BUCKET_NAME);

  const file = bucket.file(filename);

  const readStream = fs.createReadStream(pathUrl);

  return new Promise((resolve, reject) =>
    readStream
      .pipe(file.createWriteStream({ gzip: true }))
      .on("error", e => {
        console.log("ERROR Uploading file in fileUpload function", e);
        reject(e);
      })
      .on("finish", () => {
        // console.log("Uploaded to google cloud:", filename);
        resolve();
      })
  );
};

export default createTest;
