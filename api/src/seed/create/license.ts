import {
  Prisma,
  CompanyCreateOneWithoutLicensesInput
} from "../../generated/prisma-client";
import * as faker from "faker";

const createLicense = async (db: Prisma, companyId: string) => {
  const number = faker.random.uuid();
  const type = faker.random.number({ min: 1, max: 7, precision: 1 });
  const company: CompanyCreateOneWithoutLicensesInput = {
    connect: { id: companyId }
  };

  const license = await db.createLicense({ number, type, company });

  return license;
};

export default createLicense;
