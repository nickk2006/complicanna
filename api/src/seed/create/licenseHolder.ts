import {
  Prisma,
  CompanyCreateOneWithoutLicenseHoldersInput,
  LicenseCreateOneInput
} from "../../generated/prisma-client";
import * as faker from "faker";

const createLicenseHolder = async (
  db: Prisma,
  companyId: string,
  licenseId: string
) => {
  const name = faker.company.companyName();
  const nameSlug = name.toLowerCase();
  const license: LicenseCreateOneInput = { connect: { id: licenseId } };
  const company: CompanyCreateOneWithoutLicenseHoldersInput = {
    connect: { id: companyId }
  };

  const licenseHolder = await db.createLicenseHolder({
    name,
    nameSlug,
    license,
    company
  });

  return licenseHolder;
};

export default createLicenseHolder;
