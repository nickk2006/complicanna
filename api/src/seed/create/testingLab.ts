import {
  Prisma,
  CompanyCreateOneWithoutTestingLabsInput
} from "../../generated/prisma-client";
import * as faker from "faker";

const createTestingLab = async (db: Prisma, companyId: string) => {
  const name = faker.company.companyName();
  const nameSlug = name.toLowerCase();
  const email = faker.internet.email();
  const phone = faker.phone.phoneNumber();
  const company: CompanyCreateOneWithoutTestingLabsInput = {
    connect: { id: companyId }
  };

  const testingLab = await db.createTestingLab({
    name,
    nameSlug,
    email,
    phone,
    company
  });

  return testingLab;
};

export default createTestingLab;
