import {
  Prisma,
  WeightOrQuantityCreateOneInput,
  Unit,
  UserCreateOneWithoutAssignedBatchesInput,
  LicenseHolderCreateOneWithoutBatchesSentInput,
  LicenseHolderCreateOneWithoutBatchesReceivedInput,
  CompanyCreateOneWithoutBatchesInput
} from "../../generated/prisma-client";
import * as faker from "faker";
import * as shortid from "shortid";

const createBatch = async (
  db: Prisma,
  companyId: string,
  licenseHolderFromId: string,
  licenseHolderToId: string,
  assignedUserId: string
) => {
  const units: Unit[] = ["KG", "UNITS", "G", "LBS"];

  const dateOfEntry = faker.date.recent(5);
  const batchNumber = shortid.generate();
  const description = faker.lorem.lines(1);
  const weightOrQuantity: WeightOrQuantityCreateOneInput = {
    create: {
      magnitude: faker.random.number({ min: 0, max: 100, precision: 0.1 }),
      unit: faker.random.arrayElement(units)
    }
  };
  const bestBy = faker.date.future(1);
  const sellBy = faker.date.between(dateOfEntry, bestBy);
  const from: LicenseHolderCreateOneWithoutBatchesSentInput = {
    connect: { id: licenseHolderFromId }
  };
  const forLicenseHolder: LicenseHolderCreateOneWithoutBatchesReceivedInput = {
    connect: { id: licenseHolderToId }
  };
  const company: CompanyCreateOneWithoutBatchesInput = {
    connect: { id: companyId }
  };

  const assignedUser: UserCreateOneWithoutAssignedBatchesInput = {
    connect: { id: assignedUserId }
  };

  const batch = await db.createBatch({
    dateOfEntry,
    batchNumber,
    description,
    weightOrQuantity,
    bestBy,
    sellBy,
    from,
    for: forLicenseHolder,
    company,
    assignedUser
  });

  return batch;
};

export default createBatch;
