import {
  Prisma,
  AddressCreateOneWithoutCompanyInput,
  CompanyType,
  SubscriptionType
} from "../../generated/prisma-client";

import * as faker from "faker";

const createCompany = async (db: Prisma) => {
  const name = faker.company.companyName();
  const nameSlug = name.toLowerCase();
  const address: AddressCreateOneWithoutCompanyInput = {
    create: {
      lineOne: faker.address.streetAddress(),
      city: faker.address.city(),
      state: faker.address.stateAbbr(),
      zip: faker.address.zipCode()
    }
  };
  const type: CompanyType = "DISTRIBUTOR";
  const subscriptionType: SubscriptionType = "BASIC";

  const company = await db.createCompany({
    name,
    nameSlug,
    address,
    type,
    subscriptionType
  });

  return company;
};

export default createCompany;
