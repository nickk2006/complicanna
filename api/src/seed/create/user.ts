import { Prisma } from "../../generated/prisma-client";
import * as faker from "faker";
import * as admin from "firebase-admin";

export interface IFirebaseUserOptions {
  email: string;
  password: string;
}

const createUser = async (
  db: Prisma,
  companyId: string,
  firebaseUserOptions: IFirebaseUserOptions
) => {
  const firstName = faker.name.firstName();
  const lastName = faker.name.lastName();
  const employeeId = faker.random.uuid();

  const user = await db.createUser({
    firstName,
    lastName,
    employeeId,
    email: firebaseUserOptions.email,
    company: { connect: { id: companyId } }
  });

  const fbUser = await admin.auth().createUser({
    email: firebaseUserOptions.email,
    password: firebaseUserOptions.password,
    uid: user.id
  });

  await admin.auth().setCustomUserClaims(fbUser.uid, {
    isAdmin: true,
    companyId
  });

  return user;
};

export default createUser;
