import { keys } from "./constants";
import { Prisma } from "./generated/prisma-client";

const db = new Prisma({
  endpoint: keys.PRISMA_ENDPOINT!,
  secret: keys.PRISMA_MANAGEMENT_API_SECRET!,
  debug: true
});

export default db;
