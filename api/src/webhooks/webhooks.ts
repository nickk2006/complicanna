import { Request, Response } from "express";
import stripe from "./stripe";

const handleWebhooks = async (req: Request, res: Response) => {
  const stripeSig = req.headers["stripe-signature"];
  try {
    if (typeof stripeSig === "string") {
      await stripe(req.body, stripeSig);
    }

    res.json({ received: true });
  } catch (err) {
    res.status(400).send(err);
  }
};

export default handleWebhooks;
