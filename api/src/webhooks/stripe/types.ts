interface IDisplayItems {
  amount: number;
  currency: string;
  plan: IPlan;
  quantity: number;
  type: string;
}

export interface IPlan {
  id: string;
  object: string;
  active: boolean;
  aggregate_usage?: any;
  amount: number;
  billing_scheme: string;
  created: number;
  currency: string;
  interval: string;
  interval_count: number;
  livemode: boolean;
  metadata: object;
  nickname: string;
  product: string;
  tiers?: any;
  tiers_mode?: any;
  transform_usage?: any;
  trial_period_days?: any;
  usage_type: string;
}

export interface ICheckoutPayload {
  id: string;
  object: string;
  billing_address_collection?: any;
  cancel_url: string;
  client_reference_id?: any;
  customer: string;
  customer_email: string;
  display_items: IDisplayItems[];
  livemode: boolean;
  locale?: any;
  payment_intent?: any;
  payment_method_types: string[];
  subscription: string;
  success_url: string;
}
