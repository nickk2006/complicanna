import * as Stripe from "stripe";
import { ICheckoutPayload, IPlan } from "./types";
import { keys } from "../../constants";
import { SubscriptionType, Prisma } from "../../generated/prisma-client";
import db from "../../prisma";
import * as admin from "firebase-admin";
import { ICustomClaims } from "../../google";

const getSubscriptionFromPlan = (plan: IPlan): SubscriptionType => {
  const planId = plan.id;

  if (typeof plan === "undefined") {
    console.error("Plan is undefined in getSubscriptionFromPlanId");
    throw new Error("Plan is undefined in getSubscriptionFromPlanId");
  }

  if (planId === keys.STRIPE_STATEWIDE_PLAN_ID) {
    return "STATEWIDE";
  }
  if (planId === keys.STRIPE_REGIONAL_PLAN_ID) {
    return "REGIONAL";
  }
  return "BASIC";
};

const getCompanyIdFromUserEmail = async (email: string) => {
  const user = await admin.auth().getUserByEmail(email);

  const { companyId } = user.customClaims as ICustomClaims;

  return companyId;
};

const updateCompanySubscription = (
  companyId: string,
  prisma: Prisma,
  newSubscriptionType: SubscriptionType
) =>
  prisma.updateCompany({
    where: { id: companyId },
    data: { subscriptionType: newSubscriptionType }
  });

const handleCheckout = async (payload: Stripe.IObject) => {
  const checkoutPayload = payload as ICheckoutPayload;

  const { customer_email: email, display_items } = checkoutPayload;

  const { plan } = display_items[0];

  const subscriptionType = getSubscriptionFromPlan(plan);

  const companyId = await getCompanyIdFromUserEmail(email);

  const updatedCompany = await updateCompanySubscription(
    companyId,
    db,
    subscriptionType
  );

  console.log("UPDATED COMPANY", updatedCompany);

  return true;
};

export default handleCheckout;
