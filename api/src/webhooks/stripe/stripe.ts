import stripe from "../../stripe";
import * as Stripe from "stripe";

import handleCheckout from "./handleCheckout";

const verifySignature = async (payload: object, sig: string) => {
  const endpointSecret = process.env.STRIPE_WEBHOOK_SECRET;

  let event: Stripe.events.IEvent;

  try {
    event = stripe.webhooks.constructEvent(payload, sig, endpointSecret);

    switch (event.type) {
      case "checkout.session.completed":
        const checkoutSession = event.data.object;
        await handleCheckout(checkoutSession);
        break;
      default:
        throw new Error(`Incorrect Stipe payload, ${event}`);
    }
  } catch (err) {
    throw new Error(`Webhook Error: ${err.message}`);
  }
};

export default verifySignature;
