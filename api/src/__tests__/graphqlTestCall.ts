import { graphql } from "graphql";
import { makeExecutableSchema } from "graphql-tools";
import { resolvers } from "../resolvers";
import { IFirebaseToken } from "../google";

const schema = makeExecutableSchema({
  typeDefs: "../src/schema.graphql",
  resolvers: resolvers as any
});

const DEFAULT_USER_INFO: IFirebaseToken = {
  uid: "TEST_USER_ID",
  companyId: "TEST_COMPANY_ID",
  isAdmin: false
};

const graphqlTestCall = async (
  query: any,
  variables?: any,
  context = DEFAULT_USER_INFO
) =>
  graphql(
    schema,
    query,
    undefined,
    {
      user: context
    },
    variables
  );
