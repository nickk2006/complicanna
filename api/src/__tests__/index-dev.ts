require("dotenv").config(); // tslint:disable-line
import * as admin from "firebase-admin";
import { GraphQLServer } from "graphql-yoga";
import { Prisma } from "../generated/prisma-client";
import { resolvers } from "../resolvers";
import { ContextParameters } from "graphql-yoga/dist/types";
import permissions from "../permissions";
import { keys } from "../constants";

// admin.initializeApp({
//   credential: admin.credential.cert(serviceAccount),
//   databaseURL: "https://vangoh-63e57.firebaseio.com"
// });

admin.initializeApp({
  credential: admin.credential.cert({
    projectId: keys.FIREBASE_PROJECT_ID,
    clientEmail: keys.FIREBASE_CLIENT_EMAIL,
    privateKey: keys.FIREBASE_PRIVATE_KEY
  }),
  databaseURL: keys.FIREBASE_DATABASE_URL
});

const db = new Prisma({
  endpoint: process.env.PRISMA_ENDPOINT!,
  secret: process.env.PRISMA_SECRET!,
  debug: true
});

const getUserDev = async (
  resolve: any,
  root: any,
  args: any,
  ctx: ContextParameters,
  info: any
) => {
  const user = {
    uid: "ABCUID",
    companyId: "ABCCOMPANYID",
    isAdmin: true
  };
  return resolve(root, args, { user, ...ctx }, info);
};
const server = new GraphQLServer({
  typeDefs: "./src/generated/prisma-schema-dev/schema.graphql",
  resolvers: resolvers as any,
  mocks: true,
  middlewares: [getUserDev],
  context: req => ({ db, firebase: admin, ...req })
});

server.start({ playground: "/graphiql" }, ({ port }) =>
  console.log(`Server is running on http://localhost:${port}`)
);
