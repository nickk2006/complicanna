import { rule, deny, allow } from "graphql-shield";
import { IContext } from "../resolvers/types/Context";
import { QueryResolvers } from "../generated/resolvers";

const isCompanyEmployee = rule({ cache: "contextual" })(
  (parent, args: QueryResolvers.ArgsCompany, ctx: IContext, info) =>
    ctx.db.$exists.company({
      id: ctx.user.companyId
    })
);

const batch = rule({ cache: "strict" })(
  (parent, args: QueryResolvers.ArgsBatch, ctx: IContext, info) =>
    ctx.db.$exists.batch({
      id: args.where.id,
      company: { id: ctx.user.companyId }
    })
);

const license = rule({ cache: "strict" })(
  (parent, args: QueryResolvers.ArgsLicense, ctx: IContext, info) =>
    ctx.db.$exists.license({
      id: args.where.id,
      company: { id: ctx.user.companyId }
    })
);

const licenseHolder = rule({ cache: "strict" })(
  (parent, args: QueryResolvers.ArgsLicenseHolder, ctx: IContext, info) =>
    ctx.db.$exists.licenseHolder({
      id: args.where.id,
      company: { id: ctx.user.companyId }
    })
);

const test = rule({ cache: "strict" })(
  (parent, args: QueryResolvers.ArgsTest, ctx: IContext, info) =>
    ctx.db.$exists.test({
      id: args.where.id,
      company: { id: ctx.user.companyId }
    })
);

const testingLab = rule({ cache: "strict" })(
  (parent, args: QueryResolvers.ArgsTestingLab, ctx: IContext, info) =>
    ctx.db.$exists.testingLab({
      id: args.where.id,
      company: { id: ctx.user.companyId }
    })
);

const query = {
  "*": deny,
  company: isCompanyEmployee,
  batches: allow,
  licenseHolders: isCompanyEmployee,
  tests: isCompanyEmployee,
  testingLabs: isCompanyEmployee,
  user: allow,
  license,
  batch,
  licenseHolder,
  test,
  testingLab
};

export default query;
