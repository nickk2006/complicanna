import { rule, shield, allow, deny, and } from "graphql-shield";
import { IContext } from "../resolvers/types/Context";
import Query from "./queryPermissions";

const isAdmin = rule()((parent, args, ctx: IContext) => ctx.user.isAdmin);

const isPaidSubscriber = rule({ cache: "strict" })(
  (parent, args, ctx: IContext, info) =>
    !ctx.db.$exists.company({
      id: ctx.user.companyId,
      subscriptionType: "BASIC"
    })
);

const permissions = shield(
  {
    Mutation: {
      requestDemo: allow,
      assignUserToCompany: and(isPaidSubscriber, isAdmin),
      "*": isPaidSubscriber
    },
    Query
  },
  {
    fallbackRule: allow,
    debug: true
  }
);

export default permissions;
