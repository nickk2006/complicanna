import createStorage from "./storage";
import createFirebase, { getUser } from "./firebase";

export { IFirebaseToken, ICustomClaims } from "./types";

export { createStorage, createFirebase, getUser };
