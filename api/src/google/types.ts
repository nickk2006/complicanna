export interface IFirebaseToken extends ICustomClaims {
  uid: string;
}

export interface ICustomClaims {
  companyId: string;
  isAdmin: boolean;
}
