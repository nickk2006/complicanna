import { Storage } from "@google-cloud/storage";
import { keys } from "../constants";

const createStorage = () =>
  new Storage({
    projectId: keys.GOOGLE_CLOUD_PROJECT_ID,
    credentials: {
      client_email: keys.GCS_CLIENT_EMAIL,
      private_key: keys.GCS_PRIVATE_KEY
    }
  });

export default createStorage;
