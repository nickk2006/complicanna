import * as admin from "firebase-admin";
import { ContextParameters } from "graphql-yoga/dist/types";
import { IFirebaseToken } from "../google";
import { keys } from "../constants";

const initializeFirebaseApp = () =>
  admin.initializeApp({
    credential: admin.credential.cert({
      projectId: keys.FIREBASE_PROJECT_ID,
      clientEmail: keys.FIREBASE_CLIENT_EMAIL,
      privateKey: keys.FIREBASE_PRIVATE_KEY
    }),
    databaseURL: keys.FIREBASE_DATABASE_URL
  });

export const getUser = async (
  resolve: any,
  root: any,
  args: any,
  ctx: ContextParameters,
  info: any
) => {
  try {
    const authHeader = ctx.request.headers.authorization;
    if (authHeader) {
      const [_, token] = authHeader.split(" ");
      const user = ((await admin
        .auth()
        .verifyIdToken(token)) as any) as IFirebaseToken;
      return resolve(root, args, { user, ...ctx }, info);
    }
    return resolve(root, args, ctx, info);
  } catch (e) {
    console.log("ERROR IN getUser function", e);
    return resolve(root, args, ctx, info);
  }
};

export default initializeFirebaseApp;
