import banner from "./banner.jpg";
import locationIcon from "./location-icon.svg";
import menuIcon from "./menu.svg";
import profitIcon from "./profits.svg";
import batchesDesktop from "./Batches-Desktop.png";
import batchesMobile from "./Batches-Mobile.png";
import batchIntake from "./Batch-Intake.png";
import batchSingle from "./Batch.png";

export {
  banner,
  locationIcon,
  menuIcon,
  profitIcon,
  batchesMobile,
  batchesDesktop,
  batchIntake,
  batchSingle
};
