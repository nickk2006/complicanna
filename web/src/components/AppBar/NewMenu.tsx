import React from "react";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import {
  NewBatchLink,
  NewTestLink,
  NewTestingLabLink,
  NewLicenseHolderLink,
  NewEmployeeLink
} from "../Routing/Links";
import Link from "@material-ui/core/Link";
import { useAppBarLinkStyles as useLinkStyles } from "../Theme";
import Button from "@material-ui/core/Button";

const NewBatch: React.SFC<{}> = () => {
  const classes = useLinkStyles();
  return (
    <Link
      className={classes.root}
      variant="subtitle1"
      color="inherit"
      component={NewBatchLink}
    >
      Batch
    </Link>
  );
};

const NewTest: React.SFC<{}> = () => {
  const classes = useLinkStyles();
  return (
    <Link
      className={classes.root}
      variant="subtitle1"
      color="inherit"
      component={NewTestLink}
    >
      Test
    </Link>
  );
};

const NewTestingLab: React.SFC<{}> = () => {
  const classes = useLinkStyles();
  return (
    <Link
      className={classes.root}
      variant="subtitle1"
      color="inherit"
      component={NewTestingLabLink}
    >
      Testing Lab
    </Link>
  );
};

const NewLicenseHolder: React.SFC<{}> = () => {
  const classes = useLinkStyles();
  return (
    <Link
      className={classes.root}
      variant="subtitle1"
      color="inherit"
      component={NewLicenseHolderLink}
    >
      License Holder
    </Link>
  );
};

const NewEmployee: React.SFC<{}> = () => {
  const classes = useLinkStyles();
  return (
    <Link
      className={classes.root}
      variant="subtitle1"
      color="inherit"
      component={NewEmployeeLink}
    >
      Employee
    </Link>
  );
};

const NewMenu: React.FC<{}> = () => {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const handleClick = (event: any) => {
    setAnchorEl(event.currentTarget);
  };

  function handleClose() {
    setAnchorEl(null);
  }

  return (
    <>
      <Button onClick={handleClick}>New</Button>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        open={Boolean(anchorEl)}
        onClose={handleClose}
        // anchorOrigin={{
        //   vertical: "bottom",
        //   horizontal: "right"
        // }}
        // transformOrigin={{
        //   vertical: "top",
        //   horizontal: "right"
        // }}
      >
        <MenuItem onClick={handleClose}>
          <NewBatch />
        </MenuItem>
        <MenuItem onClick={handleClose}>
          <NewTest />
        </MenuItem>
        <MenuItem onClick={handleClose}>
          <NewTestingLab />
        </MenuItem>
        <MenuItem onClick={handleClose}>
          <NewLicenseHolder />
        </MenuItem>
        <MenuItem onClick={handleClose}>
          <NewEmployee />
        </MenuItem>
      </Menu>
    </>
  );
};

export default NewMenu;
