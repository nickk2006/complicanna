import React from "react";
import Link from "@material-ui/core/Link";
import NewMenu from "./NewMenu";
import {
  HomeLink,
  // PricingLink,
  DemoLink,
  LoginLink,
  SignUpLink,
  BatchesLink,
  MyTodosLink,
  MyAccountLink,
  PricingLink
} from "../Routing/Links";
import Button from "@material-ui/core/Button";

export const Home: React.SFC<{}> = () => (
  <Link
    variant="h6"
    underline="none"
    color="inherit"
    noWrap
    component={HomeLink}
  >
    CompliCanna
  </Link>
);

const LinkBtn: React.SFC<{
  component: React.ComponentType<any>;
  label: string;
}> = props => {
  const { component: Component, label } = props;
  return (
    <Button component={Component} variant="text">
      {label}
    </Button>
  );
};

const Contact: React.SFC<{}> = props => {
  return (
    <a href="mailto:hi@complicanna.com">
      <Button variant="text">CONTACT</Button>
    </a>
  );
};

export const AppBarAuthLinks: React.SFC<{}> = () => {
  return (
    <div>
      <LinkBtn component={BatchesLink} label="Batches" />
      <LinkBtn component={MyTodosLink} label="My Todos" />
      <NewMenu />
      <Contact />
      <LinkBtn component={MyAccountLink} label="My Account" />
    </div>
  );
};

export const AppBarUnauthLinks: React.SFC<{}> = () => {
  return (
    <div>
      <LinkBtn component={DemoLink} label="Demo" />
      <LinkBtn component={PricingLink} label="Pricing" />
      <Contact />
      <LinkBtn component={SignUpLink} label="Sign Up" />
      <LinkBtn component={LoginLink} label="Login" />
      {/* <LinkBtn component={SignUpLink} label="Sign Up" /> */}
    </div>
  );
};
