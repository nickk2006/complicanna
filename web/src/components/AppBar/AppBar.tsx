import React from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import { makeStyles, createStyles } from "@material-ui/styles";
import { ITheme } from "../Theme";

import firebase from "../Firebase";
import { useAuthState } from "react-firebase-hooks/auth";
import { Home, AppBarAuthLinks, AppBarUnauthLinks } from "./Links";

const useStyles = makeStyles((theme: ITheme) =>
  createStyles({
    appBar: {
      position: "relative"
    },
    toolbarTitle: {
      flex: 1
    },
    menuButton: {
      // marginLeft: -12,
      marginRight: 10
    }
  })
);
const EnhancedAppBar: React.FC<{}> = () => {
  const classes = useStyles();
  const { user } = useAuthState(firebase.auth());

  const isLoggedIn = user !== null;

  return (
    <AppBar position="static" color="default" className={classes.appBar}>
      <Toolbar>
        <div className={classes.toolbarTitle}>
          <Home />
        </div>
        {isLoggedIn ? <AppBarAuthLinks /> : <AppBarUnauthLinks />}
      </Toolbar>
    </AppBar>
  );
};

export default EnhancedAppBar;
