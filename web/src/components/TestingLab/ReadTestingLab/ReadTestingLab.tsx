import React from "react";
import { gql } from "apollo-boost";
import { Query } from "react-apollo";
import { withRouter, RouteComponentProps } from "react-router";
import { useReadListStyle } from "../../Theme";
import Typography from "@material-ui/core/Typography";
import { Link as RouterLink } from "react-router-dom";
import Link from "@material-ui/core/Link";
import { ListItemTypography, ListItemTypographyProps } from "../../Typography";
import * as ROUTES from "../../../constants/routes";
import capitalize from "lodash/capitalize";
import Loading from "../../Loading";

import {
  testingLab,
  testingLabVariables,
  testingLab_testingLab_tests
} from "./__generated__/testingLab";

export const READ_TESTING_LAB = gql`
  query testingLab($where: TestingLabWhereUniqueInput!) {
    testingLab(where: $where) {
      id
      name
      email
      phone
      tests {
        id
        result
        batch {
          id
          batchNumber
        }
      }
    }
  }
`;

class TestingLabQuery extends Query<testingLab, testingLabVariables> {}

const Tests: React.SFC<{ tests: testingLab_testingLab_tests[] | null }> = ({
  tests
}) => {
  const list = useReadListStyle();
  if (tests === null) {
    return <ListItemTypography>None</ListItemTypography>;
  } else {
    return (
      <>
        {tests.map(test => {
          const result = capitalize(test.result);

          const batchRoute = ROUTES.READ_BATCH_FN(test.batch.id);
          const BatchLink = (props: any) => (
            <RouterLink to={batchRoute} {...props} />
          );

          return (
            <div key={test.id} className={list.dataRow}>
              <ListItemTypography>{result}</ListItemTypography>
              <Link
                component={BatchLink}
                variant={ListItemTypographyProps.variant}
              >
                {test.batch.batchNumber}
              </Link>
            </div>
          );
        })}
      </>
    );
  }
};

export const ReadTestingLabBase: React.SFC<testingLab> = ({ testingLab }) => {
  const list = useReadListStyle();
  if (testingLab === null) return <div>Can't find Testing Lab</div>;

  const { tests } = testingLab;

  console.dir(testingLab);

  return (
    <div className={list.root}>
      <div className={list.titleRow}>
        <Typography variant="h5">{`Testing Lab - ${
          testingLab.name
        }`}</Typography>
      </div>
      <div className={list.dataRow}>
        <ListItemTypography>Email</ListItemTypography>
        <Link
          href={`mailto:${testingLab.email}`}
          variant={ListItemTypographyProps.variant}
        >
          {testingLab.email}
        </Link>
        <ListItemTypography>Phone</ListItemTypography>
        <ListItemTypography>{testingLab.phone}</ListItemTypography>
      </div>
      <div className={list.titleRow}>
        <Typography variant="h6">Tests</Typography>
      </div>
      <Tests tests={tests} />
    </div>
  );

  // const { id, name, nameSlug, email, phone, company, tests } = testingLab;
  // return (
  //   <ul>
  //     <li>ID: {id}</li>
  //     <li>Name: {name}</li>
  //     <li>Name Slug: {nameSlug}</li>
  //     <li>Email: {email}</li>
  //     <li>Phone: {phone}</li>
  //     <li>Company ID: {company.id}</li>
  //     <li>Company Name: {company.name}</li>
  //     <li>
  //       Tests:{" "}
  //       {tests !== null ? (
  //         tests.map(test => <li key={test.id}>ID: {test.id}</li>)
  //       ) : (
  //         <></>
  //       )}
  //     </li>
  //   </ul>
  // );
};

interface IProps extends RouteComponentProps<{ testingLabId: string }> {}

export const ReadTestingLab: React.SFC<IProps> = props => (
  <TestingLabQuery
    query={READ_TESTING_LAB}
    variables={{ where: { id: props.match.params.testingLabId } }}
  >
    {({ data, loading, error }) => {
      if (loading) return <Loading />;
      if (error !== undefined) return <div>error: {error.message}</div>;
      if (typeof data !== "undefined")
        return <ReadTestingLabBase testingLab={data.testingLab} />;
    }}
  </TestingLabQuery>
);

export default withRouter<IProps>(ReadTestingLab);
