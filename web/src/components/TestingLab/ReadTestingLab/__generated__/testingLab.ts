/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

import { TestingLabWhereUniqueInput, TestResult } from "./../../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: testingLab
// ====================================================

export interface testingLab_testingLab_tests_batch {
  __typename: "Batch";
  id: string;
  batchNumber: string | null;
}

export interface testingLab_testingLab_tests {
  __typename: "Test";
  id: string;
  result: TestResult;
  batch: testingLab_testingLab_tests_batch;
}

export interface testingLab_testingLab {
  __typename: "TestingLab";
  id: string;
  name: string;
  email: string;
  phone: string | null;
  tests: testingLab_testingLab_tests[] | null;
}

export interface testingLab {
  testingLab: testingLab_testingLab | null;
}

export interface testingLabVariables {
  where: TestingLabWhereUniqueInput;
}
