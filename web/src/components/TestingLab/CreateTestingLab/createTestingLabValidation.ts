import * as yup from "yup";
import { requiredString } from "../../FormComponents";
import { CreateTestingLabInput } from "./../../../types/graphql-global-types";

const createLabValidation: yup.ObjectSchema<CreateTestingLabInput> = yup.object(
  {
    name: requiredString,
    email: yup.string().email("Invalid email"),
    phone: yup.string()
  }
);

export default createLabValidation;
