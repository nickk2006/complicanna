import React from "react";
import { gql } from "apollo-boost";
import { Mutation } from "react-apollo";
import CreateTestingLab from "./CreateTestingLabFormik";
import {
  createTestingLabVariables,
  createTestingLab
} from "./__generated__/createTestingLab";

export const CREATE_TESTING_LAB = gql`
  mutation createTestingLab($data: CreateTestingLabInput!) {
    createTestingLab(data: $data) {
      id
    }
  }
`;

class CreateTestingLabMutation extends Mutation<
  createTestingLab,
  createTestingLabVariables
> {}

const ConnectedForm: React.FC<{}> = props => (
  <CreateTestingLabMutation mutation={CREATE_TESTING_LAB}>
    {createTestingLab => (
      <CreateTestingLab createTestingLab={createTestingLab} />
    )}
  </CreateTestingLabMutation>
);

export default ConnectedForm;
