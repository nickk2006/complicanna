/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

import { CreateTestingLabInput } from "./../../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: createTestingLab
// ====================================================

export interface createTestingLab_createTestingLab {
  __typename: "TestingLab";
  id: string;
}

export interface createTestingLab {
  createTestingLab: createTestingLab_createTestingLab;
}

export interface createTestingLabVariables {
  data: CreateTestingLabInput;
}
