import React from "react";
import { Field, FormikProps } from "formik";
import { TextField } from "formik-material-ui";
import { CreateTestingLabInput } from "../../../types/graphql-global-types";
import { Title, SubmitBtn, Form } from "../../FormComponents";

interface IProps extends FormikProps<CreateTestingLabInput> {}

const TestingLabForm: React.SFC<IProps> = props => {
  return (
    <Form>
      <Title>New Testing Lab</Title>
      <Field
        name="name"
        label="Testing Lab Name"
        variant="outlined"
        component={TextField}
      />
      <Field
        name="email"
        type="email"
        label="Lab Email"
        variant="outlined"
        component={TextField}
      />
      <Field
        name="phone"
        label="Phone Number"
        variant="outlined"
        component={TextField}
      />
      <SubmitBtn disabled={props.isSubmitting}>Create Testing Lab</SubmitBtn>
    </Form>
  );
};

export default TestingLabForm;
