import React from "react";
import { Formik, FormikActions } from "formik";
import * as ROUTES from "../../../constants/routes";
import useReactRouter from "use-react-router";
import { MutationFn } from "react-apollo";
import {
  createTestingLab,
  createTestingLabVariables
} from "./__generated__/createTestingLab";
import { CreateTestingLabInput } from "../../../types/graphql-global-types";
import CreateTestingLabForm from "./CreateTestingLabForm";
import schema from "./createTestingLabValidation";

export const INITIAL_VALUES: CreateTestingLabInput = {
  name: "",
  email: "",
  phone: ""
};

const FormikConnectedForm: React.FC<{
  createTestingLab: MutationFn<createTestingLab, createTestingLabVariables>;
}> = ({ createTestingLab }) => {
  const { history } = useReactRouter();

  const handleSubmit = async (
    data: CreateTestingLabInput,
    formikBag: FormikActions<CreateTestingLabInput>
  ) => {
    const res = await createTestingLab({ variables: { data } });
    if (typeof res !== "undefined" && typeof res.data !== "undefined") {
      formikBag.resetForm();
      history.push(ROUTES.READ_TESTING_LAB_FN(res.data.createTestingLab.id));
    }
  };

  return (
    <Formik
      initialValues={INITIAL_VALUES}
      onSubmit={handleSubmit}
      validationSchema={schema}
    >
      {formikProps => <CreateTestingLabForm {...formikProps} />}
    </Formik>
  );
};

export default FormikConnectedForm;
