/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: testingLabsForPicker
// ====================================================

export interface testingLabsForPicker_testingLabs {
  __typename: "TestingLab";
  id: string;
  name: string;
}

export interface testingLabsForPicker {
  testingLabs: testingLabsForPicker_testingLabs[];
}
