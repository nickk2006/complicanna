import React from "react";
import { Field } from "formik";
import MenuItem from "@material-ui/core/MenuItem";
import { Query } from "react-apollo";
import { gql } from "apollo-boost";
import { TextField } from "formik-material-ui";
import { testingLabsForPicker } from "./__generated__/testingLabsForPicker";
import { usePickerStyle } from "../../Theme";
import Loading from "../../Loading";

export const READ_TESTING_LABS = gql`
  query testingLabsForPicker {
    testingLabs {
      id
      name
    }
  }
`;

interface IPropsTestingPickerBase extends testingLabsForPicker {
  name: string;
  label: string;
}

const TestingLabPickerBase: React.FC<IPropsTestingPickerBase> = ({
  testingLabs,
  name,
  label
}) => {
  const pickerStyle = usePickerStyle();

  return (
    <div className={pickerStyle.root}>
      <Field
        name={name}
        label={label}
        variant="outlined"
        select
        component={TextField}
      >
        {testingLabs.map(lab => (
          <MenuItem key={lab.id} value={lab.id}>
            {lab.name}
          </MenuItem>
        ))}
      </Field>
    </div>
  );
};

class TestingLabsQuery extends Query<testingLabsForPicker> {}

const TestingLabPicker: React.SFC<{ name?: string; label?: string }> = ({
  name = "testingLab",
  label = "Testing Lab"
}) => (
  <TestingLabsQuery query={READ_TESTING_LABS}>
    {({ data, loading, error }) => {
      if (loading) return <Loading />;
      if (typeof data !== "undefined")
        return (
          <TestingLabPickerBase
            testingLabs={data.testingLabs}
            label={label}
            name={name}
          />
        );
      if (error) {
        console.log("ERROR", error);
        return <div>ERROR: {error.message}</div>;
      }
    }}
  </TestingLabsQuery>
);

export default TestingLabPicker;
