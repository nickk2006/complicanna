import React from "react";
import Typography from "@material-ui/core/Typography";
import { makeStyles, createStyles } from "@material-ui/styles";
import { ITheme } from "../Theme";
import { RegionalCard, StateCard, MultistateCard } from "../PricingCards";

const useStyles = makeStyles((theme: ITheme) =>
  createStyles({
    heroContent: {
      maxWidth: 600,
      margin: "0 auto",
      padding: `${theme.spacing.unit * 8}px 0 ${theme.spacing.unit * 6}px`
    },
    cardContainer: {
      display: "grid",
      [theme.breakpoints.down("sm")]: {
        gridTemplateColumns: "auto",
        gridTemplateRows: "auto",
        gridTemplateAreas: "'intro' 'state' 'multistate'",
        rowGap: "50px"
      },
      [theme.breakpoints.up("md")]: {
        gridTemplateColumns: "repeat(3, 275px)",
        gridTemplateRows: "repeat(auto, 500px)",
        gridTemplateAreas: "'intro state multistate'",
        columnGap: 37.5
      }
    },
    intro: {
      gridArea: "intro"
    },
    state: {
      gridArea: "state"
    },
    multistate: {
      gridArea: "multistate"
    }
  })
);

const Pricing: React.FC<{}> = props => {
  const classes = useStyles();

  return (
    <>
      <div className={classes.heroContent}>
        <Typography
          component="h1"
          variant="h2"
          align="center"
          color="textPrimary"
          gutterBottom
        >
          Pricing
        </Typography>
        <Typography
          variant="h6"
          align="center"
          color="textSecondary"
          component="p"
        >
          Let us help find the right plan for you
        </Typography>
      </div>
      <div className={classes.cardContainer}>
        <div className={classes.intro}>
          <RegionalCard />
        </div>
        <div className={classes.state}>
          <StateCard />
        </div>
        <div className={classes.multistate}>
          <MultistateCard />
        </div>
      </div>
    </>
  );
};

export default Pricing;
