import React from "react";
import { Field } from "formik";
import { TextField } from "formik-material-ui";
import MenuItem from "@material-ui/core/MenuItem";

interface IProps {
  name: string;
  label?: string;
}

export const LicensePicker: React.SFC<IProps> = ({
  name,
  label = "License Type"
}) => {
  return (
    <Field
      name={name}
      select
      label={label}
      variant="outlined"
      component={TextField}
    >
      <MenuItem value={0} />
      <MenuItem value={1}>Adult-Use Distributor</MenuItem>
      <MenuItem value={2}>Adult-Use Retailer</MenuItem>
      <MenuItem value={3}>Adult-Use Microbusiness</MenuItem>
      <MenuItem value={4}>Adult-Use Retailer Non-Storefront</MenuItem>
      <MenuItem value={5}>Medical Distributor</MenuItem>
      <MenuItem value={6}>Medical Retailer</MenuItem>
      <MenuItem value={7}>Medical Microbusiness</MenuItem>
      <MenuItem value={8}>Medical Retailer Non-Storefront</MenuItem>
      <MenuItem value={9}>Combined Retailer</MenuItem>
    </Field>
  );
};

export default LicensePicker;
