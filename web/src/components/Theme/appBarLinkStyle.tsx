import { makeStyles, createStyles } from "@material-ui/styles";
import { ITheme } from ".";

const useAppBarLinkStyles = makeStyles((theme: ITheme) =>
  createStyles({
    root: {
      marginLeft: "15px",
      marginRight: "15px"
    }
  })
);

export default useAppBarLinkStyles;
