import { ITheme } from ".";
import { makeStyles, createStyles } from "@material-ui/styles";

const usePickerStyle = makeStyles((theme: ITheme) =>
  createStyles({
    root: {
      display: "grid",
      gridRowGap: theme.spacing.unit
    }
  })
);

export default usePickerStyle;
