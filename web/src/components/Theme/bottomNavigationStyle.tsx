import { makeStyles, createStyles } from "@material-ui/styles";
import { ITheme } from ".";

const bottomNavigationStyle = makeStyles((theme: ITheme) =>
  createStyles({
    root: {
      position: "fixed",
      overflow: "hidden",
      width: "100%",
      bottom: 0,
      zIndex: theme.zIndex.appBar
    }
  })
);

export default bottomNavigationStyle;
