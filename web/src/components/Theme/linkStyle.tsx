import { makeStyles } from "@material-ui/styles";
import { ITheme } from ".";

const useLinkStyles = makeStyles((theme: ITheme) => ({
  noColor: {
    color: theme.palette.text.primary
  }
}));

export default useLinkStyles;
