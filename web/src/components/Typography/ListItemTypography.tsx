import React from "react";
import Typography, { TypographyProps } from "@material-ui/core/Typography";

export const ListItemTypographyProps: TypographyProps = {
  variant: "body1"
};

const ListItemTypography: React.SFC<TypographyProps> = ({
  children,
  ...rest
}) => (
  <Typography variant="body1" {...rest}>
    {children}
  </Typography>
);

export default ListItemTypography;
