import React from "react";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardHeader from "@material-ui/core/CardHeader";
import useCardStyles from "./usePricingCardStyles";
import { Pricing, Description, GetStartedBtn } from "./CardComponents";
import { statewidePlan } from "../../constants/plans";

const description = [
  "Process 150 Batches/Month",
  "5 Admin Users",
  "1 hour of 1-on-1 Onboarding",
  "Project Metrics",
  "Exportable Reports"
];

const StateCard: React.FC<{}> = props => {
  const classes = useCardStyles();

  return (
    <Card>
      <CardHeader
        title={statewidePlan.displayName}
        titleTypographyProps={{ align: "center", color: "inherit" }}
        subheaderTypographyProps={{ align: "center" }}
        className={classes.cardHeaderMed}
      />
      <CardContent className={classes.content}>
        <div className={classes.cardPricing}>
          <Pricing amount={statewidePlan.price} />
        </div>
        {description.map(line => (
          <Description key={line}>{line}</Description>
        ))}
      </CardContent>
      <CardActions className={classes.cardActions}>
        <GetStartedBtn plan={statewidePlan} variant="contained">
          Get Started
        </GetStartedBtn>
      </CardActions>
    </Card>
  );
};

export default StateCard;
