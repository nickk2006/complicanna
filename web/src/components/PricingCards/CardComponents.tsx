import React from "react";
import Typography from "@material-ui/core/Typography";
import { makeStyles, createStyles } from "@material-ui/styles";
import { ITheme } from "../Theme";
import { ButtonProps } from "@material-ui/core/Button";
import Button from "@material-ui/core/Button";
import useReactRouter from "use-react-router";
import * as ROUTES from "../../constants/routes";
import { IPlan } from "../../constants/plans";

const usePricingStyles = makeStyles((theme: ITheme) =>
  createStyles({
    dollar: {
      alignSelf: "center",
      marginRight: 5
    }
  })
);
export const Pricing: React.SFC<{ amount: number }> = props => {
  const { amount } = props;
  const classes = usePricingStyles();
  return (
    <>
      {/* <div className={classes.price}> */}
      <Typography
        className={classes.dollar}
        variant="subtitle1"
        color="textPrimary"
      >
        $
      </Typography>
      <Typography component="h2" variant="h3" color="textPrimary">
        {amount}
      </Typography>
      {/* </div> */}
      <Typography variant="h6" color="textSecondary">
        /mo
      </Typography>
    </>
  );
};

export const Description: React.SFC<{}> = props => {
  const { children } = props;
  return <Typography variant="body2">{children}</Typography>;
};

export const GetStartedBtn: React.SFC<{
  plan: IPlan;
  variant: ButtonProps["variant"];
}> = props => {
  const { plan, children, variant } = props;

  const route = ROUTES.SIGN_UP_FN(plan);

  const { history } = useReactRouter();

  const handleClick = () => {
    history.push(route);
  };

  return (
    <Button onClick={handleClick} fullWidth variant={variant} color="primary">
      {children}
    </Button>
  );
};
