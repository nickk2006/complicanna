import { ButtonProps } from "@material-ui/core/Button";

export interface IPricingDetails {
  title: string;
  subheader?: string;
  price: string;
  description: string[];
  buttonText: string;
  buttonVariant: ButtonProps["variant"];
}

// const pricingDetails: IPricingDetails[] = [
//   {
//     title: "Canna Essentials",
//     price: "85",
//     description: [],
//     buttonText: "Get Started",
//     buttonVariant:
//   }
// ]
