import React from "react";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardHeader from "@material-ui/core/CardHeader";
import useCardStyles from "./usePricingCardStyles";
import { Pricing, Description, GetStartedBtn } from "./CardComponents";
import { regionalPlan } from "../../constants/plans";

const description = [
  "Process 50 Batches/Month",
  "1 admin user",
  "1 hour of 1-on-1 Onboarding"
];

const RegionalCard: React.FC<{}> = props => {
  const classes = useCardStyles();

  return (
    <Card>
      <CardHeader
        title={regionalPlan.displayName}
        titleTypographyProps={{ align: "center", color: "inherit" }}
        subheaderTypographyProps={{ align: "center" }}
        className={classes.cardHeaderLight}
      />
      <CardContent className={classes.content}>
        <div className={classes.cardPricing}>
          <Pricing amount={regionalPlan.price} />
        </div>
        {description.map(line => (
          <Description key={line}>{line}</Description>
        ))}
      </CardContent>
      <CardActions className={classes.cardActions}>
        <GetStartedBtn variant="outlined" plan={regionalPlan}>
          Get Started
        </GetStartedBtn>
      </CardActions>
    </Card>
  );
};

export default RegionalCard;
