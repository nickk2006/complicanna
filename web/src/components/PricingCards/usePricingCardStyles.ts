import { makeStyles, createStyles } from "@material-ui/styles";
import { ITheme } from "../Theme";
import green from "@material-ui/core/colors/green";

export default makeStyles((theme: ITheme) =>
  createStyles({
    content: {
      height: 325,
      marginTop: theme.spacing.unit
    },
    cardHeaderLight: {
      backgroundColor: green[100]
    },
    cardHeaderMed: {
      backgroundColor: green[300]
    },
    cardHeaderDark: {
      backgroundColor: green[500]
    },
    cardPricing: {
      display: "flex",
      justifyContent: "center",
      alignItems: "baseline",
      marginBottom: theme.spacing.unit * 3
    },
    cardActions: {
      [theme.breakpoints.up("sm")]: {
        paddingBottom: theme.spacing.unit * 2
      }
    }
  })
);
