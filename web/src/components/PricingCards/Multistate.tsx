import React from "react";
import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardHeader from "@material-ui/core/CardHeader";
import useCardStyles from "./usePricingCardStyles";
import { Description } from "./CardComponents";
import clipboard from "../../images/clipboards.svg";
import { makeStyles, createStyles } from "@material-ui/styles";
import { ITheme } from "../Theme";

const useMultistateStyles = makeStyles((theme: ITheme) =>
  createStyles({
    clipboard: {
      height: 50,
      width: 50
    }
  })
);

const description = [
  "Process 150+ Batches/Month",
  "5+ Admin Users",
  "1 hour of 1-on-1 Onboarding",
  "Project Metrics",
  "Exportable Reports",
  "User management (OAuth2, Okta, etc)",
  "Priority Support",
  "Custom Invoicing Options"
];

const Multistate: React.FC<{}> = props => {
  const classes = useCardStyles();
  const multistateClasses = useMultistateStyles();

  return (
    <Card>
      <CardHeader
        title="Multistate"
        titleTypographyProps={{
          align: "center",
          color: "inherit"
        }}
        subheaderTypographyProps={{ align: "center" }}
        className={classes.cardHeaderDark}
      />
      <CardContent className={classes.content}>
        <div className={classes.cardPricing}>
          <img
            alt="multistate-icon"
            className={multistateClasses.clipboard}
            src={clipboard}
          />
        </div>
        {description.map(line => (
          <Description key={line}>{line}</Description>
        ))}
      </CardContent>
      <CardActions className={classes.cardActions}>
        <Button fullWidth variant="outlined" color="primary">
          Contact Us
        </Button>
      </CardActions>
    </Card>
  );
};

export default Multistate;
