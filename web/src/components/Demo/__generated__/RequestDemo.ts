/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

import { RequestDemoInput } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: RequestDemo
// ====================================================

export interface RequestDemo {
  requestDemo: boolean;
}

export interface RequestDemoVariables {
  data: RequestDemoInput;
}
