import React from "react";
import RequestDemoMutation, { Request_Demo } from "./RequestDemoMutation";
import DemoRequestFormFormik from "./DemoRequestFormFormik";
import SuccessSnackbar from "../SuccessSnackbar";

const ApiConnectedForm: React.FC<{}> = props => {
  const [open, setOpen] = React.useState(false);
  const handleCompleted = () => {
    setOpen(true);
  };
  return (
    <>
      <RequestDemoMutation
        mutation={Request_Demo}
        onCompleted={handleCompleted}
      >
        {requestDemo => <DemoRequestFormFormik requestDemo={requestDemo} />}
      </RequestDemoMutation>
      <SuccessSnackbar open={open} setOpen={setOpen} />
    </>
  );
};

export default ApiConnectedForm;
