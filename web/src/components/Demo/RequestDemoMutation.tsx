import { gql } from "apollo-boost";
import { Mutation } from "react-apollo";
import { RequestDemo, RequestDemoVariables } from "./__generated__/RequestDemo";

export const Request_Demo = gql`
  mutation RequestDemo($data: RequestDemoInput!) {
    requestDemo(data: $data)
  }
`;

class RequestDemoMutation extends Mutation<RequestDemo, RequestDemoVariables> {}

export default RequestDemoMutation;
