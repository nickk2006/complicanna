import React from "react";
import DemoRequestFormFields from "./DemoRequestFormFields";
import { Formik, FormikActions } from "formik";
import { MutationFn } from "react-apollo";
import { RequestDemo, RequestDemoVariables } from "./__generated__/RequestDemo";
import { RequestDemoInput } from "../../types/graphql-global-types";

interface IProps {
  requestDemo: MutationFn<RequestDemo, RequestDemoVariables>;
}

const webToApi = (values: RequestDemoInput) => {
  const data: RequestDemoVariables = {
    data: values
  };

  return data;
};

const INITIAL_VALUES: RequestDemoInput = {
  firstName: "",
  lastName: "",
  companyName: "",
  email: ""
};

const DemoRequestFormFormik: React.FC<IProps> = props => {
  const { requestDemo } = props;

  const handleSubmit = async (
    values: RequestDemoInput,
    formikActions: FormikActions<RequestDemoInput>
  ) => {
    const data = webToApi(values);
    try {
      await requestDemo({ variables: data });
      formikActions.resetForm();
      // setSuccess(true);
    } catch (e) {
      console.error(e);
      return;
    }
  };

  return (
    <Formik initialValues={INITIAL_VALUES} onSubmit={handleSubmit}>
      {formikProps => <DemoRequestFormFields {...formikProps} />}
    </Formik>
  );
};

export default DemoRequestFormFormik;
