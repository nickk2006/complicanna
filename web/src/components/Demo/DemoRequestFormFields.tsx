import React from "react";
import { FormikProps } from "formik";
import { FirstName, LastName, CompanyName, Email } from "../SignUp/FormFields";
import { Form, Title, SubmitBtn } from "../FormComponents";
import { RequestDemoInput } from "./../../types/graphql-global-types";

interface IDemoRequestFormFieldsProps extends FormikProps<RequestDemoInput> {}

const DemoRequestFormFields: React.FC<IDemoRequestFormFieldsProps> = props => {
  return (
    <Form>
      <Title>Request A Demo</Title>
      <FirstName />
      <LastName />
      <CompanyName name="companyName" />
      <Email />
      <SubmitBtn disabled={props.isSubmitting}>Request a Demo</SubmitBtn>
    </Form>
  );
};

export default DemoRequestFormFields;
