import React from "react";
import MyAccountView from "./MyAccountView";
import MyAccountQuery, { MY_ACCOUNT_QUERY } from "./MyAccountQuery";
import Loading from "../Loading";
import Typography from "@material-ui/core/Typography";

const MyAccount: React.FC<{}> = props => {
  return (
    <MyAccountQuery query={MY_ACCOUNT_QUERY}>
      {({ data, loading, error }) => {
        if (loading) return <Loading />;
        if (typeof data !== "undefined")
          return <MyAccountView user={data.user} />;
        if (error) return <div>{JSON.stringify(error)}</div>;
        return <Typography>No User Found</Typography>;
      }}
    </MyAccountQuery>
  );
};

export default MyAccount;
