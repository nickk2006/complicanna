import React from "react";
import { ListItemTypography } from "../Typography";
import { useListStyles } from "../Theme";
import { MyAccountQuery } from "./__generated__/MyAccountQuery";
import { dateForDisplay } from "../../helpers/format";
import SignOutBtn from "../SignOut";
import { makeStyles, createStyles } from "@material-ui/styles";
import { ITheme } from "../Theme";
import DataRow from "../DataRow";

interface IProps extends MyAccountQuery {}

const useStyles = makeStyles((theme: ITheme) =>
  createStyles({
    root: {
      display: "grid",
      gridRowGap: 20
    }
  })
);

const MyAccountView: React.FC<IProps> = props => {
  const { user } = props;
  const listStyle = useListStyles();
  const classes = useStyles();
  if (user === null) {
    return <ListItemTypography>No User</ListItemTypography>;
  }

  const name = `${user.firstName} ${user.lastName}`;
  const employeeId = user.employeeId !== null ? user.employeeId : "n/a";
  const createDate = dateForDisplay(user.createdAt);

  return (
    <div className={classes.root}>
      <div className={listStyle.root}>
        <DataRow label="Name" value={name} />
        <DataRow label="Email" value={user.email} />
        <DataRow label="Account Created" value={createDate} />
        <DataRow label="Employee Id" value={employeeId} />
      </div>
      <SignOutBtn />
    </div>
  );
};

export default MyAccountView;
