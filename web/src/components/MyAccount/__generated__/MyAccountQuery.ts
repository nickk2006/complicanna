/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: MyAccountQuery
// ====================================================

export interface MyAccountQuery_user {
  __typename: "User";
  id: string;
  firstName: string;
  lastName: string;
  employeeId: string | null;
  email: string;
  createdAt: any;
}

export interface MyAccountQuery {
  user: MyAccountQuery_user | null;
}
