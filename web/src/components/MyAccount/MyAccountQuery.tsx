import { gql } from "apollo-boost";
import { MyAccountQuery } from "./__generated__/MyAccountQuery";
import { Query } from "react-apollo";

export const MY_ACCOUNT_QUERY = gql`
  query MyAccountQuery {
    user {
      id
      firstName
      lastName
      employeeId
      email
      createdAt
    }
  }
`;

class QueryMyAccount extends Query<MyAccountQuery> {}

export default QueryMyAccount;
