import { MockedResponse } from "react-apollo/test-utils";
import { READ_LICENSE_HOLDERS_QUERY } from "../LicenseHolder/LicenseHolderPicker";
import { licenseHoldersForPicker } from "../LicenseHolder/LicenseHolderPicker/__generated__/licenseHoldersForPicker";
import { FetchResult } from "react-apollo";

const result: FetchResult<licenseHoldersForPicker> = {
  data: {
    licenseHolders: [
      {
        __typename: "LicenseHolder",
        id: "LicenseHolderId",
        name: "LicenseHolderName"
      }
    ]
  }
};

const mock: MockedResponse = {
  request: {
    query: READ_LICENSE_HOLDERS_QUERY
  },
  result
};

export default mock;
