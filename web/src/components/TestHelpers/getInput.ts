const getInput = (container: HTMLElement, fieldName: string) => {
  const node = container.querySelector(
    `input[name='${fieldName}']`
  ) as HTMLInputElement;

  return node;
};

export default getInput;
