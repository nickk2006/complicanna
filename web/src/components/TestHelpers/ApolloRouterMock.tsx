import React from "react";
import { MemoryRouter } from "react-router-dom";
import {
  MockedProvider,
  MockedProviderProps,
  MockedResponse
} from "react-apollo/test-utils";
import { LocationDescriptor } from "history";

import theme from "../Theme";
import { MuiThemeProvider as ThemeProvider } from "@material-ui/core/styles";

const ApolloRouterMock: React.SFC<{
  mocks?: ReadonlyArray<MockedResponse>;
  initialEntries?: LocationDescriptor[];
  initialIndex?: number;
  addTypename?: boolean;
}> = props => {
  const { mocks, initialEntries, initialIndex, addTypename, children } = props;
  const updatedTypeChildren = children as React.ReactElement;

  return (
    <ThemeProvider theme={theme}>
      <MemoryRouter initialEntries={initialEntries} initialIndex={initialIndex}>
        <MockedProvider mocks={mocks} addTypename={addTypename}>
          {updatedTypeChildren}
        </MockedProvider>
      </MemoryRouter>
    </ThemeProvider>
  );
};

ApolloRouterMock.defaultProps = {
  addTypename: false
};

export default ApolloRouterMock;
