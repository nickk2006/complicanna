import React from "react";
import { makeStyles, createStyles } from "@material-ui/styles";
import { ITheme } from "../Theme";

export const bottomGutter = {
  paddingBottom: 60
};

export const useStyles = makeStyles((theme: ITheme) =>
  createStyles({
    content: {
      display: "grid",
      justifyItems: "center",
      width: "auto",
      marginLeft: 0,
      marginRight: 0,
      marginTop: theme.spacing.unit * 3,
      [theme.breakpoints.up(900 + theme.spacing.unit * 3 * 2)]: {
        // width: 900,
        marginLeft: theme.spacing.unit * 3,
        marginRight: theme.spacing.unit * 3
      },
      [theme.breakpoints.down("sm")]: {
        ...bottomGutter
      }
    }
  })
);

const ResponsiveDrawer: React.FC<{}> = props => {
  const classes = useStyles();
  return <main className={classes.content}>{props.children}</main>;
};

export default ResponsiveDrawer;
