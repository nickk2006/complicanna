import React from "react";
import Snackbar from "@material-ui/core/Snackbar";
import useReactRouter from "use-react-router";
import Typography from "@material-ui/core/Typography";

const SuccessSnackbar: React.FC<{
  open: boolean;
  setOpen: React.Dispatch<React.SetStateAction<boolean>>;
  successMessage?: string;
  successRoute?: string;
}> = props => {
  const { open, setOpen, successRoute, successMessage = "Success" } = props;
  const { history } = useReactRouter();

  const handleClose = (event: object, reason: string) => {
    setOpen(false);
    if (successRoute) history.push(successRoute);
  };

  return (
    <Snackbar
      anchorOrigin={{
        vertical: "bottom",
        horizontal: "left"
      }}
      open={open}
      autoHideDuration={2000}
      onClose={handleClose}
      ContentProps={{
        "aria-describedby": "message-id"
      }}
      message={
        <Typography color="inherit" variant="body1" id="message-id">
          {successMessage}
        </Typography>
      }
    />
  );
};

export default SuccessSnackbar;
