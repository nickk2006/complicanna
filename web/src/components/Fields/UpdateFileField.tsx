import React from "react";
import { makeStyles, createStyles } from "@material-ui/styles";
import { ITheme } from "../Theme";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import AttachFileIcon from "@material-ui/icons/AttachFile";
import { Field, FieldProps } from "formik";
import Link from "@material-ui/core/Link";
import classNames from "classnames";

const useStylesUpdateFileField = makeStyles((theme: ITheme) =>
  createStyles({
    root: {
      display: "flex",
      flexDirection: "column"
    },
    oldFile: {
      display: "grid",
      gridTemplateColumns: "auto 1fr",
      gridColumnGap: "0.25em"
    },
    newFile: {
      display: "grid",
      gridTemplateColumns: "48px 452px",
      alignItems: "center"
    },
    oldFileName: {
      textDecoration: "line-through"
    }
  })
);

export interface IOriginalFile {
  title: string;
  signedUrl: string;
}

interface IUpdateFileFieldProps {
  name: string;
  title: string;
  originalFile: IOriginalFile | undefined;
}

const UpdateFileField: React.FC<IUpdateFileFieldProps> = ({
  name,
  title,
  originalFile
}) => {
  const inputEl = React.useRef<HTMLInputElement>(null);
  const [filename, setFilename] = React.useState("");
  const [isNewFileSet, setIsNewFileSet] = React.useState(false);

  const classes = useStylesUpdateFileField();
  const oldFileText = classNames({
    [classes.oldFileName]: isNewFileSet
  });

  const actionLabel =
    typeof originalFile === "undefined" ? "Upload" : "Replace";

  const openFileDialog = () => {
    const ref = inputEl.current;
    if (ref !== null) {
      ref.click();
    }
  };

  return (
    <div className={classes.root}>
      <div className={classes.oldFile}>
        <Typography variant="body1">{`${actionLabel} ${title}:`}</Typography>
        {typeof originalFile !== "undefined" ? (
          <Link
            href={originalFile.signedUrl}
            className={oldFileText}
            variant="body1"
            target="_blank"
          >
            {originalFile.title}
          </Link>
        ) : (
          <></>
        )}
      </div>
      <Field
        name={name}
        render={({ field, form }: FieldProps) => (
          <div className={classes.newFile}>
            <input
              style={{ display: "none" }}
              ref={inputEl}
              type="file"
              onChange={event => {
                const files = event.target.files;
                if (files === null || files[0] === null) {
                  form.setFieldValue(name, undefined);
                  setFilename(title);
                } else {
                  form.setFieldValue(name, files[0]);
                  setFilename(files[0].name);
                  setIsNewFileSet(true);
                }
              }}
            />
            <IconButton disabled={form.isSubmitting} onClick={openFileDialog}>
              <AttachFileIcon />
            </IconButton>
            <Typography variant="body1" noWrap>
              {filename}
            </Typography>
          </div>
        )}
      />
    </div>
  );
};

interface IFileLocation {
  __typename: "FileLocation";
  id: string;
  signedUrl: string;
  title: string;
}

export const apiToFormikFileField = (apiFile: IFileLocation | null) => {
  if (apiFile === null) {
    return undefined;
  } else {
    const formikField: IOriginalFile = {
      title: apiFile.title,
      signedUrl: apiFile.signedUrl
    };

    return formikField;
  }
};

export default UpdateFileField;
