import React from "react";
import * as ROUTES from "../../constants/routes";
import { Switch, Route } from "react-router-dom";
import PrivateRoute from "./PrivateRoute";

import SignUp from "../SignUp";
import SignIn from "../SignIn";
import { CreateBatchForm, ReadBatch, ReadBatches } from "../Batch";
import { CreateLicenseHolder, ReadLicenseHolder } from "../LicenseHolder";
import { CreateTestingLab, ReadTestingLab } from "../TestingLab";
import { ReadTests, CreateTest, UpdateTest } from "../BatchTests";
import Demo from "../Demo";
import { ReadUserTodos } from "../UserTodo";
import MyAccount from "../MyAccount";
import DemoSignin from "../DemoSignin";
import Pricing from "../Pricing";

interface IRoutes {
  path: string;
  view: any;
  isPrivate: boolean;
}

const ROUTESOBJECT: IRoutes[] = [
  // Misc
  { path: ROUTES.DEMO, view: Demo, isPrivate: false },
  { path: ROUTES.DEMO_SIGNIN, view: DemoSignin, isPrivate: false },
  { path: ROUTES.PRICING, view: Pricing, isPrivate: false },
  // { path: "/landing", view: Landing, isPrivate: false },
  // Batches
  { path: ROUTES.CREATE_BATCH, view: CreateBatchForm, isPrivate: true },
  { path: ROUTES.READ_BATCH, view: ReadBatch, isPrivate: true },
  { path: ROUTES.READ_BATCHES, view: ReadBatches, isPrivate: true },
  // Auth
  { path: ROUTES.SIGN_UP, view: SignUp, isPrivate: false },
  { path: ROUTES.LOGIN, view: SignIn, isPrivate: false },
  { path: ROUTES.MY_ACCOUNT, view: MyAccount, isPrivate: true },
  // License Holders
  {
    path: ROUTES.CREATE_LICENSE_HOLDER,
    view: CreateLicenseHolder,
    isPrivate: true
  },
  {
    path: ROUTES.READ_LICENSE_HOLDER,
    view: ReadLicenseHolder,
    isPrivate: true
  },
  // Testing Labs
  { path: ROUTES.CREATE_TESTING_LAB, view: CreateTestingLab, isPrivate: true },
  { path: ROUTES.READ_TESTING_LAB, view: ReadTestingLab, isPrivate: true },
  // Tests
  { path: ROUTES.READ_TESTS, view: ReadTests, isPrivate: true },
  { path: ROUTES.CREATE_TEST, view: CreateTest, isPrivate: true },
  { path: ROUTES.UPDATE_TEST, view: UpdateTest, isPrivate: true },

  // UserTodos
  { path: ROUTES.READ_USER_TODOS, view: ReadUserTodos, isPrivate: true }
];

const routes = (
  <Switch>
    {ROUTESOBJECT.map(({ path, view: View, isPrivate }) => {
      if (isPrivate) {
        return <PrivateRoute exact key={path} path={path} component={View} />;
      } else {
        return <Route exact key={path} path={path} component={View} />;
      }
    })}
  </Switch>
);

export default routes;
