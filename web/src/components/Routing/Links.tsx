import React from "react";
import { Link as RouterLink } from "react-router-dom";
import * as ROUTES from "../../constants/routes";

export const HomeLink: React.SFC<any> = props => (
  <RouterLink to={ROUTES.HOME} {...props} />
);

export const PricingLink: React.SFC<any> = props => (
  <RouterLink to={ROUTES.PRICING} {...props} />
);

export const DemoLink: React.SFC<any> = props => (
  <RouterLink to={ROUTES.DEMO} {...props} />
);

// export const ContactLink: React.SFC<any> = props => (
//   <RouterLink href={`mailto:hi@complicanna.com`} {...props} />
// );

export const LoginLink: React.SFC<any> = props => (
  <RouterLink to={ROUTES.LOGIN} {...props} />
);

export const SignUpLink: React.SFC<any> = props => (
  <RouterLink to={ROUTES.SIGN_UP} {...props} />
);

export const BatchesLink: React.SFC<any> = props => (
  <RouterLink to={ROUTES.READ_BATCHES} {...props} />
);

export const MyTodosLink: React.SFC<any> = props => (
  <RouterLink to={ROUTES.READ_USER_TODOS} {...props} />
);

export const MyAccountLink: React.SFC<any> = props => (
  <RouterLink to={ROUTES.MY_ACCOUNT} {...props} />
);

export const NewBatchLink: React.SFC<any> = props => (
  <RouterLink to={ROUTES.CREATE_BATCH} {...props} />
);

export const NewTestingLabLink: React.SFC<any> = props => (
  <RouterLink to={ROUTES.CREATE_TESTING_LAB} {...props} />
);

export const NewTestLink: React.SFC<any> = props => (
  <RouterLink to={ROUTES.CREATE_TEST} {...props} />
);

export const NewLicenseHolderLink: React.SFC<any> = props => (
  <RouterLink to={ROUTES.CREATE_LICENSE_HOLDER} {...props} />
);

export const NewEmployeeLink: React.SFC<any> = props => (
  <RouterLink to={ROUTES.CREATE_EMPLOYEE} {...props} />
);
