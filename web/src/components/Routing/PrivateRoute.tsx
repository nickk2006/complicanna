import React from "react";
import { Route, Redirect } from "react-router-dom";
import * as Routes from "../../constants/routes";
import { useAuthState } from "react-firebase-hooks/auth";
import firebase from "../Firebase";
import Loading from "../Loading";

const PrivateRoute: React.FC<any> = ({ component: Component, ...rest }) => {
  const { initialising, user } = useAuthState(firebase.auth());

  return (
    <Route
      {...rest}
      render={routeProps => {
        if (initialising) return <Loading />;
        if (typeof user !== "undefined" && user !== null)
          return <Component {...routeProps} />;

        return <Redirect to={Routes.LOGIN} />;
      }}
    />
  );
};

export default PrivateRoute;
