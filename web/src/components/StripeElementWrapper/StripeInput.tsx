import React from "react";
import { makeStyles, createStyles } from "@material-ui/styles";
import theme, { ITheme } from "../Theme";
import { InputBaseComponentProps } from "@material-ui/core/InputBase";

const useStyles = makeStyles((theme: ITheme) =>
  createStyles({
    root: {
      width: "100%",
      padding: "6px 0 7px",
      cursor: "text"
    }
  })
);

// interface IProps extends InputBaseComponentProps {
//   component: Function;
//   onBlur: Function;
//   onFocus: Function;
//   onChange: Function;
// }

const StripeInput: React.ReactType<InputBaseComponentProps> = props => {
  const { component: Component, onFocus, onChange, onBlur } = props;
  const classes = useStyles();

  return (
    <Component
      className={classes.root}
      onFocus={onFocus}
      onBlur={onBlur}
      onChange={onChange}
      placeholder=""
      style={{
        base: {
          fontSize: `${theme.typography.fontSize}px`,
          fontFamily: theme.typography.fontFamily,
          color: "#000000de"
        }
      }}
    />
  );
};

export default StripeInput;
