import React from "react";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Input from "@material-ui/core/Input";

import StripeInput from "./StripeInput";
import { FormHelperText } from "@material-ui/core";

const StripeElementWraper: React.FC<{
  component: Function;
  label: string;
}> = props => {
  const [focus, setFocus] = React.useState(false);
  const [empty, setEmpty] = React.useState(false);
  const [error, setError] = React.useState<Error | undefined>(undefined);
  const [touched, setTouched] = React.useState(false);

  const handleBlur = () => {
    setFocus(false);
    setTouched(true);
  };

  const handleFocus = () => {
    setFocus(true);
  };

  const handleChange = (event: any) => {
    console.log("EVENT", event);
    setError(event.error);
    setEmpty(event.empty);
  };

  const { component, label } = props;
  return (
    <>
      <FormControl
        fullWidth
        margin="normal"
        error={touched && typeof error !== "undefined"}
      >
        <InputLabel focused={focus} shrink={focus || !empty}>
          {label}
        </InputLabel>
        <Input
          fullWidth
          inputComponent={StripeInput}
          onFocus={handleFocus}
          onBlur={handleBlur}
          onChange={handleChange}
          inputProps={{ component }}
        />
        <FormHelperText id="component-error-text">
          {touched && error && error.message}
        </FormHelperText>
      </FormControl>
    </>
  );
};

export default StripeElementWraper;
