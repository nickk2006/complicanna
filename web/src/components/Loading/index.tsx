import React from "react";
import CircularProgress from "@material-ui/core/CircularProgress";
import { makeStyles } from "@material-ui/styles";
import { ITheme } from "../Theme";

const useStyles = makeStyles((theme: ITheme) => ({
  progress: {
    margin: theme.spacing.unit * 2
  }
}));

const Loading: React.FC<{}> = () => {
  const classes = useStyles();

  return <CircularProgress className={classes.progress} />;
};

export default Loading;
