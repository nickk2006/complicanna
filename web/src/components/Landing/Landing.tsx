import React from "react";
import classNames from "classnames";
import { makeStyles, createStyles } from "@material-ui/styles";
import { landingTheme, ITheme } from "../Theme";
import {
  banner,
  locationIcon,
  menuIcon as transparentIcon,
  profitIcon,
  batchesDesktop,
  batchIntake,
  batchSingle
} from "../../images";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import { unstable_useMediaQuery as useMediaQuery } from "@material-ui/core/useMediaQuery";
import { MuiThemeProvider as ThemeProvider } from "@material-ui/core/styles";
import { DemoLink, LoginLink } from "../Routing/Links";
import { bottomGutter } from "../Layout";

const useStyles = makeStyles((theme: ITheme) =>
  createStyles({
    root: {
      display: "grid",
      gridRowGap: "1em",
      [theme.breakpoints.down("sm")]: {
        gridTemplateColumns: "1fr",
        gridTemplateRows: "repeat(8, auto)",
        gridTemplateAreas:
          '"header" "main-one-A" "main-one-B" "main-two-B" "main-two-A" "main-three-A" "main-three-B" "footer"',
        ...bottomGutter
      },
      [theme.breakpoints.up("md")]: {
        gridTemplateColumns: "1fr 1fr",
        gridTemplateRows: "auto repeat(3, 600px) auto",
        gridTemplateAreas:
          '"header header" "main-one-A main-one-B" "main-two-A main-two-B" "main-three-A main-three-B" "footer footer"'
      }
    },
    header: {
      gridArea: "header",
      backgroundImage: `url(${banner})`,
      // backgroundColor: "#D3D3D3",
      [theme.breakpoints.down("sm")]: {
        height: 500
      },
      [theme.breakpoints.only("sm")]: {
        height: 500
      },
      [theme.breakpoints.up("md")]: {
        height: 500
      }
    },
    mainOneText: {
      gridArea: "main-one-A"
    },
    mainOneImage: {
      gridArea: "main-one-B"
    },
    mainTwoImage: {
      gridArea: "main-two-A"
    },
    mainTwoText: {
      gridArea: "main-two-B"
    },
    mainThreeText: {
      gridArea: "main-three-A"
    },
    mainThreeImage: {
      gridArea: "main-three-B"
    },
    imageContainer: {
      overflow: "hidden",
      [theme.breakpoints.down("sm")]: {
        display: "block",
        margin: "auto"
      }
    },
    image: {
      display: "block",
      margin: "auto",
      [theme.breakpoints.down("sm")]: {
        maxWidth: "100%",
        height: "auto"
      },

      [theme.breakpoints.up("md")]: {
        maxHeight: "100%",
        width: "auto"
      }
    },
    footer: {
      gridArea: "footer"
    }
  })
);

const useHeaderStyles = makeStyles((theme: ITheme) =>
  createStyles({
    root: {
      display: "grid",
      gridRowGap: "1em",
      [theme.breakpoints.down("sm")]: {
        gridTemplateRows: "1fr 2fr 2fr 1fr 1fr 1fr",
        gridTemplateColumns: "2fr minmax(200px, 1fr) 2fr",
        gridTemplateAreas:
          '". . ." "title title title" "subtitle subtitle subtitle" ". btn-one . " ". btn-two ."'
      },
      [theme.breakpoints.up("md")]: {
        gridTemplateRows: "2fr 3fr 3fr 1fr 2fr",
        gridTemplateColumns:
          "2fr 1fr minmax(200px, 1fr) minmax(200px, 1fr) 1fr 2fr",
        gridTemplateAreas:
          '". . . . . ." ". title title title title ." ". subtitle subtitle subtitle subtitle ." ". . btn-one btn-two . ." ". . . . . ."'
      }
    },
    title: {
      gridArea: "title",
      alignSelf: "end"
      // backgroundColor: "#adb2d3"
    },
    subtitle: {
      gridArea: "subtitle",
      alignSelf: "center"
      // backgroundColor: "#b1e6f0"
    },
    btnOne: {
      gridArea: "btn-one",
      placeSelf: "center"
      // backgroundColor: "#896279"
    },
    btnTwo: {
      gridArea: "btn-two",
      placeSelf: "center"
      // backgroundColor: "#eee5e9"
    }
  })
);

const useMainStyles = makeStyles((theme: ITheme) =>
  createStyles({
    root: {
      display: "grid",
      gridTemplateRows: "1fr 100px auto auto 1fr",
      gridRowGap: "3em"
    },
    leftRoot: {
      gridTemplateColumns:
        "1em 5fr minmax(auto, 100px) minmax(auto, 350px) 1fr 1em",
      gridTemplateAreas: `". . . . . ." ". . icon . . ." ". . text-one text-one . ." ". . text-two text-two . ." ". . . . . ."`
    },
    rightRoot: {
      gridTemplateColumns:
        "1em 1fr minmax(auto, 350px) minmax(auto, 100px) 5fr 1em",
      gridTemplateAreas: `". . . . . ." ". . . icon . ." ". . text-one text-one . ." ". . text-two text-two . ." ". . . . . ."`
    },
    iconContainer: {
      gridArea: "icon"
    },
    icon: {
      width: "100%"
    },
    header: {
      gridArea: "text-one"
    },
    subheader: {
      gridArea: "text-two"
    },
    leftImage: {
      justifySelf: "end"
    }
  })
);

const Layout: React.FC<{}> = () => {
  const classes = useStyles();
  const header = useHeaderStyles();
  const main = useMainStyles();
  const isScreenSmall = useMediaQuery(
    `(max-width:${landingTheme.breakpoints.values.sm}px)`
  );

  return (
    <ThemeProvider theme={landingTheme}>
      <div className={classes.root}>
        <div className={classNames(classes.header, header.root)}>
          <div className={header.title}>
            <Typography
              color="primary"
              variant={isScreenSmall ? "h3" : "h2"}
              align="center"
            >
              A new way to work
            </Typography>
          </div>
          <div className={header.subtitle}>
            <Typography
              color="primary"
              variant={isScreenSmall ? "h5" : "h4"}
              align="center"
            >
              One place to secure, share and manage all your distribution
              workflows
            </Typography>
          </div>
          <div className={header.btnOne}>
            <Button variant="contained" size="large" component={DemoLink}>
              Request A Demo
            </Button>
          </div>
          <div className={header.btnTwo}>
            <Button
              variant="outlined"
              color="primary"
              size="large"
              component={LoginLink}
            >
              Login
            </Button>
          </div>
        </div>
        <div
          className={classNames(classes.mainOneText, main.root, main.leftRoot)}
        >
          <div className={main.iconContainer}>
            <img className={main.icon} alt="location-icon" src={locationIcon} />
          </div>
          <div className={main.header}>
            <Typography color="inherit" variant="h4" align="left">
              Knowledge of batches from entry to exit
            </Typography>
          </div>
          <div className={main.subheader}>
            <Typography variant="subtitle1" align="left">
              Our curated interfaces give you a single, comprehensive view of
              each batch - every change, every second, in full context. See all
              your data in context.
            </Typography>
          </div>
        </div>
        <div
          className={classNames(classes.mainOneImage, classes.imageContainer)}
        >
          <img
            className={classes.image}
            alt="batches-screenshot"
            src={batchesDesktop}
          />
        </div>
        <div
          className={classNames(
            classes.mainTwoImage,
            main.leftImage,
            classes.imageContainer
          )}
        >
          <img
            className={classes.image}
            alt="overhead-screenshot"
            src={batchIntake}
          />
        </div>
        <div
          className={classNames(classes.mainTwoText, main.root, main.rightRoot)}
        >
          <div className={main.iconContainer}>
            <img className={main.icon} alt="profit-icon" src={profitIcon} />
          </div>
          <div className={main.header}>
            <Typography variant="h4" align="right">
              Reduce compliance overhead
            </Typography>
          </div>
          <div className={main.subheader}>
            <Typography variant="subtitle1" align="right">
              Cannabis is an intensely regulated industry. Eliminate cumbersome
              paper and email-based workflows, manage digital assets across your
              entire ecosystem, and better manage and govern business-critical
              records.
            </Typography>
          </div>
        </div>
        <div
          className={classNames(
            classes.mainThreeText,
            main.root,
            main.leftRoot
          )}
        >
          <div className={main.iconContainer}>
            <img
              className={main.icon}
              alt="transparency-icon"
              src={transparentIcon}
            />
          </div>
          <div className={main.header}>
            <Typography variant="h4" align="left">
              Transparent workflows
            </Typography>
          </div>
          <div className={main.subheader}>
            <Typography variant="subtitle1" align="left">
              Get everyone on the same page and empower your teams. You can
              orient around what matters, increase throughput, and accelerate
              turnaround cycles.
            </Typography>
          </div>
        </div>
        <div
          className={classNames(classes.mainThreeImage, classes.imageContainer)}
        >
          <img
            className={classes.image}
            alt="batch-screenshot"
            src={batchSingle}
          />
        </div>
        {/* <div className={classes.footer}>
          <Typography variant="body1" align="center">
            FOOTER PLACEHOLDER
          </Typography>
        </div> */}
      </div>
    </ThemeProvider>
  );
};

export default Layout;
