import React from "react";
import { useListStyles } from "../Theme";
import TextCell from "../TextCell";
import ValueCell, { ILink } from "../ValueCell";

const DataRow: React.FC<{
  label: string;
  value: string;
  link?: ILink;
  [key: string]: any;
}> = props => {
  const { label, value, link, ...typography } = props;
  const classes = useListStyles();
  return (
    <div className={classes.dataRow}>
      <TextCell text={label} />
      <ValueCell value={value} link={link} {...typography} />
    </div>
  );
};

export default DataRow;
