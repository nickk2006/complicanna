import React from "react";
import Avatar from "@material-ui/core/Avatar";
import { makeStyles, createStyles } from "@material-ui/styles";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import { ITheme } from "../Theme";
import SignInFormBase from "./SignInForm";
import { IFormValues } from "./types";

const useStyles = makeStyles((theme: ITheme) =>
  createStyles({
    paper: {
      marginTop: theme.spacing.unit * 8,
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
      padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme
        .spacing.unit * 3}px`
    },
    avatar: {
      margin: theme.spacing.unit,
      backgroundColor: theme.palette.primary.main
    },
    form: {
      width: "100%", // Fix IE 11 issue.
      marginTop: theme.spacing.unit
    },
    submit: {
      marginTop: theme.spacing.unit * 3
    },
    header: {
      marginBottom: "1em"
    }
  })
);

const SignIn: React.FC<{ initialValues?: IFormValues }> = props => {
  const classes = useStyles();

  return (
    <Paper className={classes.paper}>
      <Avatar className={classes.avatar}>
        <LockOutlinedIcon />
      </Avatar>
      <Typography className={classes.header} component="h1" variant="h5">
        Sign in to your account
      </Typography>
      <SignInFormBase initialValues={props.initialValues} />
    </Paper>
  );
};

export default SignIn;
