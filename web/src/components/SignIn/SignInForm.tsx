import React from "react";
import { IFormValues } from "./types";
import * as ROUTES from "../../constants/routes";
import { Formik, Form } from "formik";
import firebase from "../Firebase";
import useReactRouter from "use-react-router";
import { ITheme } from "../Theme";
import { makeStyles, createStyles } from "@material-ui/styles";
import { Email, Password, SubmitBtn } from "../FormComponents";
import schema from "./signInFormValidation";

const INITIAL_VALUES: IFormValues = {
  email: "",
  password: ""
};

const useStyles = makeStyles((theme: ITheme) =>
  createStyles({
    root: {
      display: "grid",
      gridTemplateColumns: "minmax(auto, 330px)",
      gridRowGap: "2em"
    }
  })
);

const SignInFormBase: React.FC<{ initialValues?: IFormValues }> = ({
  initialValues = INITIAL_VALUES
}) => {
  const { history } = useReactRouter();
  const classes = useStyles();

  const handleSubmit = async ({ email, password }: IFormValues) => {
    const userCredential = await firebase
      .auth()
      .signInWithEmailAndPassword(email, password);

    if (userCredential.user === null) {
      throw new Error("User does not exist");
    }
    // const token = await userCredential.user.getIdTokenResult();

    //TODO: Create type for user that includes claims?
    // const companyId = token.claims.companyId as string;

    const route = ROUTES.READ_BATCHES;
    history.push(route);
  };

  return (
    <Formik
      initialValues={initialValues}
      validateOnBlur={false}
      validateOnChange={false}
      validationSchema={schema}
      onSubmit={handleSubmit}
    >
      {formikProps => (
        <Form className={classes.root}>
          <Email />
          <Password />
          <SubmitBtn disabled={formikProps.isSubmitting}>Sign In</SubmitBtn>
        </Form>
      )}
    </Formik>
  );
};
export default SignInFormBase;
