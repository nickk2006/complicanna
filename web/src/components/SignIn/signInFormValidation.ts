import * as yup from "yup";
import { requiredString, email } from "../FormComponents";
import { IFormValues } from "./types";

const schema: yup.ObjectSchema<IFormValues> = yup.object().shape({
  email,
  password: requiredString
});

export default schema;
