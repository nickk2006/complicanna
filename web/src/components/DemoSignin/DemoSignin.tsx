import React from "react";
import useRouter from "use-react-router";
import * as ROUTES from "../../constants/routes";
import firebase from "../Firebase";
import Loading from "../Loading";

interface ILogin {
  email: string;
  password: string;
}

const DemoSignin: React.FC<{}> = props => {
  const { location, history } = useRouter();
  const params = new URLSearchParams(location.search);

  const encoded = params.get("user");

  if (encoded === null) {
    history.push(ROUTES.LOGIN);
    return <div />;
  }
  const decoded = atob(encoded);
  const { email, password } = JSON.parse(decoded) as ILogin;

  if (typeof email === "undefined" || typeof password === "undefined") {
    history.push(ROUTES.LOGIN);
    return <div />;
  }

  firebase
    .auth()
    .signInWithEmailAndPassword(email, password)
    .then(() => history.push(ROUTES.READ_BATCHES))
    .catch(() => history.push(ROUTES.LOGIN));

  return <Loading />;
};

export default DemoSignin;
