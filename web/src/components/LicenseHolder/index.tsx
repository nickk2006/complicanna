import CreateLicenseHolder from "./CreateLicenseHolder";
import ReadLicenseHolder from "./ReadLicenseHolder";

export { CreateLicenseHolder, ReadLicenseHolder };
