import LicenseHolderPicker from "./LicenseHolderPicker";

export { READ_LICENSE_HOLDERS_QUERY } from "./LicenseHolderPicker";

export default LicenseHolderPicker;
