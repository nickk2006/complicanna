import React from "react";
import { gql } from "apollo-boost";
import { Query } from "react-apollo";
import { Field } from "formik";
import { TextField } from "formik-material-ui";
import MenuItem from "@material-ui/core/MenuItem";
import Loading from "../../Loading";

import { licenseHoldersForPicker } from "./__generated__/licenseHoldersForPicker";
import { usePickerStyle } from "../../Theme";

export const READ_LICENSE_HOLDERS_QUERY = gql`
  query licenseHoldersForPicker {
    licenseHolders {
      id
      name
    }
  }
`;

interface IPropsLicensePickerBase extends licenseHoldersForPicker {
  name: string;
  label: string;
}

const LicensePickerBase: React.FC<IPropsLicensePickerBase> = ({
  name,
  licenseHolders,
  label
}) => {
  const pickerStyle = usePickerStyle();

  return (
    <div className={pickerStyle.root}>
      <Field
        name={name}
        label={label}
        variant="outlined"
        select
        component={TextField}
      >
        {licenseHolders.map(holder => (
          <MenuItem key={holder.id} value={holder.id}>
            {holder.name}
          </MenuItem>
        ))}
      </Field>
    </div>
  );
};

class LicenseHolderPickerQuery extends Query<licenseHoldersForPicker> {}

const LicensePicker: React.SFC<{ name: string; label: string }> = ({
  name,
  label
}) => (
  <LicenseHolderPickerQuery query={READ_LICENSE_HOLDERS_QUERY}>
    {({ data, loading, error }) => {
      if (loading) return <Loading />;
      if (typeof data !== "undefined")
        return (
          <LicensePickerBase
            licenseHolders={data.licenseHolders}
            label={label}
            name={name}
          />
        );
      if (error) {
        console.log("ERROR", error);
        return <div>ERROR: {error.message}</div>;
      }
    }}
  </LicenseHolderPickerQuery>
);

export default LicensePicker;
