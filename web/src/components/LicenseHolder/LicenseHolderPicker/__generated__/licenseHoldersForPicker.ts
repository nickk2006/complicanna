/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: licenseHoldersForPicker
// ====================================================

export interface licenseHoldersForPicker_licenseHolders {
  __typename: "LicenseHolder";
  id: string;
  name: string;
}

export interface licenseHoldersForPicker {
  licenseHolders: licenseHoldersForPicker_licenseHolders[];
}
