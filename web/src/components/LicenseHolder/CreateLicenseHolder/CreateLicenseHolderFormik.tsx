import React from "react";
import { Formik, FormikActions } from "formik";
import { MutationFn } from "react-apollo";
import * as ROUTES from "../../../constants/routes";
import useReactRouter from "use-react-router";
import { CreateLicenseHolderInput } from "../../../types/graphql-global-types";
import {
  createLicenseHolder,
  createLicenseHolderVariables
} from "./__generated__/createLicenseHolder";
import CreateLicenseHolderForm from "./CreateLicenseHolderForm";
import schema from "./createLicenseHolderValidation";

interface ICreateLicenseInput {
  number: string;
  type?: string;
}

export interface IFormValues {
  name: string;
  license: ICreateLicenseInput;
}

const INITIAL_VALUES: IFormValues = {
  name: "",
  license: {
    number: "",
    type: ""
  }
};

const webToApi = (values: IFormValues) => {
  const valueType = values.license.type;
  const type = typeof valueType !== "undefined" ? parseInt(valueType) : 0;
  const data: CreateLicenseHolderInput = {
    ...values,
    license: {
      number: values.license.number,
      type
    }
  };

  return data;
};

const FormikForm: React.SFC<{
  createLicenseHolder: MutationFn<
    createLicenseHolder,
    createLicenseHolderVariables
  >;
}> = props => {
  const { history } = useReactRouter();

  const handleSubmit = async (
    values: IFormValues,
    formikBag: FormikActions<IFormValues>
  ) => {
    const data = webToApi(values);
    const res = await props.createLicenseHolder({ variables: { data } });
    if (typeof res !== "undefined" && typeof res.data !== "undefined") {
      formikBag.resetForm();
      history.push(
        ROUTES.READ_LICENSE_HOLDER_FN(res.data.createLicenseHolder.id)
      );
    }
  };

  return (
    <Formik
      initialValues={INITIAL_VALUES}
      onSubmit={handleSubmit}
      validationSchema={schema}
    >
      {formikProps => <CreateLicenseHolderForm {...formikProps} />}
    </Formik>
  );
};

export default FormikForm;
