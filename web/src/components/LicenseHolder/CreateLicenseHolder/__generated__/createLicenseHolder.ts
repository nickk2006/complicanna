/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

import { CreateLicenseHolderInput } from "./../../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: createLicenseHolder
// ====================================================

export interface createLicenseHolder_createLicenseHolder {
  __typename: "LicenseHolder";
  id: string;
}

export interface createLicenseHolder {
  createLicenseHolder: createLicenseHolder_createLicenseHolder;
}

export interface createLicenseHolderVariables {
  data: CreateLicenseHolderInput;
}
