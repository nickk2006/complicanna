import React from "react";
import { Field, FormikProps } from "formik";
import { TextField } from "formik-material-ui";
import LicensePicker from "../../License/LicensePicker";
import { Title, SubmitBtn, Form } from "../../FormComponents";
import { IFormValues } from "./CreateLicenseHolderFormik";

interface IProps extends FormikProps<IFormValues> {}

const LicenseHolderForm: React.SFC<IProps> = props => {
  return (
    <Form>
      <Title>New Manufacturer / Distributor</Title>
      <Field
        name="name"
        label="License Holder Name"
        variant="outlined"
        component={TextField}
      />
      <Field
        name="license.number"
        label="License ID"
        variant="outlined"
        component={TextField}
      />
      <LicensePicker name="license.type" />
      <SubmitBtn disabled={props.isSubmitting}>Create License Holder</SubmitBtn>
    </Form>
  );
};

export default LicenseHolderForm;
