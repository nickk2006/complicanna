import React from "react";
import { gql } from "apollo-boost";
import { Mutation } from "react-apollo";
import CreateLicenseHolderFormik from "./CreateLicenseHolderFormik";
import {
  createLicenseHolderVariables,
  createLicenseHolder
} from "./__generated__/createLicenseHolder";

export const CREATE_LICENSE_HOLDER = gql`
  mutation createLicenseHolder($data: CreateLicenseHolderInput!) {
    createLicenseHolder(data: $data) {
      id
    }
  }
`;

class CreateLicenseHolderMutation extends Mutation<
  createLicenseHolder,
  createLicenseHolderVariables
> {}

const ConnectedForm: React.SFC<{}> = () => (
  <CreateLicenseHolderMutation mutation={CREATE_LICENSE_HOLDER}>
    {createLicenseHolder => (
      <CreateLicenseHolderFormik createLicenseHolder={createLicenseHolder} />
    )}
  </CreateLicenseHolderMutation>
);

export default ConnectedForm;
