import * as yup from "yup";
import { requiredString, REQUIRED } from "../../FormComponents";
import {
  CreateLicenseHolderInput,
  CreateLicenseInput
} from "../../../types/graphql-global-types";

const licenseSchema: yup.ObjectSchema<CreateLicenseInput> = yup.object({
  number: requiredString,
  type: yup.number().required(REQUIRED)
});

const createLicenseHolderValidation: yup.ObjectSchema<
  CreateLicenseHolderInput
> = yup.object({
  name: requiredString,
  license: licenseSchema.required()
});

export default createLicenseHolderValidation;
