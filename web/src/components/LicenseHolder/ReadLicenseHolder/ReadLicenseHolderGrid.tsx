import React from "react";
import {
  licenseHolder,
  licenseHolder_licenseHolder_batchesSent_for,
  licenseHolder_licenseHolder_batchesReceived_from
} from "./__generated__/licenseHolder";
import { ListItemTypography } from "../../Typography";
// import * as ROUTES from "../../../constants/routes";
import { useReadListStyle } from "../../Theme";
import Typography from "@material-ui/core/Typography";

import * as ROUTES from "../../../constants/routes";
import Link from "@material-ui/core/Link";
import { Link as RouterLink } from "react-router-dom";

export const ReadLicenseHolderBase: React.SFC<licenseHolder> = ({
  licenseHolder
}) => {
  const classes = useReadListStyle();

  if (licenseHolder !== null) {
    const { name, license, batchesReceived, batchesSent } = licenseHolder;
    return (
      <div className={classes.root}>
        <div className={classes.titleRow}>
          <Typography variant="h6">Information</Typography>
        </div>
        <div className={classes.dataRow}>
          <ListItemTypography>Name</ListItemTypography>
          <ListItemTypography>{name}</ListItemTypography>
        </div>
        <div className={classes.dataRow}>
          <ListItemTypography>License Type</ListItemTypography>
          <ListItemTypography>{license.type}</ListItemTypography>
        </div>
        <div className={classes.dataRow}>
          <ListItemTypography>License Number</ListItemTypography>
          <ListItemTypography>{license.number}</ListItemTypography>
        </div>
        <div className={classes.titleRow}>
          <Typography variant="h6">Batches Sent</Typography>
        </div>
        {batchesSent !== null &&
          batchesSent.map(batch => (
            <ReadLicenseBatchSection
              key={batch.id}
              batchId={batch.id}
              licenseHolder={batch.for}
            />
          ))}
        <div className={classes.titleRow}>
          <Typography variant="h6">Batches Received</Typography>
        </div>
        {batchesReceived !== null &&
          batchesReceived.map(batch => (
            <ReadLicenseBatchSection
              key={batch.id}
              batchId={batch.id}
              licenseHolder={batch.from}
            />
          ))}
      </div>
    );
  } else {
    return <></>;
  }
};

interface IReadLicenseBatchSectionProps {
  licenseHolder:
    | licenseHolder_licenseHolder_batchesSent_for
    | licenseHolder_licenseHolder_batchesReceived_from;
  batchId: string;
}

const ReadLicenseBatchSection: React.FC<
  IReadLicenseBatchSectionProps
> = props => {
  const { batchId, licenseHolder } = props;
  const classes = useReadListStyle();

  const BatchLink: React.SFC<any> = props => (
    <RouterLink to={ROUTES.READ_BATCH_FN(batchId)} {...props} />
  );

  const LicenseHolderLink: React.SFC<any> = props => (
    <RouterLink
      to={ROUTES.READ_LICENSE_HOLDER_FN(licenseHolder.id)}
      {...props}
    />
  );

  return (
    <div key={batchId}>
      <div className={classes.dataRow}>
        <ListItemTypography>ID</ListItemTypography>
        <ListItemTypography>
          <Link component={BatchLink}>{batchId}</Link>
        </ListItemTypography>
      </div>
      <div className={classes.dataRow}>
        <ListItemTypography>Sent To</ListItemTypography>
        <ListItemTypography>
          <Link component={LicenseHolderLink}>{licenseHolder.name}</Link>
        </ListItemTypography>
      </div>
    </div>
  );
};

export default ReadLicenseHolderBase;
