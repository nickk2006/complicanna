/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

import { LicenseHolderWhereUniqueInput } from "./../../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: licenseHolder
// ====================================================

export interface licenseHolder_licenseHolder_license {
  __typename: "License";
  id: string;
  number: string;
  type: number;
}

export interface licenseHolder_licenseHolder_batchesSent_for {
  __typename: "LicenseHolder";
  id: string;
  name: string;
}

export interface licenseHolder_licenseHolder_batchesSent {
  __typename: "Batch";
  id: string;
  for: licenseHolder_licenseHolder_batchesSent_for;
}

export interface licenseHolder_licenseHolder_batchesReceived_from {
  __typename: "LicenseHolder";
  id: string;
  name: string;
}

export interface licenseHolder_licenseHolder_batchesReceived {
  __typename: "Batch";
  id: string;
  from: licenseHolder_licenseHolder_batchesReceived_from;
}

export interface licenseHolder_licenseHolder {
  __typename: "LicenseHolder";
  id: string;
  name: string;
  license: licenseHolder_licenseHolder_license;
  batchesSent: licenseHolder_licenseHolder_batchesSent[] | null;
  batchesReceived: licenseHolder_licenseHolder_batchesReceived[] | null;
}

export interface licenseHolder {
  licenseHolder: licenseHolder_licenseHolder | null;
}

export interface licenseHolderVariables {
  where: LicenseHolderWhereUniqueInput;
}
