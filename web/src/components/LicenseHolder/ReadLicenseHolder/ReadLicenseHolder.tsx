import React from "react";
import { gql } from "apollo-boost";
import { Query } from "react-apollo";
import { withRouter, RouteComponentProps } from "react-router";
import {
  licenseHolder,
  licenseHolderVariables
} from "./__generated__/licenseHolder";
import ReadLicenseHolderBase from "./ReadLicenseHolderGrid";
import Loading from "../../Loading";

export const READ_LICENSE_HOLDER = gql`
  query licenseHolder($where: LicenseHolderWhereUniqueInput!) {
    licenseHolder(where: $where) {
      id
      name
      license {
        id
        number
        type
      }
      batchesSent {
        id
        for {
          id
          name
        }
      }
      batchesReceived {
        id
        from {
          id
          name
        }
      }
    }
  }
`;

class LicenseHolderQuery extends Query<licenseHolder, licenseHolderVariables> {}

interface IProps extends RouteComponentProps<{ licenseHolderId: string }> {}

export const ReadLicenseHolder: React.SFC<IProps> = props => (
  <LicenseHolderQuery
    query={READ_LICENSE_HOLDER}
    variables={{ where: { id: props.match.params.licenseHolderId } }}
  >
    {({ data, loading, error }) => {
      if (loading) return <Loading />;
      if (typeof error !== "undefined")
        return <div>error: {error.message}</div>;
      if (typeof data !== "undefined")
        return <ReadLicenseHolderBase licenseHolder={data.licenseHolder} />;
    }}
  </LicenseHolderQuery>
);

export default withRouter<IProps>(ReadLicenseHolder);
