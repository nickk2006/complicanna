import React from "react";
import { Link as RouterLink } from "react-router-dom";
import Link from "@material-ui/core/Link";
import TextCell from "../TextCell";
import { TypographyProps } from "@material-ui/core/Typography";

interface ILinkRowProps {
  link: ILink;
  value: string;
  typography?: TypographyProps;
}

export enum LinkType {
  ROUTE,
  EXTERNAL_URL
}

export interface ILink {
  path: string;
  type: LinkType;
}

interface ILinkProps {
  path: string;
  value: string;
  typography?: any;
}

const RouteLink: React.SFC<ILinkProps> = props => {
  const { path, value, children, ...typography } = props;

  const RouterLinkComponent: React.SFC<any> = routerLinkProps => (
    <RouterLink to={path} {...routerLinkProps} />
  );

  return (
    <Link variant="body1" component={RouterLinkComponent} {...typography}>
      {value}
    </Link>
  );
};

const ExternalLink: React.SFC<ILinkProps> = props => {
  const { path, value, ...typography } = props;

  return (
    <Link variant="body1" href={path} target="_blank" {...typography}>
      {value}
    </Link>
  );
};

const LinkRow: React.SFC<ILinkRowProps> = props => {
  const {
    link: { path, type },
    value,
    ...typography
  } = props;

  switch (type) {
    case LinkType.EXTERNAL_URL:
      return <ExternalLink typography={typography} path={path} value={value} />;
    case LinkType.ROUTE:
    default:
      return <RouteLink typography={typography} path={path} value={value} />;
  }
};

const ValueCell: React.SFC<{
  value: string;
  link?: ILink;
  typography?: TypographyProps;
}> = props => {
  const { value, link, ...typography } = props;

  if (typeof link === "undefined") {
    return <TextCell text={value} {...typography} />;
  }
  return <LinkRow value={value} link={link} {...typography} />;
};

export default ValueCell;
