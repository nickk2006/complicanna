import React from "react";
import { Formik } from "formik";
import { IFormValues, IPlanParam } from "./types";
import * as Yup from "yup";
import { Form, Title, SubmitBtn, PlanPicker } from "../FormComponents";
import { gql } from "apollo-boost";
import { Mutation, MutationFn } from "react-apollo";
import {
  SignupAdmin_signUpAdmin,
  SignupAdminVariables
} from "./__generated__/SignupAdmin";
import {
  FirstName,
  LastName,
  Email,
  Password,
  CompanyName
} from "./FormFields";
import { checkoutRedirect } from "../Stripe";
import { statewidePlan } from "../../constants/plans";
import useReactRouter from "use-react-router";

const SIGNUP_ADMIN = gql`
  mutation SignupAdmin($input: SignUpInput!) {
    signUpAdmin(input: $input) {
      id
    }
  }
`;

const getInitialValues = (planId: string | null): IFormValues => ({
  firstName: "",
  lastName: "",
  email: "",
  password: "",
  companyName: "",
  plan: planId !== null ? planId : statewidePlan.planId
});

const SignUpSchema: Yup.ObjectSchema<IFormValues> = Yup.object({
  firstName: Yup.string().required("Required"),
  lastName: Yup.string().required("Required"),
  email: Yup.string()
    .email("Invalid Email")
    .required("Required"),
  password: Yup.string()
    .min(8, "Min 8 Characters")
    .required("Required"),
  companyName: Yup.string().required("Required"),
  plan: Yup.string().required("Required")
});

class SignUpMutation extends Mutation<
  SignupAdmin_signUpAdmin,
  SignupAdminVariables
> {}

const handleSubmit = (
  signupAdmin: MutationFn<SignupAdmin_signUpAdmin, SignupAdminVariables>
) => async (values: IFormValues) => {
  const { plan, ...input } = values;
  await signupAdmin({ variables: { input } });
  checkoutRedirect(plan, values.email);
};

const SignUpFormBase: React.FC<{}> = () => {
  const { location } = useReactRouter<IPlanParam>();

  const searchParams = new URLSearchParams(location.search);
  const planId = searchParams.get("id");

  const initialValues = getInitialValues(planId);

  return (
    <SignUpMutation mutation={SIGNUP_ADMIN}>
      {signUpAdmin => (
        <Formik
          initialValues={initialValues}
          validationSchema={SignUpSchema}
          validateOnBlur={false}
          validateOnChange={false}
          onSubmit={handleSubmit(signUpAdmin)}
        >
          {formikProps => (
            <Form short>
              <Title variant="h5">Create a CompliCanna Account</Title>
              <PlanPicker />
              <FirstName />
              <LastName />
              <Email />
              <Password />
              <CompanyName name="companyName" />
              <SubmitBtn disabled={formikProps.isSubmitting}>
                Create Account & Checkout
              </SubmitBtn>
            </Form>
          )}
        </Formik>
      )}
    </SignUpMutation>
  );
};

export default SignUpFormBase;
