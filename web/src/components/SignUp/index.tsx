import React from "react";
import { Link } from "react-router-dom";
import * as ROUTES from "../../constants/routes";
import SignUpFormBase from "./SignUpFormBase";

const SignUpLink = () => (
  <p>
    Don't have an account? <Link to={ROUTES.SIGN_UP}>Sign Up</Link>
  </p>
);

const SignUpForm: React.SFC<{}> = () => {
  return <SignUpFormBase />;
};

export default SignUpForm;

export { SignUpLink };
