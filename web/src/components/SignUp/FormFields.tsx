import React from "react";
import { Field } from "formik";
import { TextField } from "formik-material-ui";
import MenuItem from "@material-ui/core/MenuItem";
import { usePickerStyle } from "../Theme";

export const FirstName: React.SFC<{}> = () => (
  <Field
    name="firstName"
    label="First Name"
    variant="outlined"
    component={TextField}
  />
);

export const LastName: React.SFC<{}> = () => (
  <Field
    name="lastName"
    label="Last Name"
    variant="outlined"
    component={TextField}
  />
);

export const Email: React.SFC<{}> = () => (
  <Field
    name="email"
    type="email"
    label="Email"
    variant="outlined"
    component={TextField}
  />
);

export const Password: React.SFC<{}> = () => (
  <Field
    name="password"
    type="password"
    label="Password"
    variant="outlined"
    component={TextField}
  />
);

export const CompanyName: React.SFC<{ name?: string }> = props => {
  const { name } = props;

  return (
    <Field
      name={name || "company.name"}
      label="Company Name"
      variant="outlined"
      component={TextField}
    />
  );
};

export const AddressLineOne: React.SFC<{}> = () => (
  <Field
    name="company.address.lineOne"
    label="Address Line One"
    variant="outlined"
    component={TextField}
  />
);

export const AddressLineTwo: React.SFC<{}> = () => (
  <Field
    name="company.address.lineTwo"
    label="Address Line Two"
    variant="outlined"
    component={TextField}
  />
);

export const City: React.SFC<{}> = () => (
  <Field
    name="company.address.city"
    label="City"
    variant="outlined"
    component={TextField}
  />
);

export const State: React.SFC<{}> = () => {
  const pickerStyle = usePickerStyle();
  return (
    <div className={pickerStyle.root}>
      <Field
        name="company.address.state"
        label="State"
        type="select"
        variant="outlined"
        select
        component={TextField}
      >
        <MenuItem value={"CA"}>California</MenuItem>
      </Field>
    </div>
  );
};

export const Zip: React.SFC<{}> = () => (
  <Field
    name="company.address.zip"
    label="Zip Code"
    variant="outlined"
    component={TextField}
  />
);
