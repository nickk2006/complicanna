import { SignUpInput } from "../../types/graphql-global-types";

export interface ILicense {
  number: string;
  type: number | string;
}

export interface IAddress {
  lineOne: string;
  lineTwo: string;
  city: string;
  state: string;
  zip: string;
}

export interface ICompany {
  name: string;
  address: IAddress;
  licenses: Array<ILicense>;
}

export interface IFormValues extends SignUpInput {
  plan: string;
}

export interface IPlanParam {
  id?: string;
}
