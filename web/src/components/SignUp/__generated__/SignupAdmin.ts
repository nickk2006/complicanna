/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

import { SignUpInput } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: SignupAdmin
// ====================================================

export interface SignupAdmin_signUpAdmin {
  __typename: "Company";
  id: string;
}

export interface SignupAdmin {
  signUpAdmin: SignupAdmin_signUpAdmin | null;
}

export interface SignupAdminVariables {
  input: SignUpInput;
}
