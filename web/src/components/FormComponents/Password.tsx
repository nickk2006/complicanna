import React from "react";
import { Field } from "formik";
import { TextField } from "formik-material-ui";

const Password: React.SFC<{}> = props => {
  return (
    <Field
      name="password"
      type="password"
      label="Password"
      variant="outlined"
      component={TextField}
    />
  );
};

export default Password;
