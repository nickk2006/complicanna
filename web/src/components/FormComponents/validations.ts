import * as Yup from "yup";

export const REQUIRED = "Required";

export const email = Yup.string()
  .email("Invalid email")
  .required(REQUIRED);

export const requiredString = Yup.string().required(REQUIRED);
