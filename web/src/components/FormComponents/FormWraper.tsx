import React from "react";
import classNames from "classnames";
import { Form } from "formik";
import Paper from "@material-ui/core/Paper";
import { ITheme } from "../Theme";
import { makeStyles, createStyles } from "@material-ui/styles";

const useStyle = makeStyles((theme: ITheme) =>
  createStyles({
    root: {
      ...theme.mixins.gutters(),
      paddingTop: theme.spacing.unit * 2,
      paddingBottom: theme.spacing.unit * 2
    },
    form: {
      display: "grid",
      gridRowGap: "2em"
    },
    normalForm: {
      gridTemplateColumns: `minmax(auto, ${theme.breakpoints.values.sm}px)`
    },
    shortForm: {
      gridTemplateColumns: `minmax(auto, 395px)`
    }
  })
);
const FormWraper: React.SFC<{ short?: boolean }> = props => {
  const { short = false } = props;
  const classes = useStyle();

  return (
    <Paper className={classes.root} elevation={1}>
      <Form
        className={classNames(
          classes.form,
          short ? classes.shortForm : classes.normalForm
        )}
      >
        {props.children}
      </Form>
    </Paper>
  );
};

export default FormWraper;
