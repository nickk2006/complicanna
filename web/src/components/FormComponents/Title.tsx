import React from "react";
import Typography, { TypographyProps } from "@material-ui/core/Typography";

const Title: React.SFC<{ variant?: TypographyProps["variant"] }> = props => {
  const { variant = "h4" } = props;
  return (
    <Typography variant={variant} align="center">
      {props.children}
    </Typography>
  );
};

export default Title;
