import React from "react";
import { Field } from "formik";
import { TextField } from "formik-material-ui";

const Email: React.SFC<{}> = props => {
  return (
    <Field
      name="email"
      type="email"
      label="Email"
      variant="outlined"
      component={TextField}
    />
  );
};

export default Email;
