import React from "react";
import MenuItem from "@material-ui/core/MenuItem";
import { Field } from "formik";
import { ITheme } from "../Theme";
import { makeStyles } from "@material-ui/styles";
import { createStyles } from "@material-ui/core";
import { TextField } from "formik-material-ui";

const useStyles = makeStyles((theme: ITheme) =>
  createStyles({
    weight: {
      display: "grid",
      gridTemplateColumns: "100px 100px",
      gridColumnGap: "0.5em"
    }
  })
);

const WeightField: React.SFC<{}> = () => {
  const classes = useStyles();

  return (
    <div className={classes.weight}>
      <Field
        name="weightOrQuantity.magnitude"
        type="number"
        label="Weight"
        component={TextField}
        variant="outlined"
      />
      <Field
        name="weightOrQuantity.unit"
        select
        component={TextField}
        variant="outlined"
      >
        <MenuItem value={"KG"}>kg</MenuItem>
        <MenuItem value={"G"}>g</MenuItem>
        <MenuItem value={"LBS"}>lb</MenuItem>
        <MenuItem value={"UNITS"}>unit(s)</MenuItem>
      </Field>
    </div>
  );
};

export default WeightField;
