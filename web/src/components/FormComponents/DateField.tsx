import React from "react";
import { Field } from "formik";
import { TextField } from "formik-material-ui";

interface IProps {
  name: string;
  label: string;
}

const DateField: React.SFC<IProps> = props => (
  <Field
    name={props.name}
    label={props.label}
    component={TextField}
    type="date"
    InputLabelProps={{ shrink: true }}
    variant="outlined"
  />
);

export default DateField;
