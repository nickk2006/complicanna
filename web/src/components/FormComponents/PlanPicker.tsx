import React from "react";
import { Field } from "formik";
import { TextField } from "formik-material-ui";
import { plans } from "../../constants/plans";
import MenuItem from "@material-ui/core/MenuItem";

const PlanPicker: React.SFC<{}> = props => {
  return (
    <Field
      name="plan"
      select
      label="Plan"
      variant="outlined"
      component={TextField}
    >
      {plans.map(p => (
        <MenuItem key={p.planId} value={p.planId}>
          {`${p.displayName} - $${p.price}/month`}
        </MenuItem>
      ))}
    </Field>
  );
};

export default PlanPicker;
