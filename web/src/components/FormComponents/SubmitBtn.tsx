import React from "react";
import Button from "@material-ui/core/Button";

interface IProps {
  disabled: boolean;
}

const SubmitBtn: React.SFC<IProps> = props => {
  const { disabled, children } = props;

  return (
    <Button
      variant="contained"
      color="primary"
      disabled={disabled}
      type="submit"
    >
      {children}
    </Button>
  );
};

export default SubmitBtn;
