import React from "react";
import { ListItemTypography } from "../Typography";
import { TypographyProps } from "@material-ui/core/Typography";

const TextCell: React.SFC<{
  text: string;
  typography?: TypographyProps;
}> = props => {
  const { text, typography } = props;
  return <ListItemTypography {...typography}>{text}</ListItemTypography>;
};

export default TextCell;
