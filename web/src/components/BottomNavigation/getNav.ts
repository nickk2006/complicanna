import * as ROUTES from "../../constants/routes";

export const getNavAuth = (pathname: string): ROUTES.AUTH_NAV => {
  const paths = pathname.split("/");
  if (paths.includes("new")) {
    return ROUTES.AUTH_NAV.NEW;
  }
  if (paths.includes("my-todos")) {
    return ROUTES.AUTH_NAV.TODOS;
  }
  if (
    paths.includes("batches") ||
    paths.includes("license-holders") ||
    paths.includes("testing-labs") ||
    paths.includes("tests")
  ) {
    return ROUTES.AUTH_NAV.BATCHES;
  }
  if (paths.includes("my-account")) {
    return ROUTES.AUTH_NAV.ACCOUNT;
  }
  return ROUTES.AUTH_NAV.HOME;
};

export const getNavUnauth = (pathname: string): ROUTES.UNAUTH_NAV => {
  switch (pathname) {
    case ROUTES.DEMO:
      return ROUTES.UNAUTH_NAV.DEMO;
    case ROUTES.PRICING:
      return ROUTES.UNAUTH_NAV.PRICING;
    case ROUTES.SIGN_UP:
      return ROUTES.UNAUTH_NAV.SIGN_UP;
    case ROUTES.LOGIN:
      return ROUTES.UNAUTH_NAV.LOGIN;
    default:
      return ROUTES.UNAUTH_NAV.HOME;
  }
};
