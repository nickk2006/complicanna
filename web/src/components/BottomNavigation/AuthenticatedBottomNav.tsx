import React from "react";
import BottomNavigationAction from "@material-ui/core/BottomNavigationAction";
import DialogTitle from "@material-ui/core/DialogTitle";
import Dialog from "@material-ui/core/Dialog";
import ListItem from "@material-ui/core/ListItem";
import List from "@material-ui/core/List";
import ListItemText from "@material-ui/core/ListItemText";
import BottomNavigation from "@material-ui/core/BottomNavigation";

import BatchIcon from "@material-ui/icons/Dns";
import TodoIcon from "@material-ui/icons/PlaylistAddCheck";
import CreateIcon from "@material-ui/icons/AddBox";
import MyAccountIcon from "@material-ui/icons/Person";
import HomeIcon from "@material-ui/icons/Star";
import useReactRouter from "use-react-router";
import { getNavAuth } from "./getNav";
import * as ROUTES from "../../constants/routes";
import { bottomNavigationStyle } from "../Theme";

const AuthenticatedBottomNav: React.FC<{}> = () => {
  const { history, location } = useReactRouter();
  const [open, setOpen] = React.useState(false);
  const classes = bottomNavigationStyle();

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleDialogClick = (route: string) => (event: any) => {
    handleClose();
    history.push(route);
  };

  const value = getNavAuth(location.pathname);

  const handleClick = (route: string) => (event: any) => {
    history.push(route);
  };

  return (
    <>
      <BottomNavigation value={value} showLabels className={classes.root}>
        <BottomNavigationAction
          onClick={handleClick(ROUTES.HOME)}
          label="Home"
          icon={<HomeIcon />}
        />
        <BottomNavigationAction
          onClick={handleClick(ROUTES.READ_USER_TODOS)}
          label="Todos"
          icon={<TodoIcon />}
        />
        <BottomNavigationAction
          onClick={handleClick(ROUTES.READ_BATCHES)}
          label="Batches"
          icon={<BatchIcon />}
        />
        <BottomNavigationAction
          onClick={handleOpen}
          label="New"
          icon={<CreateIcon />}
        />
        <BottomNavigationAction
          onClick={handleClick(ROUTES.MY_ACCOUNT)}
          label="Account"
          icon={<MyAccountIcon />}
        />
      </BottomNavigation>
      <Dialog open={open} onClose={handleClose} aria-labelledby="new-dialog">
        <DialogTitle id="new-dialog-title">Create New Resource</DialogTitle>
        <div>
          <List>
            <ListItem onClick={handleDialogClick(ROUTES.CREATE_BATCH)} button>
              <ListItemText primary="Batch" />
            </ListItem>
            <ListItem
              onClick={handleDialogClick(ROUTES.CREATE_LICENSE_HOLDER)}
              button
            >
              <ListItemText
                primary="License Holder"
                secondary="Manufacturer / Distributor"
              />
            </ListItem>
            <ListItem onClick={handleDialogClick(ROUTES.CREATE_TEST)} button>
              <ListItemText primary="Test" />
            </ListItem>
            <ListItem
              onClick={handleDialogClick(ROUTES.CREATE_TESTING_LAB)}
              button
            >
              <ListItemText primary="Testing Lab" />
            </ListItem>
          </List>
        </div>
      </Dialog>
    </>
  );
};

export default AuthenticatedBottomNav;
