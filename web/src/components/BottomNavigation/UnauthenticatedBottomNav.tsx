import React from "react";
import BottomNavigationAction from "@material-ui/core/BottomNavigationAction";

import DemoIcon from "@material-ui/icons/TouchApp";
import PricingIcon from "@material-ui/icons/CreditCard";
import MyAccountIcon from "@material-ui/icons/Person";
import HomeIcon from "@material-ui/icons/Star";
import LoginIcon from "@material-ui/icons/LabelImportant";
import useReactRouter from "use-react-router";
import { getNavUnauth } from "./getNav";
import * as ROUTES from "../../constants/routes";
import { bottomNavigationStyle } from "../Theme";
import BottomNavigation from "@material-ui/core/BottomNavigation";

const UnauthenticatedBottomNav: React.FC<{}> = props => {
  const { history, location } = useReactRouter();
  const classes = bottomNavigationStyle();

  const value = getNavUnauth(location.pathname);

  const handleClick = (route: string) => (event: any) => {
    history.push(route);
  };

  return (
    <BottomNavigation value={value} showLabels className={classes.root}>
      <BottomNavigationAction
        onClick={handleClick(ROUTES.PRICING)}
        label="Pricing"
        icon={<PricingIcon />}
      />
      <BottomNavigationAction
        onClick={handleClick(ROUTES.DEMO)}
        label="Demo"
        icon={<DemoIcon />}
      />
      <BottomNavigationAction
        onClick={handleClick(ROUTES.HOME)}
        label="Home"
        icon={<HomeIcon />}
      />
      <BottomNavigationAction
        onClick={handleClick(ROUTES.SIGN_UP)}
        label="Register"
        icon={<MyAccountIcon />}
      />
      <BottomNavigationAction
        onClick={handleClick(ROUTES.LOGIN)}
        label="Login"
        icon={<LoginIcon />}
      />
    </BottomNavigation>
  );
};

export default UnauthenticatedBottomNav;
