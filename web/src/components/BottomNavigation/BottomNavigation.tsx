import React from "react";
import firebase from "../Firebase";
import { useAuthState } from "react-firebase-hooks/auth";
import AuthenticatedBottomNav from "./AuthenticatedBottomNav";
import UnauthenticatedBottomNav from "./UnauthenticatedBottomNav";

const BottomNavigation: React.FC<{}> = () => {
  const { user } = useAuthState(firebase.auth());

  const isLoggedIn = !!user;

  return (
    <>
      {isLoggedIn ? <AuthenticatedBottomNav /> : <UnauthenticatedBottomNav />}
    </>
  );
};

export default BottomNavigation;
