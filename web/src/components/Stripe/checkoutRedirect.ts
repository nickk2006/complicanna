import stripe from "./stripe";
const checkoutRedirect = (plan: string, customerEmail: string) =>
  stripe
    // @ts-ignore
    .redirectToCheckout({
      items: [
        // Replace with the ID of your SKU
        { plan, quantity: 1 }
      ],
      successUrl: "https://www.complicanna.com/success",
      cancelUrl: "https://www.complicanna.com/canceled",
      customerEmail
    })
    .then(function(result: any) {
      if (result.error) {
        // If `redirectToCheckout` fails due to a browser or network
        // error, display the localized error message to your customer.
        // var displayError = document.getElementById('error-message');
        // displayError.textContent = result.error.message;
        console.log(result.error);
      }
    }) as Promise<void>;

export default checkoutRedirect;
