import keys from "../../config";
// @ts-ignore
const Stripe = window.Stripe;

const stripe = Stripe(keys.REACT_APP_STRIPE_API_KEY);

export default stripe;
