import React from "react";
import { Field } from "formik";
import { TextField } from "formik-material-ui";
import MenuItem from "@material-ui/core/MenuItem";
import { usePickerStyle } from "../../Theme";

interface IProps {
  name?: string;
  label?: string;
}

const resultOptions = [
  { name: "Passed", value: "PASSED" },
  { name: "Failed", value: "FAILED" },
  { name: "Pending", value: "PENDING" }
];

const BatchResultPicker: React.FC<IProps> = ({
  name = "result",
  label = "Result"
}) => {
  const pickerStyle = usePickerStyle();

  return (
    <div className={pickerStyle.root}>
      <Field
        name={name}
        label={label}
        variant="outlined"
        select
        component={TextField}
      >
        {resultOptions.map((option, index) => (
          <MenuItem key={index} value={option.value}>
            {option.name}
          </MenuItem>
        ))}
      </Field>
    </div>
  );
};

export default BatchResultPicker;
