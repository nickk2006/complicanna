import React from "react";
import { gql } from "apollo-boost";
import { Query } from "react-apollo";
import { tests, tests_tests } from "./__generated__/tests";

import * as ROUTES from "../../../constants/routes";
import Link from "@material-ui/core/Link";
import { Link as RouterLink } from "react-router-dom";
import { ITheme } from "../../Theme";

import Loading from "../../Loading";
import { makeStyles } from "@material-ui/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import capitalize from "lodash/capitalize";

export const READ_TESTS_QUERY = gql`
  query tests {
    tests {
      id
      lab {
        id
        name
      }
      batch {
        id
        from {
          id
          name
        }
      }
      sampleDate
      resultDate
      certificateOfAnalysis {
        id
      }
      chainOfCustody {
        id
      }
      result
    }
  }
`;

const useStyles = makeStyles((theme: ITheme) => ({
  root: {
    width: "100%",
    marginTop: theme.spacing.unit * 3,
    overflowX: "auto"
  },
  table: {
    minWidth: 600
  },
  dateCol: {
    minWidth: "13em"
  },
  link: {
    color: theme.palette.text.primary
  }
}));

interface IPropsTestCell {
  key: string;
  test: tests_tests;
}

const TestCell: React.SFC<IPropsTestCell> = ({ test, key }) => {
  const classes = useStyles();

  const testDetailsRoute = ROUTES.READ_TEST_FN(test.id);
  const TestDetailsLink = (props: any) => (
    <RouterLink to={testDetailsRoute} {...props} />
  );

  const fromRoute = ROUTES.READ_LICENSE_HOLDER_FN(test.batch.from.id);
  const FromLink = (props: any) => <RouterLink to={fromRoute} {...props} />;

  if (test.lab === null) {
    return <></>;
  }
  const labRoute = ROUTES.READ_TESTING_LAB_FN(test.lab.id);
  const LabLink = (props: any) => <RouterLink to={labRoute} {...props} />;

  // const COARoute = ROUTES.READ

  return (
    <TableRow key={key}>
      <TableCell align="center">
        <Link component={TestDetailsLink} className={classes.link}>
          {test.sampleDate}
        </Link>
      </TableCell>
      <TableCell align="center">
        <Link component={FromLink} className={classes.link}>
          {test.batch.from.name}
        </Link>
      </TableCell>
      <TableCell>
        <Link component={LabLink} className={classes.link}>
          {test.lab.name}
        </Link>
      </TableCell>
      <TableCell>{capitalize(test.result)}</TableCell>
    </TableRow>
  );
};

const ReadTestsBase: React.SFC<tests> = ({ tests }) => {
  const classes = useStyles();

  return (
    <Paper className={classes.root}>
      <Table className={classes.table}>
        <TableHead>
          <TableRow>
            <TableCell align="center">Sample Date</TableCell>
            <TableCell align="center">From</TableCell>
            <TableCell align="center">Lab</TableCell>
            <TableCell align="center">Result</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {tests.map(test => (
            <TestCell test={test} key={test.id} />
          ))}
        </TableBody>
        <TableBody />
      </Table>
    </Paper>
  );
};

class ReadTestsQuery extends Query<tests> {}

const ReadTests: React.SFC<{}> = () => (
  <ReadTestsQuery query={READ_TESTS_QUERY}>
    {({ data, loading, error }) => {
      if (loading) return <Loading />;
      if (typeof data !== "undefined")
        return <ReadTestsBase tests={data.tests} />;
      if (error) {
        console.log("ERROR", error);
        return <div>ERROR: {error.message}</div>;
      }
    }}
  </ReadTestsQuery>
);

export default ReadTests;
