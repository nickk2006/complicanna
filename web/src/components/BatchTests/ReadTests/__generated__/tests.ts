/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

import { TestResult } from "./../../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: tests
// ====================================================

export interface tests_tests_lab {
  __typename: "TestingLab";
  id: string;
  name: string;
}

export interface tests_tests_batch_from {
  __typename: "LicenseHolder";
  id: string;
  name: string;
}

export interface tests_tests_batch {
  __typename: "Batch";
  id: string;
  from: tests_tests_batch_from;
}

export interface tests_tests_certificateOfAnalysis {
  __typename: "FileLocation";
  id: string;
}

export interface tests_tests_chainOfCustody {
  __typename: "FileLocation";
  id: string;
}

export interface tests_tests {
  __typename: "Test";
  id: string;
  lab: tests_tests_lab;
  batch: tests_tests_batch;
  sampleDate: any | null;
  resultDate: any | null;
  certificateOfAnalysis: tests_tests_certificateOfAnalysis | null;
  chainOfCustody: tests_tests_chainOfCustody | null;
  result: TestResult;
}

export interface tests {
  tests: tests_tests[];
}
