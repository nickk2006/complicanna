import React from "react";
import { Formik, FormikActions } from "formik";
import { createTestVariables, createTest } from "./__generated__/createTest";
import { MutationFn } from "react-apollo";
import * as ROUTES from "../../../constants/routes";
import useReactRouter from "use-react-router";
import { CreateTestInput } from "../../../types/graphql-global-types";
import CreateTestForm from "./CreateTestForm";
import schema from "./createTestFormValidation";

export const INITIAL_VALUES: CreateTestInput = {
  labId: "",
  batchId: "",
  sampleDate: ""
};

const FormikForm: React.SFC<{
  createTest: MutationFn<createTest, createTestVariables>;
}> = props => {
  const { history } = useReactRouter();

  const handleSubmit = async (
    data: CreateTestInput,
    formikBag: FormikActions<CreateTestInput>
  ) => {
    const res = await props.createTest({ variables: { data } });
    if (typeof res !== "undefined" && typeof res.data !== "undefined") {
      formikBag.resetForm();
      history.push(ROUTES.READ_BATCH_FN(data.batchId));
    }
  };

  return (
    <Formik
      initialValues={INITIAL_VALUES}
      onSubmit={handleSubmit}
      validationSchema={schema}
    >
      {formikProps => <CreateTestForm {...formikProps} />}
    </Formik>
  );
};

export default FormikForm;
