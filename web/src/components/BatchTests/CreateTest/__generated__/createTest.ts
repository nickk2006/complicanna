/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

import { CreateTestInput } from "./../../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: createTest
// ====================================================

export interface createTest_createTest {
  __typename: "Test";
  id: string;
}

export interface createTest {
  createTest: createTest_createTest;
}

export interface createTestVariables {
  data: CreateTestInput;
}
