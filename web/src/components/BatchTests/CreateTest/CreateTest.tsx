import React from "react";
import { gql } from "apollo-boost";
import { Mutation } from "react-apollo";
import { createTest, createTestVariables } from "./__generated__/createTest";
import CreateTestFormik from "./CreateTestFormik";

export const CREATE_TEST = gql`
  mutation createTest($data: CreateTestInput!) {
    createTest(data: $data) {
      id
    }
  }
`;

class CreateTestMutation extends Mutation<createTest, createTestVariables> {}

const ConnectedForm: React.SFC<{}> = () => (
  <CreateTestMutation mutation={CREATE_TEST}>
    {handleSubmit => <CreateTestFormik createTest={handleSubmit} />}
  </CreateTestMutation>
);

export default ConnectedForm;
