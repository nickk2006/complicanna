import React from "react";
import { FormikProps } from "formik";
import { CreateTestInput } from "./../../../types/graphql-global-types";
import { ReadBatchPicker } from "../../Batch";
import { TestingLabPicker } from "../../TestingLab";
import { Title, DateField, SubmitBtn, Form } from "../../FormComponents";

interface IProps extends FormikProps<CreateTestInput> {}

const TestForm: React.SFC<IProps> = props => {
  return (
    <Form>
      <Title>Test Form</Title>
      <ReadBatchPicker name="batchId" label="Batch" />
      <TestingLabPicker name="labId" label="Testing Lab" />
      <DateField name="sampleDate" label="Sampling Date" />
      <SubmitBtn disabled={props.isSubmitting}>Create Test</SubmitBtn>
    </Form>
  );
};

export default TestForm;
