import { requiredString } from "../../FormComponents";
import * as yup from "yup";
import { CreateTestInput } from "../../../types/graphql-global-types";

const schema: yup.ObjectSchema<CreateTestInput> = yup.object({
  batchId: requiredString,
  labId: requiredString,
  sampleDate: requiredString
});

export default schema;
