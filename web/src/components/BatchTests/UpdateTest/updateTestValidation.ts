import * as yup from "yup";
import { IUpdateTestFormikInputs } from "./types";
import * as GraphqlTypes from "./../../../types/graphql-global-types";

const updateTestSchema: yup.ObjectSchema<IUpdateTestFormikInputs> = yup.object({
  batchId: yup.string(),
  labId: yup.string(),
  sampleDate: yup.string(),
  resultDate: yup.string(),
  certificateOfAnalysis: yup.mixed(),
  chainOfCustody: yup.mixed(),
  video: yup.mixed(),
  result: yup
    .mixed()
    .oneOf([
      GraphqlTypes.TestResult.FAILED,
      GraphqlTypes.TestResult.PASSED,
      GraphqlTypes.TestResult.PENDING
    ])
});

export default updateTestSchema;
