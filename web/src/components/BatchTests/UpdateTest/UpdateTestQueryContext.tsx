import React from "react";
import { updateTestQuery_test } from "./__generated__/updateTestQuery";

const UpdateTestContext = React.createContext<updateTestQuery_test | null>(
  null
);

export default UpdateTestContext;
