/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

import { TestWhereUniqueInput, TestResult } from "./../../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: updateTestQuery
// ====================================================

export interface updateTestQuery_test_lab {
  __typename: "TestingLab";
  id: string;
  name: string;
}

export interface updateTestQuery_test_batch {
  __typename: "Batch";
  id: string;
}

export interface updateTestQuery_test_certificateOfAnalysis {
  __typename: "FileLocation";
  id: string;
  signedUrl: string;
  title: string;
}

export interface updateTestQuery_test_chainOfCustody {
  __typename: "FileLocation";
  id: string;
  signedUrl: string;
  title: string;
}

export interface updateTestQuery_test_video {
  __typename: "FileLocation";
  id: string;
  signedUrl: string;
  title: string;
}

export interface updateTestQuery_test {
  __typename: "Test";
  id: string;
  lab: updateTestQuery_test_lab;
  batch: updateTestQuery_test_batch;
  sampleDate: any | null;
  resultDate: any | null;
  result: TestResult;
  certificateOfAnalysis: updateTestQuery_test_certificateOfAnalysis | null;
  chainOfCustody: updateTestQuery_test_chainOfCustody | null;
  video: updateTestQuery_test_video | null;
}

export interface updateTestQuery {
  test: updateTestQuery_test | null;
}

export interface updateTestQueryVariables {
  where: TestWhereUniqueInput;
}
