/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

import { UpdateTestInput } from "./../../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: updateTestMutation
// ====================================================

export interface updateTestMutation_updateTest_batch {
  __typename: "Batch";
  id: string;
}

export interface updateTestMutation_updateTest {
  __typename: "Test";
  batch: updateTestMutation_updateTest_batch;
}

export interface updateTestMutation {
  updateTest: updateTestMutation_updateTest;
}

export interface updateTestMutationVariables {
  data: UpdateTestInput;
  testId: string;
}
