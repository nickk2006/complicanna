import { mapFormikValuesToApi } from "../helpers";
import { IUpdateTestFormikInputs } from "../types";
import { TestResult } from "../../../../types/graphql-global-types";

describe("Helper functions for BatchTest Module", () => {
  it("Converts object from IUpdateTestFormikInputs to IUpdateTestApiInputs", () => {
    const chainOfCustody = new File([], "coc.pdf");

    const formikValues: IUpdateTestFormikInputs = {
      labId: "",
      batchId: "",
      sampleDate: "2019-10-11",
      resultDate: "2019-10-12",
      certificateOfAnalysis: undefined,
      chainOfCustody,
      video: undefined,
      result: TestResult.PENDING
    };

    const apiValues = mapFormikValuesToApi(formikValues);

    expect(apiValues.labId).toBeUndefined();
    expect(apiValues.batchId).toBeUndefined();
    expect(apiValues.sampleDate).toBe("2019-10-11");
    expect(apiValues.resultDate).toBe("2019-10-12");
    expect(apiValues.certificateOfAnalysis).toBeUndefined();
    expect(apiValues.chainOfCustody).toBe(chainOfCustody);
    expect(apiValues.video).toBeUndefined();
    expect(apiValues.result).toBe(TestResult.PENDING);
  });
});
