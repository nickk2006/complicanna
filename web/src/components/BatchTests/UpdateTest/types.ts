import {
  updateTestQuery,
  updateTestQueryVariables,
  updateTestQuery_test
} from "./__generated__/updateTestQuery";
import {
  updateTestMutation_updateTest,
  updateTestMutationVariables
} from "./__generated__/updateTestMutation";
import { Query } from "react-apollo";
import { Mutation, MutationFn } from "react-apollo";
import * as GraphqlTypes from "./../../../types/graphql-global-types";

export class UpdateTestsQuery extends Query<
  updateTestQuery,
  updateTestQueryVariables
> {}

export class UpdateTestMutation extends Mutation<
  updateTestMutation_updateTest,
  updateTestMutationVariables
> {}

export interface IUpdateTestFormProps {
  test: updateTestQuery_test | null;
  handleSubmit: MutationFn<
    updateTestMutation_updateTest,
    updateTestMutationVariables
  >;
}

export interface IUpdateTestFormikInputs {
  labId: string;
  batchId: string;
  sampleDate: string;
  resultDate: string;
  certificateOfAnalysis?: File;
  chainOfCustody?: File;
  video?: File;
  result: GraphqlTypes.TestResult;
}

export interface IUpdateTestApiInputs {
  labId?: string;
  batchId?: string;
  sampleDate?: string;
  resultDate?: string;
  certificateOfAnalysis?: File;
  chainOfCustody?: File;
  video?: File;
  result?: GraphqlTypes.TestResult;
}
