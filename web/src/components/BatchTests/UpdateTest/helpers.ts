import { IUpdateTestFormikInputs, IUpdateTestApiInputs } from "./types";
import { objectStringToUndefined } from "../../../helpers/format";

export const mapFormikValuesToApi = (formikValues: IUpdateTestFormikInputs) => {
  const originalValues = formikValues as any;

  const newValues = objectStringToUndefined(
    originalValues
  ) as IUpdateTestApiInputs;

  return newValues;
};
