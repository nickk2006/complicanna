import { gql } from "apollo-boost";

export const UPDATE_TEST_MUTATION = gql`
  mutation updateTestMutation($data: UpdateTestInput!, $testId: String!) {
    updateTest(data: $data, testId: $testId) {
      batch {
        id
      }
    }
  }
`;
