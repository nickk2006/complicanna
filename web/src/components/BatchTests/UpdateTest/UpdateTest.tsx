import React from "react";

import useReactRouter from "use-react-router";

import { UPDATE_TEST_QUERY } from "./UpdateTestQuery";
import { UPDATE_TEST_MUTATION } from "./UpdateTestMutation";
import TestQueryContext from "./UpdateTestQueryContext";
import UpdateTestForm from "./UpdateTestForm";
import * as ROUTES from "../../../constants/routes";
import { UpdateTestMutation, UpdateTestsQuery } from "./types";
import Loading from "../../Loading";

const UpdateTest: React.SFC<{}> = () => {
  const { match } = useReactRouter<ROUTES.IUPDATE_TEST>();
  const { testId } = match.params;
  return (
    <UpdateTestsQuery
      query={UPDATE_TEST_QUERY}
      variables={{ where: { id: testId } }}
    >
      {({ data, loading, error }) => {
        if (loading) return <Loading />;
        if (typeof data !== "undefined") {
          return (
            <TestQueryContext.Provider value={data.test}>
              <UpdateTestMutation mutation={UPDATE_TEST_MUTATION}>
                {handleSubmit => (
                  <UpdateTestForm
                    test={data.test}
                    handleSubmit={handleSubmit}
                  />
                )}
              </UpdateTestMutation>
            </TestQueryContext.Provider>
          );
        }
        if (error) {
          console.log("ERROR", error);
          return <div>ERROR: {error.message}</div>;
        }
      }}
    </UpdateTestsQuery>
  );
};

export default UpdateTest;
