import { gql } from "apollo-boost";

export const UPDATE_TEST_QUERY = gql`
  query updateTestQuery($where: TestWhereUniqueInput!) {
    test(where: $where) {
      id
      lab {
        id
        name
      }
      batch {
        id
      }
      sampleDate
      resultDate
      result
      certificateOfAnalysis {
        id
        signedUrl
        title
      }
      chainOfCustody {
        id
        signedUrl
        title
      }
      video {
        id
        signedUrl
        title
      }
    }
  }
`;
