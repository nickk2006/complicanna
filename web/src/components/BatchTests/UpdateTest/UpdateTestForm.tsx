import React from "react";
import { Field, Formik, FormikProps } from "formik";
import { TextField } from "formik-material-ui";
import { ReadBatchPicker } from "../../Batch";
import { TestingLabPicker } from "../../TestingLab";
import useReactRouter from "use-react-router";
import * as ROUTES from "../../../constants/routes";
import { dateForField } from "../../../helpers/format";
import BatchResultPicker from "../BatchResultPicker";
import { UpdateFileField, apiToFormikFileField } from "../../Fields";
import TestQueryContext from "./UpdateTestQueryContext";
import Typography from "@material-ui/core/Typography";
import { IUpdateTestFormProps, IUpdateTestFormikInputs } from "./types";
import { mapFormikValuesToApi } from "./helpers";
import schema from "./updateTestValidation";
import { Form, SubmitBtn } from "../../FormComponents";

const SampleDateField: React.SFC<{}> = () => (
  <Field
    name="sampleDate"
    label="Sampling Date"
    component={TextField}
    type="date"
    InputLabelProps={{ shrink: true }}
    variant="outlined"
  />
);

const ResultDateField: React.SFC<{}> = () => (
  <Field
    name="resultDate"
    label="Result Date"
    component={TextField}
    type="date"
    InputLabelProps={{ shrink: true }}
    variant="outlined"
  />
);

export const UpdateTestFormFields: React.SFC<
  FormikProps<IUpdateTestFormikInputs>
> = props => {
  const { isSubmitting } = props;
  const initialTest = React.useContext(TestQueryContext);

  if (initialTest === null) {
    return (
      <Typography variant="h5">
        Error getting Test data. Please retry
      </Typography>
    );
  }

  const { video, certificateOfAnalysis, chainOfCustody } = initialTest;

  const originalVideo = apiToFormikFileField(video);
  const originalCOA = apiToFormikFileField(certificateOfAnalysis);
  const originalCOC = apiToFormikFileField(chainOfCustody);

  return (
    <Form>
      <ReadBatchPicker name="batchId" label="Batch" />
      <TestingLabPicker name="labId" label="Testing Lab" />
      <SampleDateField />
      <ResultDateField />
      <BatchResultPicker />
      <UpdateFileField
        originalFile={originalVideo}
        title="Video"
        name="video"
      />
      <UpdateFileField
        originalFile={originalCOA}
        title="Certificate of Analysis"
        name="certificateOfAnalysis"
      />
      <UpdateFileField
        originalFile={originalCOC}
        title="Chain of Custody"
        name="chainOfCustody"
      />
      <SubmitBtn disabled={isSubmitting}>Update Test</SubmitBtn>
    </Form>
  );
};

const ConnectedForm: React.SFC<IUpdateTestFormProps> = ({
  test,
  handleSubmit
}) => {
  const { match } = useReactRouter<ROUTES.IUPDATE_TEST>();

  if (test === null) {
    return <div>No test found.</div>;
  }

  const sampleDate =
    test.sampleDate === null ? "" : dateForField(test.sampleDate);

  const resultDate =
    test.resultDate === null ? "" : dateForField(test.resultDate);

  const INITIAL_VALUES: IUpdateTestFormikInputs = {
    labId: test.lab.id || "",
    batchId: test.batch.id || "",
    sampleDate,
    resultDate,
    certificateOfAnalysis: undefined,
    chainOfCustody: undefined,
    video: undefined,
    result: test.result
  };

  return (
    <Formik
      initialValues={INITIAL_VALUES}
      onSubmit={async (values, formikBag) => {
        const data = mapFormikValuesToApi(values);
        await handleSubmit({
          variables: { data, testId: match.params.testId }
        });
        formikBag.resetForm();
      }}
      validationSchema={schema}
    >
      {formikProps => <UpdateTestFormFields {...formikProps} />}
    </Formik>
  );
};

export default ConnectedForm;
