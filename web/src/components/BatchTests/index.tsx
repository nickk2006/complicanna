import ReadTests from "./ReadTests";
import CreateTest from "./CreateTest";
import UpdateTest from "./UpdateTest";

export { ReadTests, CreateTest, UpdateTest };
