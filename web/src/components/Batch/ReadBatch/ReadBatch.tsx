import React from "react";
import { gql } from "apollo-boost";
import { Query } from "react-apollo";
import { batch, batchVariables } from "./__generated__/batch";
import { withRouter, RouteComponentProps } from "react-router";
import { useReadListStyle } from "../../Theme";
import Typography from "@material-ui/core/Typography";
import moment from "moment";
import { Link as RouterLink } from "react-router-dom";
import Link from "@material-ui/core/Link";
import { ListItemTypography, ListItemTypographyProps } from "../../Typography";
import * as ROUTES from "../../../constants/routes";
import TestSection from "./ReadBatchTestSection";
import Loading from "../../Loading";

class ReadBatchQuery extends Query<batch, batchVariables> {}

export const READ_BATCH_QUERY = gql`
  query batch($where: BatchWhereUniqueInput!) {
    batch(where: $where) {
      id
      dateOfEntry
      batchNumber
      description
      tests {
        id
        sampleDate
        resultDate
        result
        lab {
          id
          name
        }
        certificateOfAnalysis {
          id
          signedUrl
          title
        }
        chainOfCustody {
          id
          signedUrl
          title
        }
        video {
          id
          signedUrl
          title
        }
      }
      from {
        id
        name
      }
      for {
        id
        name
      }
    }
  }
`;

export const ReadBatchBase: React.SFC<batch> = ({ batch }) => {
  const listStyle = useReadListStyle();

  if (batch === null) {
    return <Typography variant="body1">Can't find Batch.</Typography>;
  }

  const { description } = batch;
  const dateOfEntry = moment(batch.dateOfEntry).format("MMMM D, YYYY");

  const forRoute = ROUTES.READ_LICENSE_HOLDER_FN(batch.for.id);
  const ForLink = (props: any) => <RouterLink to={forRoute} {...props} />;

  const fromRoute = ROUTES.READ_LICENSE_HOLDER_FN(batch.from.id);
  const FromLink = (props: any) => <RouterLink to={fromRoute} {...props} />;

  return (
    <div className={listStyle.root}>
      <div className={listStyle.titleRow}>
        <Typography variant="h6">Information</Typography>
      </div>
      <div className={listStyle.dataRow}>
        <ListItemTypography>Batch Number</ListItemTypography>
        <ListItemTypography>{batch.batchNumber}</ListItemTypography>
      </div>
      <div className={listStyle.dataRow}>
        <ListItemTypography>Date of Entry</ListItemTypography>
        <ListItemTypography>{dateOfEntry}</ListItemTypography>
      </div>
      <div className={listStyle.dataRow}>
        <ListItemTypography>Received From</ListItemTypography>
        <Link component={FromLink} variant={ListItemTypographyProps.variant}>
          {batch.from.name}
        </Link>
      </div>
      <div className={listStyle.dataRow}>
        <Typography variant="body1">Held For</Typography>
        <Link component={ForLink} variant={ListItemTypographyProps.variant}>
          {batch.for.name}
        </Link>
      </div>
      <div className={listStyle.dataRow}>
        <Typography variant="body1">Description</Typography>
        <ListItemTypography>
          {description !== null ? description : "N/A"}
        </ListItemTypography>
      </div>

      {batch.tests &&
        batch.tests.map((test, index) => (
          <TestSection test={test} index={index} key={test.id || index} />
        ))}
    </div>
  );
};

interface IProps extends RouteComponentProps<{ batchId: string }> {}

export const ReadBatch: React.SFC<IProps> = props => (
  <ReadBatchQuery
    query={READ_BATCH_QUERY}
    variables={{ where: { id: props.match.params.batchId } }}
  >
    {({ data, loading, error }) => {
      if (loading) return <Loading />;
      if (typeof data !== "undefined")
        return <ReadBatchBase batch={data.batch} />;
      if (error) {
        console.log("ERROR", error);
        return <div>ERROR: {error.message}</div>;
      }
    }}
  </ReadBatchQuery>
);

export default withRouter(ReadBatch);
