/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

import { BatchWhereUniqueInput, TestResult } from "./../../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: batch
// ====================================================

export interface batch_batch_tests_lab {
  __typename: "TestingLab";
  id: string;
  name: string;
}

export interface batch_batch_tests_certificateOfAnalysis {
  __typename: "FileLocation";
  id: string;
  signedUrl: string;
  title: string;
}

export interface batch_batch_tests_chainOfCustody {
  __typename: "FileLocation";
  id: string;
  signedUrl: string;
  title: string;
}

export interface batch_batch_tests_video {
  __typename: "FileLocation";
  id: string;
  signedUrl: string;
  title: string;
}

export interface batch_batch_tests {
  __typename: "Test";
  id: string;
  sampleDate: any | null;
  resultDate: any | null;
  result: TestResult;
  lab: batch_batch_tests_lab;
  certificateOfAnalysis: batch_batch_tests_certificateOfAnalysis | null;
  chainOfCustody: batch_batch_tests_chainOfCustody | null;
  video: batch_batch_tests_video | null;
}

export interface batch_batch_from {
  __typename: "LicenseHolder";
  id: string;
  name: string;
}

export interface batch_batch_for {
  __typename: "LicenseHolder";
  id: string;
  name: string;
}

export interface batch_batch {
  __typename: "Batch";
  id: string;
  dateOfEntry: any;
  batchNumber: string | null;
  description: string | null;
  tests: batch_batch_tests[] | null;
  from: batch_batch_from;
  for: batch_batch_for;
}

export interface batch {
  batch: batch_batch | null;
}

export interface batchVariables {
  where: BatchWhereUniqueInput;
}
