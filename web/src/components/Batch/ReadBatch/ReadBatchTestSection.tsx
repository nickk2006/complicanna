import React from "react";
import { batch_batch_tests } from "./__generated__/batch";
import { useReadListStyle } from "../../Theme";
import Typography from "@material-ui/core/Typography";
import moment from "moment";
import { Link as RouterLink } from "react-router-dom";
import Link from "@material-ui/core/Link";
import { ListItemTypography, ListItemTypographyProps } from "../../Typography";
import * as ROUTES from "../../../constants/routes";
import capitalize from "lodash/capitalize";

interface IProps {
  test: batch_batch_tests | null;
  index: number;
}

const TestSection: React.SFC<IProps> = ({ test, index }) => {
  const listStyle = useReadListStyle();

  if (test === null) {
    return <></>;
  }
  const { video, certificateOfAnalysis, chainOfCustody } = test;
  const sampleDate = moment(test.sampleDate).format("MMMM DD, YYYY");

  const resultDate = test.resultDate
    ? moment(test.resultDate).format("MMMM DD, YYYY")
    : "TBD";

  const result = capitalize(test.result);

  let VideoLink: React.SFC<any>;
  let COCLink: React.SFC<any>;
  let COALink: React.SFC<any>;

  const labRoute = ROUTES.READ_TESTING_LAB_FN(test.lab.id);
  const LabLink: React.SFC<any> = props => (
    <RouterLink to={labRoute} {...props} />
  );

  if (video !== null) {
    VideoLink = () => (
      <Link
        href={video.signedUrl}
        variant={ListItemTypographyProps.variant}
        target="_blank"
      >
        Download
      </Link>
    );
  } else {
    VideoLink = () => (
      <Typography variant={ListItemTypographyProps.variant}>
        No video uploaded
      </Typography>
    );
  }
  if (chainOfCustody !== null) {
    COCLink = () => (
      <Link
        href={chainOfCustody.signedUrl}
        variant={ListItemTypographyProps.variant}
        target="_blank"
      >
        Download
      </Link>
    );
  } else {
    COCLink = () => (
      <Typography variant={ListItemTypographyProps.variant}>
        No document uploaded
      </Typography>
    );
  }
  if (certificateOfAnalysis !== null) {
    COALink = () => (
      <Link
        href={certificateOfAnalysis.signedUrl}
        variant={ListItemTypographyProps.variant}
        target="_blank"
      >
        Download
      </Link>
    );
  } else {
    COALink = () => (
      <Typography variant={ListItemTypographyProps.variant}>
        No document uploaded
      </Typography>
    );
  }

  const updateTestRoute = ROUTES.UPDATE_TEST_FN(test.id);
  const UpdateTestLink: React.SFC<any> = props => (
    <RouterLink to={updateTestRoute} {...props} />
  );

  return (
    <div key={test.id} className={listStyle.childRoot}>
      <div className={listStyle.titleRow}>
        <Typography variant="h6">{`Test ${index + 1}`}</Typography>
        <Link
          component={UpdateTestLink}
          variant={ListItemTypographyProps.variant}
          className={listStyle.updateText}
        >
          Update
        </Link>
      </div>
      <div className={listStyle.dataRow}>
        <ListItemTypography>Lab</ListItemTypography>
        <Link variant={ListItemTypographyProps.variant} component={LabLink}>
          {test.lab.name}
        </Link>
      </div>
      <div className={listStyle.dataRow}>
        <ListItemTypography>Sample Date</ListItemTypography>
        <ListItemTypography>{sampleDate}</ListItemTypography>
      </div>
      <div className={listStyle.dataRow}>
        <ListItemTypography>Result Date</ListItemTypography>
        <ListItemTypography>{resultDate}</ListItemTypography>
      </div>
      <div className={listStyle.dataRow}>
        <ListItemTypography>Result</ListItemTypography>
        <ListItemTypography>{result}</ListItemTypography>
      </div>
      <div className={listStyle.dataRow}>
        <ListItemTypography>Certificate of Analysis</ListItemTypography>
        <COALink />
      </div>
      <div className={listStyle.dataRow}>
        <ListItemTypography>Chain of Custody</ListItemTypography>
        <COCLink />
      </div>
      <div className={listStyle.dataRow}>
        <ListItemTypography>Video</ListItemTypography>
        <VideoLink />
      </div>
    </div>
  );
};

export default TestSection;
