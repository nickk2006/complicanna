import React from "react";
import { makeStyles, createStyles } from "@material-ui/styles";
import { ITheme } from "../../Theme";
import Paper from "@material-ui/core/Paper";
import { ReadBatchesQuery_batches } from "./__generated__/ReadBatchesQuery";
import { ITableProps } from "./types";
import Checkbox from "@material-ui/core/Checkbox";
import { dateForDisplay } from "../../../helpers/format";
import { FormControlLabel } from "@material-ui/core";
import {
  handleClear,
  handleClickMobile,
  handleSubmit,
  useOpenStatus
} from "./BatchHelpers";
import TodosSnackbar from "./TodosSnackbar";
import { LinkType } from "../../ValueCell";
import DataRow from "../../DataRow";
import * as ROUTES from "../../../constants/routes";

const useStyles = makeStyles((theme: ITheme) => {
  return createStyles({
    root: {
      display: "grid",
      gridGap: 20,
      [theme.breakpoints.up("xs")]: {
        gridTemplateColumns: "1fr 1fr"
      },
      [theme.breakpoints.down("xs")]: {
        gridTemplateColumns: "1fr"
      }
    }
  });
});

export const BatchesMobile: React.SFC<{
  handleClick: (
    id: string
  ) => (
    event: {
      target: HTMLInputElement;
    },
    checked: boolean
  ) => void;
  batches: ReadBatchesQuery_batches[];
  selected: string[];
  todoLabel: string;
}> = props => {
  const { handleClick, batches, selected, todoLabel } = props;
  const classes = useStyles();

  return (
    <div className={classes.root}>
      {batches.map(b => (
        <SingleBatchCard
          handleClick={handleClick}
          key={b.id}
          batch={b}
          selected={selected}
          todoLabel={todoLabel}
        />
      ))}
    </div>
  );
};

const BatchesTableMobile: React.FC<ITableProps> = props => {
  const { batches, handleAddTodos, isSubmitting } = props;
  const [selected, setSelected] = React.useState<string[]>([]);
  const open = useOpenStatus(selected);

  const handleClick = handleClickMobile(setSelected);

  return (
    <div>
      <BatchesMobile
        batches={batches}
        handleClick={handleClick}
        selected={selected}
        todoLabel="Add Todo"
      />
      <TodosSnackbar
        open={open}
        handleSubmit={handleSubmit(handleAddTodos, selected)}
        handleClear={handleClear(setSelected)}
        disabled={isSubmitting}
        submitLabel="Add Todos"
      />
    </div>
  );
};

const useStylesSingleBatch = makeStyles((theme: ITheme) => {
  return createStyles({
    root: {
      ...theme.mixins.gutters(),
      paddingTop: theme.spacing.unit * 2,
      paddingBottom: theme.spacing.unit * 2,
      display: "grid",
      gridTemplateColumns: "1fr"
    },
    todo: {
      marginLeft: 10
    }
  });
});

const SingleBatchCard: React.FC<{
  batch: ReadBatchesQuery_batches;
  handleClick: (
    id: string
  ) => (
    event: {
      target: HTMLInputElement;
    },
    checked: boolean
  ) => void;
  selected: string[];
  todoLabel: string;
}> = props => {
  const { batch, handleClick, selected, todoLabel } = props;
  const { assignedUser } = batch;

  const classes = useStylesSingleBatch();

  const assigned =
    assignedUser !== null
      ? `${assignedUser.firstName} ${assignedUser.lastName}`
      : "";
  return (
    <Paper className={classes.root}>
      <div className={classes.todo}>
        <FormControlLabel
          control={
            <Checkbox
              value="todo"
              onChange={handleClick(batch.id)}
              checked={selected.includes(batch.id)}
            />
          }
          label={todoLabel}
        />
      </div>
      <DataRow
        label="Batch Number"
        value={batch.batchNumber || ""}
        link={{ path: ROUTES.READ_BATCH_FN(batch.id), type: LinkType.ROUTE }}
      />
      <DataRow
        label="For"
        value={batch.for.name}
        link={{
          path: ROUTES.READ_LICENSE_HOLDER_FN(batch.for.id),
          type: LinkType.ROUTE
        }}
      />
      <DataRow
        label="From"
        value={batch.from.name}
        link={{
          path: ROUTES.READ_LICENSE_HOLDER_FN(batch.from.id),
          type: LinkType.ROUTE
        }}
        noWrap
      />
      <DataRow
        label="Date of Entry"
        value={dateForDisplay(batch.dateOfEntry)}
        noWrap
      />
      <DataRow label="Assigned" value={assigned} noWrap />
      <DataRow label="Description" value={batch.description || ""} noWrap />
    </Paper>
  );
};

export default BatchesTableMobile;
