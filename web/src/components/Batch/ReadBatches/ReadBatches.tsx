import React from "react";
import { Query, MutationFn } from "react-apollo";
import {
  ReadBatchesQuery,
  ReadBatchesQuery_batches
} from "./__generated__/ReadBatchesQuery";
import ReadBatchesTable from "./ReadBatches4";
import ReadBatchesMobile from "./ReadBatchesTableMobile";
import Hidden from "@material-ui/core/Hidden";

import AddTodosMutation, { ADD_TODOS_MUTATION } from "./ReadBatchesMutations";
import { READ_BATCHES_QUERY } from "./ReadBatchesQueries";
import Loading from "../../Loading";
import { AddTodos, AddTodosVariables } from "./__generated__/AddTodos";

const Table: React.SFC<{
  batches: ReadBatchesQuery_batches[];
  handleAddTodos: MutationFn<AddTodos, AddTodosVariables>;
  isSubmitting: boolean;
}> = props => {
  const { batches, handleAddTodos, isSubmitting } = props;
  return (
    <>
      <Hidden smDown>
        <ReadBatchesTable
          isSubmitting={isSubmitting}
          batches={batches}
          handleAddTodos={handleAddTodos}
        />
      </Hidden>
      <Hidden mdUp>
        <ReadBatchesMobile
          isSubmitting={isSubmitting}
          batches={batches}
          handleAddTodos={handleAddTodos}
        />
      </Hidden>
    </>
  );
};

class BatchesQuery extends Query<ReadBatchesQuery> {}

const ReadBatches: React.SFC<{}> = () => (
  <BatchesQuery query={READ_BATCHES_QUERY}>
    {({ data, loading, error, refetch }) => (
      <AddTodosMutation mutation={ADD_TODOS_MUTATION} onCompleted={refetch}>
        {(handleAddTodos, { loading: isSubmitting }) => {
          if (loading) return <Loading />;
          if (typeof data !== "undefined")
            return (
              <Table
                isSubmitting={isSubmitting}
                batches={data.batches}
                handleAddTodos={handleAddTodos}
              />
            );

          if (error) {
            console.log("ERROR", error);
            return <div>ERROR: {error.message}</div>;
          }
        }}
      </AddTodosMutation>
    )}
  </BatchesQuery>
);

// export default ReadBatches;
export default ReadBatches;
