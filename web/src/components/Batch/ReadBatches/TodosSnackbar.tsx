import React from "react";
import Snackbar from "@material-ui/core/Snackbar";
import Button from "@material-ui/core/Button";
import ClearIcon from "@material-ui/icons/Clear";
import SubmitIcon from "@material-ui/icons/CloudUpload";
import { makeStyles, createStyles } from "@material-ui/styles";
import { ITheme } from "../../Theme";

const useStyles = makeStyles((theme: ITheme) =>
  createStyles({
    button: {
      margin: theme.spacing.unit
    },
    rightIcon: {
      marginLeft: theme.spacing.unit
    }
  })
);

const TodosSnackbar: React.FC<{
  open: boolean;
  handleSubmit: (
    event: React.MouseEvent<HTMLElement, MouseEvent>
  ) => Promise<any>;
  handleClear: (event: React.MouseEvent<HTMLElement, MouseEvent>) => void;
  submitLabel: string;
  disabled: boolean;
}> = props => {
  const { open, handleSubmit, handleClear, submitLabel, disabled } = props;

  const c = useStyles();

  return (
    <Snackbar
      anchorOrigin={{
        vertical: "bottom",
        horizontal: "left"
      }}
      open={open}
      autoHideDuration={6000}
      ContentProps={{
        "aria-describedby": "message-id"
      }}
      // message={<span id="message-id">Add to My Todos</span>}
      action={[
        <Button
          key="clear-selection"
          color="default"
          variant="contained"
          size="small"
          className={c.button}
          onClick={handleSubmit}
          disabled={disabled}
        >
          {submitLabel}
          <SubmitIcon className={c.rightIcon} />
        </Button>,
        <Button
          key="add-todos"
          color="inherit"
          variant="outlined"
          size="small"
          className={c.button}
          onClick={handleClear}
          disabled={disabled}
        >
          Clear
          <ClearIcon className={c.rightIcon} />
        </Button>
      ]}
    />
  );
};

export default TodosSnackbar;
