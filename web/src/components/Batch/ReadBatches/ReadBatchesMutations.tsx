import { gql } from "apollo-boost";
import { Mutation } from "react-apollo";
import { AddTodosVariables, AddTodos } from "./__generated__/AddTodos";

export const ADD_TODOS_MUTATION = gql`
  mutation AddTodos($addIds: [String!]!) {
    updateTodos(addIds: $addIds, removeIds: [])
  }
`;

class AddTodosMutation extends Mutation<AddTodos, AddTodosVariables> {}

export default AddTodosMutation;
