import React from "react";
import classNames from "classnames";
import { makeStyles, createStyles } from "@material-ui/styles";
import { ListItemTypography } from "../../Typography";
import { ITheme } from "../../Theme";
import { dateForDisplay } from "../../../helpers/format";
import blue from "@material-ui/core/colors/blue";
import lime from "@material-ui/core/colors/lime";
import { ITableProps } from "./types";
import TodosSnackbar from "./TodosSnackbar";
import {
  handleClick,
  handleClear,
  handleSubmit,
  formatAssignedUserName,
  formatDescription,
  useOpenStatus
} from "./BatchHelpers";
import { ReadBatchesQuery_batches } from "./__generated__/ReadBatchesQuery";
import ValueCell, { LinkType } from "../../ValueCell";
import * as ROUTES from "../../../constants/routes";

const useStyles = makeStyles((theme: ITheme) => {
  const BORDER_STYLE = "1px solid gray";
  const HEADER_COLOR = lime[200];
  const HOVER_COLOR = blue[50];

  return createStyles({
    ol: {
      paddingLeft: theme.spacing.unit,
      paddingRight: theme.spacing.unit,
      "&>li": {
        listStyle: "none"
      }
    },
    collectionContainer: {
      "&>$itemContainer:first-child $attribute": {
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        textOverflow: "initial",
        overflow: "auto",
        whiteSpace: "normal"
      },
      "&>$itemContainer:first-child": {
        backgroundColor: HEADER_COLOR
      }
    },
    collection: {
      borderTop: BORDER_STYLE,
      margin: 0,
      padding: 0,
      width: "100%"
    },
    itemContainer: {
      display: "grid",
      gridTemplateColumns: "45px 10em 2fr 1fr 3fr",
      "&:hover": {
        backgroundColor: HOVER_COLOR
      },
      "&>div:first-child": {
        borderLeft: BORDER_STYLE
      }
    },
    attributeContainer: {
      display: "grid",
      gridTemplateColumns:
        "repeat(auto-fit, minmax(var(--column-width-min), 1fr))"
    },
    attribute: {
      borderRight: BORDER_STYLE,
      borderBottom: BORDER_STYLE,
      padding: 2,
      overflow: "hidden",
      whiteSpace: "nowrap",
      textOverflow: "ellipsis"
    },
    fromFor: {
      "--column-width-min": "100px"
    },
    dateAssignedUser: {
      "--column-width-min": "100px"
    }
  });
});

export const ReadBatchesTable: React.FC<{
  batches: ReadBatchesQuery_batches[];
  selected: string[];
  setSelected: React.Dispatch<React.SetStateAction<string[]>>;
}> = props => {
  const { batches, selected, setSelected } = props;
  const c = useStyles();

  return (
    <ol className={classNames(c.ol, c.collection, c.collectionContainer)}>
      {/* Header */}
      <li className={c.itemContainer}>
        <div className={c.attribute} />
        <ListItemTypography className={c.attribute}>
          Batch Number
        </ListItemTypography>
        <div className={classNames(c.attributeContainer, c.fromFor)}>
          <div className={c.attribute}>
            <ListItemTypography>For</ListItemTypography>
          </div>
          <div className={c.attribute}>
            <ListItemTypography>From</ListItemTypography>
          </div>
        </div>
        <div className={classNames(c.attributeContainer, c.dateAssignedUser)}>
          <div className={c.attribute}>
            <ListItemTypography>Date of Entry</ListItemTypography>
          </div>
          <div className={c.attribute}>
            <ListItemTypography>Assigned</ListItemTypography>
          </div>
        </div>
        <ListItemTypography align="center" className={c.attribute}>
          Description
        </ListItemTypography>
      </li>
      {/* Data */}
      {batches.map(b => {
        const batchNumber = b.batchNumber || "n/a";
        const assignedUser = formatAssignedUserName(b.assignedUser);
        const description = formatDescription(b.description);
        return (
          <li key={b.id} className={c.itemContainer}>
            <div className={c.attribute}>
              <input
                type="checkbox"
                checked={selected.includes(b.id)}
                onChange={handleClick(b.id, setSelected)}
              />
            </div>
            <div className={c.attribute}>
              <ValueCell
                value={batchNumber}
                link={{
                  path: ROUTES.READ_BATCH_FN(b.id),
                  type: LinkType.ROUTE
                }}
                typography={{ noWrap: true }}
              />
            </div>
            <div className={classNames(c.attributeContainer, c.fromFor)}>
              <div className={c.attribute}>
                <ValueCell
                  value={b.from.name}
                  link={{
                    path: ROUTES.READ_LICENSE_HOLDER_FN(b.from.id),
                    type: LinkType.ROUTE
                  }}
                  typography={{ noWrap: true }}
                />
              </div>
              <div className={c.attribute}>
                <ValueCell
                  value={b.for.name}
                  link={{
                    path: ROUTES.READ_LICENSE_HOLDER_FN(b.for.id),
                    type: LinkType.ROUTE
                  }}
                  typography={{ noWrap: true }}
                />
              </div>
            </div>
            <div
              className={classNames(c.attributeContainer, c.dateAssignedUser)}
            >
              <ListItemTypography className={c.attribute}>
                {dateForDisplay(b.dateOfEntry)}
              </ListItemTypography>
              <ListItemTypography className={c.attribute}>
                {assignedUser}
              </ListItemTypography>
            </div>
            <ListItemTypography className={c.attribute}>
              {description}
            </ListItemTypography>
          </li>
        );
      })}
    </ol>
  );
};

const ReadBatches: React.FC<ITableProps> = props => {
  const { handleAddTodos, batches, isSubmitting } = props;
  const [selected, setSelected] = React.useState<string[]>([]);
  const open = useOpenStatus(selected);

  return (
    <>
      <ReadBatchesTable
        batches={batches}
        selected={selected}
        setSelected={setSelected}
      />
      <TodosSnackbar
        open={open}
        handleSubmit={handleSubmit(handleAddTodos, selected)}
        handleClear={handleClear(setSelected)}
        submitLabel="Add Todos"
        disabled={isSubmitting}
      />
    </>
  );
};

export default ReadBatches;
