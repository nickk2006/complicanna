import { ReadBatchesQuery } from "./__generated__/ReadBatchesQuery";
import { MutationFn } from "react-apollo";
import { AddTodos, AddTodosVariables } from "./__generated__/AddTodos";

export interface ITableProps extends ReadBatchesQuery {
  handleAddTodos: MutationFn<AddTodos, AddTodosVariables>;
  isSubmitting: boolean;
}
