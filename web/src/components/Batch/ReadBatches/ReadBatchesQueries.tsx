import { gql } from "apollo-boost";

export const ReadBatchesFragment = gql`
  fragment ReadBatchesFragment on Batch {
    id
    dateOfEntry
    batchNumber
    description
    from {
      id
      name
    }
    for {
      id
      name
    }
    assignedUser {
      id
      firstName
      lastName
    }
  }
`;

export const READ_BATCHES_QUERY = gql`
  query ReadBatchesQuery {
    batches {
      ...ReadBatchesFragment
    }
  }
  ${ReadBatchesFragment}
`;
