import React from "react";
import { MutationFn } from "react-apollo";
import { AddTodos, AddTodosVariables } from "./__generated__/AddTodos";
import { ReadBatchesQuery_batches_assignedUser } from "./__generated__/ReadBatchesQuery";

export const handleClick = (
  id: string,
  setSelected: React.Dispatch<React.SetStateAction<string[]>>
) => (event: { target: HTMLInputElement }) => {
  const checked = event.target.checked;
  if (checked) {
    setSelected(prevState => {
      return [...prevState, id];
    });
  } else {
    setSelected(prevState => {
      const newSelected = prevState.filter(selectedId => selectedId !== id);
      return newSelected;
    });
  }
};

export const handleClickMobile = (
  setSelected: React.Dispatch<React.SetStateAction<string[]>>
) => (id: string) => (
  event: { target: HTMLInputElement },
  checked: boolean
) => {
  if (checked) {
    setSelected(prevState => {
      return [...prevState, id];
    });
  } else {
    setSelected(prevState => {
      const newSelected = prevState.filter(selectedId => selectedId !== id);
      return newSelected;
    });
  }
};

export const handleClear = (
  setSelected: React.Dispatch<React.SetStateAction<string[]>>
) => (event: React.MouseEvent<HTMLElement, MouseEvent>) => {
  setSelected([]);
};

export const handleSubmit = (
  handleAddTodos: MutationFn<AddTodos, AddTodosVariables>,
  selected: string[]
) => (event: React.MouseEvent<HTMLElement, MouseEvent>) =>
  handleAddTodos({ variables: { addIds: selected } });

export const useOpenStatus = (selected: string[]) => {
  const [open, setOpen] = React.useState(false);

  React.useEffect(() => {
    if (selected.length === 0) {
      setOpen(false);
    } else {
      setOpen(true);
    }
  }, [selected]);

  return open;
};

export const formatAssignedUserName = (
  user: ReadBatchesQuery_batches_assignedUser | null
) => {
  if (user === null) return "";
  return `${user.firstName} ${user.lastName}`;
};

export const formatDescription = (description: string | null) => {
  if (description === null) return "";
  return description;
};
