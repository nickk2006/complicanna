/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: ReadBatchesFragment
// ====================================================

export interface ReadBatchesFragment_from {
  __typename: "LicenseHolder";
  id: string;
  name: string;
}

export interface ReadBatchesFragment_for {
  __typename: "LicenseHolder";
  id: string;
  name: string;
}

export interface ReadBatchesFragment_assignedUser {
  __typename: "User";
  id: string;
  firstName: string;
  lastName: string;
}

export interface ReadBatchesFragment {
  __typename: "Batch";
  id: string;
  dateOfEntry: any;
  batchNumber: string | null;
  description: string | null;
  from: ReadBatchesFragment_from;
  for: ReadBatchesFragment_for;
  assignedUser: ReadBatchesFragment_assignedUser | null;
}
