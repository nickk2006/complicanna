/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: ReadBatchesQuery
// ====================================================

export interface ReadBatchesQuery_batches_from {
  __typename: "LicenseHolder";
  id: string;
  name: string;
}

export interface ReadBatchesQuery_batches_for {
  __typename: "LicenseHolder";
  id: string;
  name: string;
}

export interface ReadBatchesQuery_batches_assignedUser {
  __typename: "User";
  id: string;
  firstName: string;
  lastName: string;
}

export interface ReadBatchesQuery_batches {
  __typename: "Batch";
  id: string;
  dateOfEntry: any;
  batchNumber: string | null;
  description: string | null;
  from: ReadBatchesQuery_batches_from;
  for: ReadBatchesQuery_batches_for;
  assignedUser: ReadBatchesQuery_batches_assignedUser | null;
}

export interface ReadBatchesQuery {
  batches: ReadBatchesQuery_batches[];
}
