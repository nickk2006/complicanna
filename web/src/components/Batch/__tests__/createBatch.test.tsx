import React from "react";
import { render, fireEvent } from "react-testing-library";
import CreateBatch from "../CreateBatch";
import { MockedResponse } from "react-apollo/test-utils";
import { CREATE_BATCH } from "../CreateBatch/CreateBatch";
import {
  createBatchVariables,
  createBatch
} from "../CreateBatch/__generated__/createBatch";
import { Unit } from "../../../types/graphql-global-types";
import { FetchResult } from "react-apollo";
import {
  ApolloRouterMock,
  licenseHoldersForPickerMock,
  getInput
} from "../../TestHelpers";
import shortid from "shortid";

const batchNumber = shortid.generate();
const description = shortid.generate();
const from = shortid.generate();
const forLicenseHolder = shortid.generate();

const variables: createBatchVariables = {
  data: {
    batchNumber,
    description,
    weightOrQuantity: {
      unit: Unit.LBS,
      magnitude: 10
    },
    dateOfEntry: "05-05-2018",
    from,
    for: forLicenseHolder
  }
};

const result: FetchResult<createBatch> = {
  data: {
    createBatch: {
      __typename: "Batch",
      id: "ASD-123"
    }
  }
};

const mocks: ReadonlyArray<MockedResponse> = [
  {
    request: {
      query: CREATE_BATCH,
      variables
    },
    result
  },
  licenseHoldersForPickerMock
];

const MockedCreateBatch: React.SFC<{}> = () => {
  return (
    <ApolloRouterMock mocks={mocks}>
      <CreateBatch />
    </ApolloRouterMock>
  );
};

it("renders correct fields", () => {
  const { getByText, getByLabelText, container } = render(
    <ApolloRouterMock mocks={mocks}>
      <CreateBatch />
    </ApolloRouterMock>
  );
  const titleNode = getByText("Batch Intake Form");
  expect(titleNode).toBeDefined();

  // const batchIdNode = container.querySelector(
  //   "input[name='batchNumber']"
  // ) as HTMLInputElement;

  // fireEvent.change(batchIdNode, {
  //   target: { value: batchNumber }
  // });
  const batchIdNode = getInput(container, "batchNumber");
  // inputTest(container, "batchNumber", batchNumber);
  fireEvent.change(batchIdNode, { taget: { value: batchNumber } });
  expect(batchIdNode.value).toBe(batchNumber);
});
