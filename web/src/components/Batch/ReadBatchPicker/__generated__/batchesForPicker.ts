/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: batchesForPicker
// ====================================================

export interface batchesForPicker_batches {
  __typename: "Batch";
  id: string;
  batchNumber: string | null;
}

export interface batchesForPicker {
  batches: batchesForPicker_batches[];
}
