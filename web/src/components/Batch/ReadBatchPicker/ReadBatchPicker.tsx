import React from "react";
import { gql } from "apollo-boost";
import { Query } from "react-apollo";
import { Field } from "formik";
import { TextField } from "formik-material-ui";
import MenuItem from "@material-ui/core/MenuItem";
import { usePickerStyle } from "../../Theme";
import Loading from "../../Loading";
import { batchesForPicker } from "./__generated__/batchesForPicker";

export const READ_BATCH_FOR_PICKER_QUERY = gql`
  query batchesForPicker {
    batches {
      id
      batchNumber
    }
  }
`;

interface IPropsBatchPickerBase extends batchesForPicker {
  name: string;
  label: string;
}

const BatchPickerBase: React.FC<IPropsBatchPickerBase> = ({
  name,
  label,
  batches
}) => {
  const pickerStyle = usePickerStyle();

  return (
    <div className={pickerStyle.root}>
      <Field
        name={name}
        label={label}
        variant="outlined"
        select
        component={TextField}
      >
        {batches.map(batch => (
          <MenuItem key={batch.id} value={batch.id}>
            {batch.batchNumber}
          </MenuItem>
        ))}
      </Field>
    </div>
  );
};

class BatchPickerQuery extends Query<batchesForPicker> {}

const BatchPicker: React.SFC<{ name: string; label: string }> = ({
  name,
  label
}) => (
  <BatchPickerQuery query={READ_BATCH_FOR_PICKER_QUERY}>
    {({ data, loading, error }) => {
      if (loading) return <Loading />;
      if (typeof data !== "undefined")
        return (
          <BatchPickerBase batches={data.batches} label={label} name={name} />
        );
      if (error) {
        console.log("ERROR", error);
        return <div>ERROR: {error.message}</div>;
      }
    }}
  </BatchPickerQuery>
);

export default BatchPicker;
