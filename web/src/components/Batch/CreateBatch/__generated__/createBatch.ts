/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

import { CreateBatchInput } from "./../../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: createBatch
// ====================================================

export interface createBatch_createBatch {
  __typename: "Batch";
  id: string;
}

export interface createBatch {
  createBatch: createBatch_createBatch;
}

export interface createBatchVariables {
  data: CreateBatchInput;
}
