import { gql } from "apollo-boost";
import { Mutation } from "react-apollo";
import { createBatch, createBatchVariables } from "./__generated__/createBatch";

export const CREATE_BATCH = gql`
  mutation createBatch($data: CreateBatchInput!) {
    createBatch(data: $data) {
      id
    }
  }
`;

export class CreateBatchMutation extends Mutation<
  createBatch,
  createBatchVariables
> {}

// export const CreateBatch: React.SFC<{}> = props =>
