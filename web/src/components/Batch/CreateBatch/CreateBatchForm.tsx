import React from "react";
import { Formik, Field, FormikActions } from "formik";
import { TextField } from "formik-material-ui";
import moment from "moment";
import { CreateBatchInput, Unit } from "../../../types/graphql-global-types";
import LicenseHolderPicker from "../../LicenseHolder/LicenseHolderPicker";
import { Title, Form } from "../../FormComponents";
import { ICreateBatchFormProps, IFormValues } from "./types";
import useReactRouter from "use-react-router";
import { WeightField, DateField, SubmitBtn } from "../../FormComponents";
import { CREATE_BATCH, CreateBatchMutation } from "./CreateBatch";
import schema from "./validation";
import { createBatch, createBatchVariables } from "./__generated__/createBatch";
import { MutationFn } from "react-apollo";
import * as ROUTES from "../../../constants/routes";

const INITIAL_VALUES: IFormValues = {
  batchNumber: "",
  description: "",
  weightOrQuantity: {
    magnitude: "",
    unit: Unit.KG
  },
  dateOfEntry: moment().format("YYYY-MM-DD"),
  from: "",
  for: ""
};

const webToApi = (values: IFormValues) => {
  const data: CreateBatchInput = {
    ...values,
    weightOrQuantity: {
      magnitude: parseFloat(values.weightOrQuantity.magnitude),
      unit: values.weightOrQuantity.unit
    }
  };

  return data;
};

const BatchIdField: React.SFC<{}> = () => (
  <Field
    name="batchNumber"
    label="Batch ID"
    component={TextField}
    variant="outlined"
    aria-labelledby="Batch ID"
    data-testid="Batch ID"
  />
);

const DescriptionField: React.SFC<{}> = () => (
  <Field
    name="description"
    label="Description"
    variant="outlined"
    component={TextField}
    multiline
  />
);

export const CreateBatchForm: React.FC<ICreateBatchFormProps> = props => {
  return (
    <Form>
      <Title>Batch Intake Form</Title>
      <BatchIdField />
      <WeightField />
      <LicenseHolderPicker name="from" label="From" />
      <LicenseHolderPicker name="for" label="For" />
      <DateField name="dateOfEntry" label="Date of Entry" />
      <DescriptionField />
      <SubmitBtn disabled={props.isSubmitting}>Create Batch</SubmitBtn>
    </Form>
  );
};

const FormikConnectedForm: React.FC<{
  createBatch: MutationFn<createBatch, createBatchVariables>;
}> = ({ createBatch }) => {
  const { history } = useReactRouter();

  const handleSubmit = async (
    values: IFormValues,
    formikBag: FormikActions<IFormValues>
  ) => {
    const data = webToApi(values);
    const res = await createBatch({ variables: { data } });
    if (typeof res !== "undefined" && typeof res.data !== "undefined") {
      formikBag.resetForm();
      history.push(ROUTES.READ_BATCH_FN(res.data.createBatch.id));
    }
  };

  return (
    <Formik
      initialValues={INITIAL_VALUES}
      onSubmit={handleSubmit}
      validationSchema={schema}
    >
      {formikProps => <CreateBatchForm {...formikProps} />}
    </Formik>
  );
};

const ApiConnectedForm: React.SFC<{}> = () => {
  return (
    <CreateBatchMutation mutation={CREATE_BATCH}>
      {createBatch => <FormikConnectedForm createBatch={createBatch} />}
    </CreateBatchMutation>
  );
};

export default ApiConnectedForm;
