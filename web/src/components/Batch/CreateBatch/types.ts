import { FormikProps } from "formik";
import { Unit } from "../../../types/graphql-global-types";

interface IWeightOrQuantity {
  magnitude: string;
  unit: Unit;
}

export interface IFormValues {
  batchNumber: string;
  description: string;
  weightOrQuantity: IWeightOrQuantity;
  dateOfEntry: string;
  from: string;
  for: string;
}

export interface ICreateBatchFormProps extends FormikProps<IFormValues> {}
