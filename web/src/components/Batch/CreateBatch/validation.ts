import * as yup from "yup";
import { IFormValues } from "./types";
import { Unit } from "../../../types/graphql-global-types";
import { requiredString } from "../../FormComponents";

const createBatchSchema: yup.ObjectSchema<IFormValues> = yup.object({
  batchNumber: requiredString,
  description: requiredString,
  weightOrQuantity: yup
    .object({
      magnitude: requiredString,
      unit: yup
        .mixed()
        .oneOf([Unit.G, Unit.KG, Unit.LBS, Unit.UNITS])
        .required("Required")
    })
    .required("Required"),
  dateOfEntry: requiredString,
  from: requiredString,
  for: requiredString
});

export default createBatchSchema;
