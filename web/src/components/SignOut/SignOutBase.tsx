import React from "react";
import Button from "@material-ui/core/Button";
import firebase from "../Firebase";
import useRouter from "use-react-router";
import * as ROUTES from "../../constants/routes";

const SignOutBtn: React.FC<{}> = props => {
  const { history } = useRouter();
  const handleClick = async () => {
    history.push(ROUTES.HOME);
    await firebase.auth().signOut();
  };

  return (
    <Button onClick={handleClick} variant="outlined">
      Sign Out
    </Button>
  );
};

export default SignOutBtn;
