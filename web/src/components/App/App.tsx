import React from "react";
import { ApolloProvider } from "react-apollo";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import firebase from "../Firebase";
import { ApolloClient } from "apollo-client";
import { setContext } from "apollo-link-context";
import { InMemoryCache } from "apollo-cache-inmemory";
import config from "../../config";
import { useAuthState } from "react-firebase-hooks/auth";
import CssBaseline from "@material-ui/core/CssBaseline";
// import { ThemeProvider } from "@material-ui/styles";
import { MuiThemeProvider as ThemeProvider } from "@material-ui/core/styles";
import { createUploadLink } from "apollo-upload-client";
import theme from "../Theme";
import { unstable_useMediaQuery as useMediaQuery } from "@material-ui/core/useMediaQuery";
import { isMobileQuery } from "../../helpers";

// Components
import Landing from "../Landing";
import Layout from "../Layout";
import AppBar from "../AppBar";
import BottomNavigation from "../BottomNavigation";
import routes from "../Routing/Routes";

const App: React.FC<{}> = () => {
  const { user } = useAuthState(firebase.auth());
  const isMobile = useMediaQuery(isMobileQuery);

  React.useEffect(() => {
    fetch(config.REACT_APP_GRAPHQL_URL).catch(e => console.log(e));
  }, []);

  const httpLink = createUploadLink({
    uri: config.REACT_APP_GRAPHQL_URL
  });

  const authLink = setContext(async (_, { headers }) => {
    if (typeof user !== "undefined" && user !== null) {
      const token = await user.getIdToken();
      return { headers: { ...headers, authorization: `Bearer ${token}` } };
    } else return headers;
  });

  const client = new ApolloClient({
    link: authLink.concat(httpLink),
    cache: new InMemoryCache()
  });

  firebase.auth().onAuthStateChanged(user => {
    if (!user) {
      client.resetStore();
    }
  });

  return (
    <ThemeProvider theme={theme}>
      <ApolloProvider client={client}>
        <CssBaseline />
        <Router>
          {isMobile ? (
            <Route path="/" component={BottomNavigation} />
          ) : (
            <Route path="/" component={AppBar} />
          )}
          <Switch>
            <Route exact path="/" component={Landing} />
            <Layout>{routes}</Layout>
          </Switch>
        </Router>
      </ApolloProvider>
    </ThemeProvider>
  );
};

export default App;
