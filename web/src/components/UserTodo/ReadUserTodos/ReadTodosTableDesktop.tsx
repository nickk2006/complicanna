import React from "react";
import { ReadBatchesTable } from "../../Batch/ReadBatches/ReadBatches4";
import TodosSnackbar from "../../Batch/ReadBatches/TodosSnackbar";
import { handleSubmit } from "./TodosHelpers";
import {
  handleClear,
  useOpenStatus
} from "../../Batch/ReadBatches/BatchHelpers";
// import {} from '../../Batch/ReadBatches/BatchHelpers'
import { IReadTodosProps } from "./types";

const ReadTodos: React.FC<IReadTodosProps> = props => {
  const { batches, handleRemoveTodos, isSubmitting } = props;
  const [selected, setSelected] = React.useState<string[]>([]);
  const open = useOpenStatus(selected);

  return (
    <>
      <ReadBatchesTable
        batches={batches}
        selected={selected}
        setSelected={setSelected}
      />
      <TodosSnackbar
        open={open}
        handleSubmit={handleSubmit(handleRemoveTodos, selected, setSelected)}
        handleClear={handleClear(setSelected)}
        submitLabel="Remove Todos"
        disabled={isSubmitting}
      />
    </>
  );
};

export default ReadTodos;
