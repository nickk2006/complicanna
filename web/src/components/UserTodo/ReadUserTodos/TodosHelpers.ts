import React from "react";
import { MutationFn } from "react-apollo";
import {
  RemoveUserTodosMutation,
  RemoveUserTodosMutationVariables
} from "./__generated__/RemoveUserTodosMutation";

export const handleSubmit = (
  handleRemoveTodos: MutationFn<
    RemoveUserTodosMutation,
    RemoveUserTodosMutationVariables
  >,
  selected: string[],
  setSelected: React.Dispatch<React.SetStateAction<string[]>>
) => async (event: React.MouseEvent<HTMLElement, MouseEvent>) => {
  await handleRemoveTodos({ variables: { removeIds: selected } });
  setSelected([]);
};
