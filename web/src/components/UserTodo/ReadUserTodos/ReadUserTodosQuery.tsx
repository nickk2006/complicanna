import { gql } from "apollo-boost";
import { ReadBatchesFragment } from "../../Batch/ReadBatches/ReadBatchesQueries";

export const READ_USER_TODOS_QUERY = gql`
  query readUserTodosBatches($where: UserWhereUniqueInput!) {
    user(where: $where) {
      assignedBatches {
        ...ReadBatchesFragment
      }
    }
  }
  ${ReadBatchesFragment}
`;
