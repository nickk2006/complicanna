import React from "react";
import TodosSnackbar from "../../Batch/ReadBatches/TodosSnackbar";
import { IReadTodosProps } from "./types";
import { handleSubmit } from "./TodosHelpers";
import {
  handleClear,
  useOpenStatus
} from "../../Batch/ReadBatches/BatchHelpers";
import { BatchesMobile } from "../../Batch/ReadBatches/ReadBatchesTableMobile";
import { handleClickMobile } from "../../Batch/ReadBatches/BatchHelpers";

const ReadTodos: React.FC<IReadTodosProps> = props => {
  const { batches, handleRemoveTodos, isSubmitting } = props;
  const [selected, setSelected] = React.useState<string[]>([]);
  const open = useOpenStatus(selected);
  const handleClick = handleClickMobile(setSelected);
  return (
    <>
      <BatchesMobile
        batches={batches}
        selected={selected}
        // setSelected={setSelected}
        handleClick={handleClick}
        todoLabel="Remove Todos"
      />
      <TodosSnackbar
        open={open}
        handleSubmit={handleSubmit(handleRemoveTodos, selected, setSelected)}
        handleClear={handleClear(setSelected)}
        submitLabel="Remove Todos"
        disabled={isSubmitting}
      />
    </>
  );
};

export default ReadTodos;
