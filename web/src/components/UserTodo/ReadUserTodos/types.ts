import { MutationFn } from "react-apollo";
import {
  RemoveUserTodosMutation,
  RemoveUserTodosMutationVariables
} from "./__generated__/RemoveUserTodosMutation";
import { readUserTodosBatches_user_assignedBatches } from "./__generated__/readUserTodosBatches";

export interface IReadTodosProps {
  batches: readUserTodosBatches_user_assignedBatches[];
  handleRemoveTodos: MutationFn<
    RemoveUserTodosMutation,
    RemoveUserTodosMutationVariables
  >;
  isSubmitting: boolean;
}
