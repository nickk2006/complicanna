import { gql } from "apollo-boost";
import { Mutation } from "react-apollo";
import {
  RemoveUserTodosMutationVariables,
  RemoveUserTodosMutation
} from "./__generated__/RemoveUserTodosMutation";

export const REMOVE_USER_TODOS_MUTATION = gql`
  mutation RemoveUserTodosMutation($removeIds: [String!]!) {
    updateTodos(addIds: [], removeIds: $removeIds)
  }
`;

class RemoveTodosMutation extends Mutation<
  RemoveUserTodosMutation,
  RemoveUserTodosMutationVariables
> {}

export default RemoveTodosMutation;
