/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: RemoveUserTodosMutation
// ====================================================

export interface RemoveUserTodosMutation {
  updateTodos: boolean;
}

export interface RemoveUserTodosMutationVariables {
  removeIds: string[];
}
