/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

import { UserWhereUniqueInput } from "./../../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: readUserTodosBatches
// ====================================================

export interface readUserTodosBatches_user_assignedBatches_from {
  __typename: "LicenseHolder";
  id: string;
  name: string;
}

export interface readUserTodosBatches_user_assignedBatches_for {
  __typename: "LicenseHolder";
  id: string;
  name: string;
}

export interface readUserTodosBatches_user_assignedBatches_assignedUser {
  __typename: "User";
  id: string;
  firstName: string;
  lastName: string;
}

export interface readUserTodosBatches_user_assignedBatches {
  __typename: "Batch";
  id: string;
  dateOfEntry: any;
  batchNumber: string | null;
  description: string | null;
  from: readUserTodosBatches_user_assignedBatches_from;
  for: readUserTodosBatches_user_assignedBatches_for;
  assignedUser: readUserTodosBatches_user_assignedBatches_assignedUser | null;
}

export interface readUserTodosBatches_user {
  __typename: "User";
  assignedBatches: readUserTodosBatches_user_assignedBatches[] | null;
}

export interface readUserTodosBatches {
  user: readUserTodosBatches_user | null;
}

export interface readUserTodosBatchesVariables {
  where: UserWhereUniqueInput;
}
