import React from "react";
import { Query, QueryResult, MutationFn } from "react-apollo";
import {
  readUserTodosBatches,
  readUserTodosBatchesVariables,
  readUserTodosBatches_user_assignedBatches
} from "./__generated__/readUserTodosBatches";
import Loading from "../../Loading";
import { useAuthState } from "react-firebase-hooks/auth";
import firebase from "../../Firebase";
import { Typography } from "@material-ui/core";
import { READ_USER_TODOS_QUERY } from "./ReadUserTodosQuery";
import ReadTodosDesktop from "./ReadTodosTableDesktop";
import RemoveTodosMutation, {
  REMOVE_USER_TODOS_MUTATION
} from "./RemoveUserTodosMutation";
import {
  RemoveUserTodosMutation,
  RemoveUserTodosMutationVariables
} from "./__generated__/RemoveUserTodosMutation";
import Hidden from "@material-ui/core/Hidden";
import ReadTodosMobile from "./ReadTodosMobile";

class ReadUserTodoQuery extends Query<
  readUserTodosBatches,
  readUserTodosBatchesVariables
> {}

const queryVariables = (userId: string): readUserTodosBatchesVariables => ({
  where: {
    id: userId
  }
});

const ReadUserTodoQueryResponse: React.SFC<{
  res: QueryResult<readUserTodosBatches, readUserTodosBatchesVariables>;
  handleRemoveTodos: MutationFn<
    RemoveUserTodosMutation,
    RemoveUserTodosMutationVariables
  >;
  isSubmitting: boolean;
}> = props => {
  const {
    res: { loading, error, data },
    handleRemoveTodos,
    isSubmitting
  } = props;
  if (loading) return <Loading />;
  if (error) return <div>{error.message}</div>;
  if (typeof data === "undefined") return <div>No todos in profile.</div>;
  if (data.user === null) return <div>Unable to find user.</div>;
  if (data.user.assignedBatches === null)
    return <div>No todos in profile.</div>;
  // return <ReadBatchesBase batches={data.user.assignedBatches} />;
  return (
    <Table
      handleRemoveTodos={handleRemoveTodos}
      batches={data.user.assignedBatches}
      isSubmitting={isSubmitting}
    />
  );
};

const Table: React.SFC<{
  batches: readUserTodosBatches_user_assignedBatches[];
  handleRemoveTodos: MutationFn<
    RemoveUserTodosMutation,
    RemoveUserTodosMutationVariables
  >;
  isSubmitting: boolean;
}> = props => {
  const { batches, handleRemoveTodos, isSubmitting } = props;

  return (
    <>
      <Hidden smDown>
        <ReadTodosDesktop
          isSubmitting={isSubmitting}
          batches={batches}
          handleRemoveTodos={handleRemoveTodos}
        />
      </Hidden>
      <Hidden mdUp>
        <ReadTodosMobile
          isSubmitting={isSubmitting}
          batches={batches}
          handleRemoveTodos={handleRemoveTodos}
        />
      </Hidden>
    </>
  );
};
const ReadUserTodo: React.SFC<{}> = () => {
  const { user, initialising } = useAuthState(firebase.auth());

  if (initialising) return <Loading />;
  if (typeof user === "undefined")
    return (
      <Typography variant="h6">
        Error fetching profile data. Please try logging in again or refreshing
        page.
      </Typography>
    );
  const variables = queryVariables(user.uid);

  return (
    <ReadUserTodoQuery query={READ_USER_TODOS_QUERY} variables={variables}>
      {queryRes => (
        <RemoveTodosMutation
          mutation={REMOVE_USER_TODOS_MUTATION}
          onCompleted={() => queryRes.refetch(variables)}
        >
          {(handleRemoveTodos, { loading: isSubmitting }) => (
            <ReadUserTodoQueryResponse
              res={queryRes}
              handleRemoveTodos={handleRemoveTodos}
              isSubmitting={isSubmitting}
            />
          )}
        </RemoveTodosMutation>
      )}
    </ReadUserTodoQuery>
  );
};

export default ReadUserTodo;
