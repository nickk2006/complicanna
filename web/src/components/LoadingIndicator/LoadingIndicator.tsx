import React from "react";
import LinearProgress from "@material-ui/core/LinearProgress";
import Fade from "@material-ui/core/Fade";
import CheckIcon from "@material-ui/icons/Check";
import ErrorIcon from "@material-ui/icons/Error";
import Typography from "@material-ui/core/Typography";

export enum LoadingState {
  sleep = "SLEEP",
  isLoading = "IS_LOADING",
  error = "ERROR"
}

interface IProps {
  loadingState: LoadingState;
  errorMessage?: string;
}

const Loading: React.SFC<{
  loadingState: LoadingState;
  errorMessage?: string;
}> = ({ loadingState, errorMessage }) => {
  switch (loadingState) {
    case LoadingState.sleep:
      return <></>;
    case LoadingState.isLoading:
      return <LinearProgress />;
    case LoadingState.error:
      return (
        <div>
          <ErrorIcon />
          <Typography>{errorMessage ? errorMessage : "Error"}</Typography>
        </div>
      );
  }
};

const Progress: React.SFC<IProps> = props => {
  const { errorMessage, loadingState } = props;

  return (
    <div>
      {props.children}
      <Fade in={loadingState !== LoadingState.sleep}>
        <Loading loadingState={loadingState} errorMessage={errorMessage} />
      </Fade>
    </div>
  );
};

export default Progress;
