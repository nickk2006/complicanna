/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

//==============================================================
// START Enums and Input Objects
//==============================================================

export enum TestResult {
  FAILED = "FAILED",
  PASSED = "PASSED",
  PENDING = "PENDING",
}

export enum Unit {
  G = "G",
  KG = "KG",
  LBS = "LBS",
  UNITS = "UNITS",
}

export interface BatchWhereUniqueInput {
  id?: string | null;
}

export interface CreateBatchInput {
  batchNumber: string;
  description: string;
  weightOrQuantity: CreateWeightOrQuantityInput;
  dateOfEntry: string;
  from: string;
  for: string;
}

export interface CreateLicenseHolderInput {
  name: string;
  license: CreateLicenseInput;
}

export interface CreateLicenseInput {
  number: string;
  type: number;
}

export interface CreateTestInput {
  labId: string;
  batchId: string;
  sampleDate?: any | null;
}

export interface CreateTestingLabInput {
  name: string;
  email: string;
  phone?: string | null;
}

export interface CreateWeightOrQuantityInput {
  magnitude: number;
  unit: Unit;
}

export interface LicenseHolderWhereUniqueInput {
  id?: string | null;
}

export interface RequestDemoInput {
  firstName: string;
  lastName: string;
  email: string;
  companyName: string;
}

export interface SignUpInput {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  companyName: string;
}

export interface TestWhereUniqueInput {
  id?: string | null;
}

export interface TestingLabWhereUniqueInput {
  id?: string | null;
}

export interface UpdateTestInput {
  labId?: string | null;
  batchId?: string | null;
  sampleDate?: any | null;
  resultDate?: any | null;
  certificateOfAnalysis?: any | null;
  video?: any | null;
  chainOfCustody?: any | null;
  result?: TestResult | null;
}

export interface UserWhereUniqueInput {
  id?: string | null;
}

//==============================================================
// END Enums and Input Objects
//==============================================================
