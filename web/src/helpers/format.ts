import moment from "moment";

export const dateForField = (date: string) =>
  moment(date)
    .utc()
    .format("YYYY-MM-DD");

export const objectStringToUndefined = (obj: any) => {
  let newObj: any = {};
  for (let key in obj) {
    if (obj[key] === "") {
      newObj[key] = undefined;
    } else {
      newObj[key] = obj[key];
    }
  }
  return newObj;
};

export const dateForDisplay = (iso8601Date: string) =>
  moment(iso8601Date)
    .utc()
    .format("MMM DD, YYYY");

export const magnitudeForDisplay = (magnitude: number) =>
  Math.floor(magnitude * 100) / 100;
