const isMobileQuery = "(max-width:600px)";

export default isMobileQuery;
