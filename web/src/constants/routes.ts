import { LocationDescriptorObject, Location } from "history";
import { IPlan } from "./plans";

export enum AUTH_NAV {
  HOME = 0,
  TODOS,
  BATCHES,
  NEW,
  ACCOUNT
}

export enum UNAUTH_NAV {
  PRICING = 0,
  DEMO,
  HOME,
  SIGN_UP,
  LOGIN
}

export interface ILocation extends LocationDescriptorObject<{}> {}

// App Bar
export const HOME = "/";

export const SIGN_UP = "/signup";

export const SIGN_UP_FN = (plan: Pick<IPlan, "planId">): Location => ({
  pathname: "/signup",
  search: `?id=${plan.planId}`,
  hash: "",
  state: {}
});

export const LOGIN = "/login";

export const PRICING = "/pricing";

export const CONTACT = "/contact";

export const MY_ACCOUNT = "/my-account";

export const DEMO = "/demo";

export const DEMO_SIGNIN = "/demo/signin";

// Administrative
export const CREATE_EMPLOYEE = "/employees/new";

// Batch
export const CREATE_BATCH = "/batches/new";

export const READ_BATCH = "/batches/:batchId";

export const READ_BATCH_FN = (batchId: string) => `/batches/${batchId}`;

export const READ_BATCHES = "/batches";

// License Holders
export const CREATE_LICENSE_HOLDER = "/license-holders/new";

export const READ_LICENSE_HOLDER = "/license-holders/:licenseHolderId";

export const READ_LICENSE_HOLDER_FN = (licenseHolderId: string) =>
  `/license-holders/${licenseHolderId}`;

// Testing Lab
export const CREATE_TESTING_LAB = "/testing-labs/new";

export const READ_TESTING_LABS = "/testing-labs";

export const READ_TESTING_LAB = "/testing-labs/:testingLabId";

export const READ_TESTING_LAB_FN = (testingLabId: string) =>
  `/testing-labs/${testingLabId}`;

// Tests
export const CREATE_TEST = "/tests/new";

export const READ_TESTS = "/tests";

export const READ_TEST = "/tests/:testId";

export const READ_TEST_FN = (testId: string) => `/tests/${testId}`;

export const UPDATE_TEST = "/tests/:testId/update";

export interface IUPDATE_TEST {
  testId: string;
}

export const UPDATE_TEST_FN = (testId: string) => `/tests/${testId}/update`;

// User Todos
export const READ_USER_TODOS = "/my-todos";

// New
export const NEW = "/new";
