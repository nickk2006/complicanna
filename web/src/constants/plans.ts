import keys from "../config";

export interface IPlan {
  planId: string;
  displayName: string;
  price: number;
}

const regionalPlan: IPlan = {
  planId: keys.REACT_APP_STRIPE_REGIONAL_PLAN_ID,
  displayName: "Regional",
  price: 80
};

const statewidePlan: IPlan = {
  planId: keys.REACT_APP_STRIPE_STATEWIDE_PLAN_ID,
  displayName: "Statewide",
  price: 150
};

const plans: IPlan[] = [regionalPlan, statewidePlan];

export { regionalPlan, statewidePlan, plans };
